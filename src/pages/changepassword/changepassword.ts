import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    LoadingController,
    ToastController
} from 'ionic-angular';
@IonicPage()
@Component({
    selector: 'page-changepassword',
    templateUrl: 'changepassword.html',
})
export class ChangepasswordPage {
    token: any
    data: any = {
        new_password: '',
        confirm_password: '',
        current_password: '',
        token: '',
        is_admin: ''
    }
    passwordType: string = 'password';
    passwordIcon: string = 'eye-off';
    passwordType2: string = 'password';
    passwordIcon2: string = 'eye-off';
    passwordType1: string = 'password';
    passwordIcon1: string = 'eye-off';
    error: any = {
        curr_pwd: '',
        new_pwd: '',
        con_pwd: '',
    }
    constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public user: UserProvider, public alert: AlertProvider) {
        let admin = localStorage.getItem("is_admin")
        if (admin == "true") {
            this.data.is_admin = true;
        } else {
            this.data.is_admin = false
        }
        this.token = localStorage.getItem("tokens")
    }
    logForm() {
        console.log(this.data)
    }
    backButtonAction() {
        this.navCtrl.pop()
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ChangepasswordPage');
    }
    confirm() {
        if (this.data.current_password == '') {
            this.error.curr_pwd = "Please Enter the Current Password"
            return
        }
        if (this.data.current_password.length < 6) {
            this.error.curr_pwd = "Minimum Password Length Should be 6"
            return
        }
        if (this.data.new_password == '') {
            this.error.new_pwd = "Please Enter the New Password"
            return
        }
        if (this.data.new_password.length < 6) {
            this.error.new_pwd = "Minimum Password Length Should be 6"
            return
        }
        if (this.data.confirm_password == '') {
            this.error.con_pwd = "Please Enter Confirm Password"
            return
        }
        if (this.data.confirm_password.length < 6) {
            this.error.con_pwd = "Minimum Password Length Should be 6"
            return
        }
        if (this.data.new_password != this.data.confirm_password) {
            this.alert.Alert.alert("Password Mismatch")
            return
        }
        this.data.token = this.token;
        this.user.change_password(this.data).subscribe((data: any) => {
            console.log(data)
            if (data.status == 'ok') {
                this.alert.Toast.show("Password Changed Successfully")
                // localStorage.setItem("token", this.token);
                this.navCtrl.setRoot('MyProfilePage')
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    showHide() {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
    showHides() {
        this.passwordType2 = this.passwordType2 === 'text' ? 'password' : 'text';
        this.passwordIcon2 = this.passwordIcon2 === 'eye-off' ? 'eye' : 'eye-off';
    }
    showHidess() {
        this.passwordType1 = this.passwordType1 === 'text' ? 'password' : 'text';
        this.passwordIcon1 = this.passwordIcon1 === 'eye-off' ? 'eye' : 'eye-off';
    }
    onChangeTime(value: any) {
        this.error.new_pwd = "";
    }
    onChangeTimes(value: any) {
        this.error.con_pwd = "";
    }
    onChangeCP() {
        this.error.curr_pwd = "";
    }
}