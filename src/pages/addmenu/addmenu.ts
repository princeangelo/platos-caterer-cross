import {
    Component,
    ViewChild,
    ElementRef
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    LoadingController,
    ToastController
} from 'ionic-angular';
import {
    FileTransfer,
    FileUploadOptions,
    FileTransferObject
} from '@ionic-native/file-transfer';
import {
    Camera,
    CameraOptions
} from '@ionic-native/camera';
import {
    NgZone
} from '@angular/core';
import {
    DomSanitizer
} from '@angular/platform-browser';
@IonicPage()
@Component({
    selector: 'page-addmenu',
    templateUrl: 'addmenu.html',
})
export class AddmenuPage {
    @ViewChild('myInput') myInput: ElementRef;
    desc: any;
    img1: any;
    imagebox: any = []
    mealsList: any
    cuisineList: any
    Packinglist: any
    gropsList: any
    imageURI: any;
    menu_items: any
    data: any = {
        catt_id: '',
        cuisine_type_id: '',
        // group_id:'',
        is_recommended: false,
        is_veg: '',
        max_order: '',
        meal_type_id: '',
        menu_description: '',
        menu_images: [],
        menu_name: '',
        min_order: '',
        package_type_id: '',
        per_person_cost: '',
        discount: '',
        discount_duration: '',
        // special_category:'',
        item_group: '',
        gst_percent: '',
        is_combo: false,
        no_of_combo: '',
        hsn_number: ''
    }
    error: any = {
        catt_id: '',
        cuisine_type_id: '',
        group_id: '',
        is_recommended: '',
        is_veg: '',
        meal_type_id: '',
        menu_description: '',
        menu_images: '',
        menu_name: '',
        order: '',
        package_type_id: '',
        per_person_cost: '',
        discount: '',
        discount_duration: '',
        hsn_number: '',
        gst_percent: ''
    }
    menuarray
    menuList: any = []
    selected: any = []
    Caterer_List: any
    showCat: any = false
    dummyPush: any = []
    showPlus: boolean = true
    count: any = 0
    combo: any
    Combo_arr: any = [{
        "id": 1,
        "value": 2,
    }, {
        "id": 2,
        "value": 3
    }, {
        "id": 3,
        "value": 4
    }, {
        "id": 4,
        "value": 5
    }, ]
    Combo_List: any = []
    items: any
    combo_items: any = []
    is_menu_taken: any = false
    selectOptions: any
    selectOptions1: any
    selectOptions2: any
    selectOptions3: any
    selectOptions4: any
    selectOptions5: any
    constructor(public _DomSanitizer: DomSanitizer, public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider, private transfer: FileTransfer, private camera: Camera, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public _zone: NgZone, public platform: Platform) {
        this.selectOptions = {
            title: 'Vegetarian',
        };
        this.selectOptions1 = {
            title: 'Meal Type',
            subTitle: '(Scroll for more)',
        };
        this.selectOptions2 = {
            title: 'Cuisine Type',
            subTitle: '(Scroll for more)',
        };
        this.selectOptions3 = {
            title: 'Packing Type',
            subTitle: '(Scroll for more)',
        };
        this.selectOptions4 = {
            title: 'Menu group',
            subTitle: '(Scroll for more)',
        };
        this.selectOptions5 = {
            title: 'Item List',
            subTitle: '(Scroll for more)',
        };
        let token: any = localStorage.getItem("tokens")
        this.user.getMenu(token).subscribe((data: any) => {
            console.log(data, "all menu")
            this.items = data.data;
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad AddmenuPage');
        this.dropDowndetails();
    }
    filepreview() {
        console.log("tested here");
    }
    backButtonAction() {
        this.navCtrl.pop()
    }
    moveFocus(nextElement) {
        nextElement.setFocus();
    }
     onChangediscount(){
       this.error.discount = ""
      }
     onChangedisduration(){
       this.error.discount_duration = ""
      }
      
    dropDowndetails() {
        this.user.getCuisineDropdownList().subscribe((data: any) => {
            console.log(data, "cuisine")
            this.cuisineList = data.data;
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        this.user.getGroupsDropdownList().subscribe((data: any) => {
            console.log(data, "groups")
            this.gropsList = data.data;
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        this.user.getMealsDropdownList().subscribe((data: any) => {
            console.log(data, "meals")
            this.mealsList = data.data;
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        this.user.getPackageDropdownList().subscribe((data: any) => {
            console.log(data, "package")
            this.Packinglist = data.data;
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        let id: any = localStorage.getItem("tokens")
        this.user.getGroupList(id).subscribe((data: any) => {
            this.alert.Loader.hide()
            console.log(data, "item group")
            this.menuarray = data.data;
            for (let i = 0; i < this.menuarray.length; i++) {
                this.menuList.push({
                    "menu": this.menuarray[i].menugroup_name,
                    "menu_id": this.menuarray[i].menugroup_id
                })
            }
            console.log("menu list", this.menuList)
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    getImage() {
        this._zone.run(() => {
            const options: CameraOptions = {
                quality: 30,
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            }
            this.camera.getPicture(options).then((imageData) => {
                this.imageURI = this._DomSanitizer.bypassSecurityTrustUrl(imageData);
                let temp = {
                    img: ''
                }
                temp.img = this.imageURI
                this.count = this.count + 1;
                this.imagebox.push(temp);
                console.log(this.imagebox)
            }, (err) => {
                console.log(err);
                this.presentToast(err);
            });
        })
    }
    randomFileName() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        var n = new Date()
        var date = n.toISOString();
        return text + "-" + date + ".png";
    }

    uploadFile() {
        return new Promise((resolve, reject) => {
            let loader = this.loadingCtrl.create({
                content: "Uploading..."
            });
            if (this.imagebox.length > 0) {
                loader.present();
            }
            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
                fileKey: 'pic[]',
                fileName: this.randomFileName(),
                chunkedMode: false,
                mimeType: "image/png",
                headers: {}
            }
            let data = [];
            for (var i = 0; i < this.imagebox.length; ++i) {
                data.push(new Promise((resolve, reject) => {
                    resolve(fileTransfer.upload(this.imagebox[i].img.changingThisBreaksApplicationSecurity, 'http://platos.in/api/AddMenu_images', options).then((data) => {
                        console.log(data, " Uploaded Successfully");
                        let temp = JSON.parse(data.response)
                        let imgURL = temp.data[0];
                        this.dummyPush.push(imgURL)
                        console.log(this.dummyPush, "dumy")
                        loader.dismiss();
                        // this.presentToast("Image uploaded successfully");
                    }, (err) => {
                        console.log(err);
                        loader.dismiss();
                        // this.presentToast(err);
                    }))
                }))
            }
            Promise.all(data).then((values) => {
                resolve(values)
            });
        })
    }
    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }
    deleteImage(index) {
        console.log(index)
        if (index > -1) {
            this.imagebox.splice(index, 1);
            this.count = this.count - 1;
        }
    }
    async menu() {
        if (this.data.menu_name == "") {
            this.error.menu_name = "Please Enter the Menu Name"
            if (this.data.menu_description == "") {
                this.error.menu_description = "Please Enter the Menu Description"
                if (this.data.per_person_cost == "") {
                    this.error.per_person_cost = "Please Enter the Per Person Cost"
                    if (this.data.gst_percent == "") {
                        this.error.gst_percent = "Please Enter the GST Percentage"
                        if (this.data.hsn_number == "") {
                            this.error.hsn_number = "Please Enter the HSN Number"
                            if (this.data.is_veg == "") {
                                this.error.is_veg = "Please Select the Dish Type"
                                if (this.data.meal_type_id == "") {
                                    this.error.meal_type_id = "Please Select the Meal Type"
                                    if (this.data.cuisine_type_id == "") {
                                        this.error.cuisine_type_id = "Please Select the Cuisine Type"
                                        if (this.data.package_type_id == "") {
                                            return this.error.package_type_id = "Please Select the Package Type"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (this.data.menu_name == "") {
            return this.error.menu_name = "Please Enter the Menu Name"
        }
        if (this.data.menu_name) {
            if (this.is_menu_taken == true) {
                return this.error.menu_name = "Menu Name Already Exists"
            }
        }
        if (this.data.min_order) {
           if(this.data.min_order > 0) {
            if (this.data.max_order) {
               if (this.data.max_order > 0) {

                if (this.data.min_order > this.data.max_order) {
                    return this.error.order = "Minimum Order Can't be more than Maximum Order"
                } else if (this.data.max_order < this.data.min_order) {
                    return this.error.order = "Maximum Order Can't be less than Minimum Order"
                } else if(this.data.min_order == this.data.max_order){
                    return this.error.order = "Minimum Order & Maximum Order Can't Be Same"
                }
              }  

             else{
                return this.error.order = "Invalid Maximum Order" 
             }

            } 
            else {
                return this.error.order = "Please Enter the Maximum Order"
            }

         }
         else{
            return this.error.order = "Invalid Minimum Order"
         }

        }


        if (this.data.max_order) {
           if (this.data.max_order > 0) {
            if (this.data.min_order) {
               if (this.data.min_order > 0) { 

                if (this.data.min_order > this.data.max_order) {
                    return this.error.order = "Minimum Order Can't be more than Maximum Order"
                } else if (this.data.max_order < this.data.min_order) {
                    return this.error.order = "Maximum Order Can't be less than Minimum Order"
                } else if(this.data.min_order == this.data.max_order){
                    return this.error.order = "Minimum Order & Maximum Order Can't Be Same"
                }

              }
              else{
                return this.error.order = "Invalid Minimum Order"
              }
            } 
            else {
                return this.error.order = "Please Enter the Minimum Order"
            }
          }
          else{
                return this.error.order = "Invalid Maximum Order"
              }
        }


        if (this.data.per_person_cost == "") {
            return this.error.per_person_cost = "Please Enter the Per Person Cost"
        }

        if (this.data.per_person_cost <= 0) {
            return this.error.per_person_cost = "Invalid Per Person Cost"
        }

        if (this.data.gst_percent == "") {
            return this.error.gst_percent = "Please Enter the GST Percentage"
        }

        if (this.data.gst_percent <= 0) {
            return this.error.gst_percent = "Invalid GST Percentage"
        }


        if (this.data.gst_percent > 100) {
            return this.error.gst_percent = "GST Percentage Should Not Exceed More Than 100"
        }
        if (this.data.hsn_number == "") {
            return this.error.hsn_number = "Please Enter The HSN Number"
        }
        if (this.data.discount) {
            if(this.data.discount > 0){
            if (this.data.discount_duration == "") {
                return this.error.discount_duration = "Please Enter the Discount Duration"
            }

             if (this.data.discount_duration <= 0) {
                return this.error.discount_duration = "Invalid Discount Duration"
            }
          }
          else{
             return this.error.discount = "Invalid Discount Percentage" 
          }
        }




        if (this.data.discount_duration) {
            if(this.data.discount_duration > 0){
            if (this.data.discount == "") {
                return this.error.discount = "Please Enter the Discount Percentage"
            }

            if (this.data.discount <= 0) {
                return this.error.discount = "Invalid Discount Percentage"
            }
          }
          else{
             return this.error.discount_duration = "Invalid Discount Duration" 
          } 
        }


        if (this.data.is_veg == "") {
            return this.error.is_veg = "Please Select the Dish Type"
        }
        if (this.data.meal_type_id == "") {
            return this.error.meal_type_id = "Please Select the Meal Type"
        }
        if (this.data.cuisine_type_id == "") {
            return this.error.cuisine_type_id = "Please Select the Cuisine Type"
        }
        if (this.data.package_type_id == "") {
            return this.error.package_type_id = "Please Select the Package Type"
        }
        await this.uploadFile()
        await this.apihit()
    }
    apihit() {
        let temp_item = []
        let combo_item = []
        debugger
        for (var i = 0; i < this.Combo_List.length; ++i) {
            temp_item = []
            for (var j = 0; j < this.Combo_List[i].combo_item.length; ++j) {
                for (var k = 0; k < this.items.length; ++k) {
                    if (this.Combo_List[i].combo_item[j] == this.items[k].menu_id) {
                        temp_item.push({
                            "menu_id": this.Combo_List[i].combo_item[j],
                            "menu_name": this.items[k].menu_name
                        })
                    }
                }
            }
            combo_item.push({
                "combo_item": temp_item,
                "combo_name": this.Combo_List[i].combo_name
            })
        }
        console.log(combo_item, "ComboList")
        this.data.combo_items = combo_item;
        console.log("array", this.dummyPush)
        if (this.dummyPush.length > 0) {
            this.data.is_recommended = true;
        }
        this.data.menu_images[0] = this.dummyPush
        let id: any = localStorage.getItem("token")
        this.data.catt_id = id;
        if (this.data.catt_id == "") {
            return this.alert.Alert.alert("Catt id was null")
        }
        console.log(this.data)
        // if(this.data.item_group == ""){
        // return this.error.group_id ="Please Select the Group"
        // }
        // if(this.menu_items){
        //   for(let i = 0;i < this.menu_items.length;i++){
        //        this.selected.push({ "menugroup_id": this.menu_items[i]})
        //     }
        //     this.data.item_group = this.selected
        // }
        console.log(this.data, "before api hit")
        this.user.addMenu(this.data).subscribe((data: any) => {
            console.log(data)
            if (data.status == 'ok') {
                this.navCtrl.setRoot('MenusPage')
                this.alert.Toast.show("Menu Added Successfully")
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    onChangeName() {
        if (this.data.menu_name) {
            let token = localStorage.getItem('tokens')
            let data2 = {
                "menu_name": this.data.menu_name,
                "token": token
            }
            this.user.checkMenuNameExists(data2).subscribe((data: any) => {
                console.log(data)
                if (data.status == 'ok') {
                    this.error.menu_name = "";
                    this.is_menu_taken = false;
                } else {
                    this.is_menu_taken = true;
                    this.error.menu_name = "Menu Name Already Exist"
                    return;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        } else {
            this.error.menu_name = "";
        }
    }
    
    onChangedesc() {
        this.error.menu_description = "";
    }
    onChangeprice() {
        this.error.per_person_cost = "";
    }
    onChangeisVeg() {
        this.error.is_veg = "";
    }
    onChangeMeal() {
        this.error.meal_type_id = "";
    }
    onChangeCui() {
        this.error.cuisine_type_id = "";
    }
    onChangePack() {
        this.error.package_type_id = "";
    }
    onChangegroup() {
        this.error.group_id = "";
    }
    onChangegstPer() {
        this.error.gst_percent = ""
    }
    onChangemin() {
        this.error.order = ""
    }
    onChangeHSN() {
        this.error.hsn_number = ""
    }
    onChangecom() {
        console.log("combo value", this.data.no_of_combo)
        let combo_count = this.data.no_of_combo
        combo_count = parseInt(combo_count)
        if (this.Combo_List.length > 0) {
            if (combo_count < this.Combo_List.length) {
                combo_count = this.Combo_List.length - combo_count;
                for (var j = 0; j < combo_count; ++j) {
                    this.Combo_List.splice(this.Combo_List.length - 1, 1);
                }
            } else if (combo_count > this.Combo_List.length) {
                combo_count = combo_count - this.Combo_List.length;
                //splice
                for (var i = 0; i < combo_count; ++i) {
                    this.Combo_List.push({
                        "combo_name": '',
                        "combo_item": ''
                    })
                }
            } else if (combo_count == this.Combo_List) {
                console.log("Good to go")
            }
        } else {
            for (var k = 0; k < combo_count; ++k) {
                this.Combo_List.push({
                    "combo_name": '',
                    "combo_item": ''
                })
            }
        }
        console.log(this.Combo_List, "Combo list")
    }

    resize() {
        var element = this.myInput['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
        var scrollHeight = element.scrollHeight;
        element.style.height = scrollHeight + 'px';
        this.myInput['_elementRef'].nativeElement.style.height = (scrollHeight + 16) + 'px';
    }

    checkBox(){
        if(this.data.is_combo == false){
           this.data.no_of_combo = '';
           this.Combo_List = []
        }
    }
}