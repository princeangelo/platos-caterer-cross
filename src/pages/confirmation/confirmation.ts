import {
    Component,
    ViewChild
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    ToastController
} from 'ionic-angular';
import {
    Api
} from '../../providers/api/api';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    Platform,
    Content
} from 'ionic-angular';
import {
    NgZone
} from '@angular/core';
import {
    Events
} from 'ionic-angular';
@IonicPage()
@Component({
    selector: 'page-confirmation',
    templateUrl: 'confirmation.html',
})
export class ConfirmationPage {
    @ViewChild(Content) content: Content;
    flagStatus = false
    searchText = ""
    OrgItems: any = []
    users: any = []
    infiniteScrollEnabled = true
    page_count = 0;
    totalPages: any = 0;
    hasMoreData: any = false
    tempDataSearch: any
    messages = [];
    constructor(public events: Events, public _zone: NgZone, public toast: ToastController, public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider, public api: Api) {

    }

    ionViewDidLoad() {
        this.apihit()
    }
    scrollToTop() {
        this.content.scrollToTop();
    }
    apihit() {
        this._zone.run(() => {
            this.OrgItems = []
            this.users = []
            this.page_count = 0;
            this.totalPages = 0;
            this.infiniteScrollEnabled = true;
            this.alert.Loader.show("Loading")
            let token = localStorage.getItem("tokens")
            this.page_count = this.page_count + 1;
            let dataToSet = {
                token: token,
                page_no: this.page_count,
                page_count: 10,
                search: ''
            }
            this.user.getConfirmationList(dataToSet).subscribe((data: any) => {
                console.log(data, "Confirmation details")
                // this.users = data.data;
                // this.OrgItems = data.data;
                if (data.status == "ok") {
                    if (data.data.total > 0) {
                        if (data.data.orders.length > 0) {
                            for (let i = 0; i < data.data.orders.length; i++) {
                                this.users.push(data.data.orders[i]);
                                this.OrgItems.push(data.data.orders[i]);
                                this.tempDataSearch = this.OrgItems
                            }
                        }
                    }
                    // if(this.users.length > 0){
                    let temp: any = data.data.total / 10
                    if ((data.data.total % 10) != 0) {
                        this.totalPages = parseInt(temp) + 1;
                    } else {
                        this.totalPages = parseInt(temp)
                    }
                    this.hasMoreData = true
                    this.alert.Loader.hide();
                } else {
                    this.alert.Alert.alert(data.data)
                    return
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        })
    }
    showToast(msg) {
        let toast = this.toast.create({
            message: msg,
            duration: 2000
        });
        toast.present();
    }
    LoadData(event) {
        this.page_count = this.page_count + 1;
        debugger
        if (this.page_count <= this.totalPages) {
            // this.alert.Loader.show("Loading")
            let id: any = localStorage.getItem("tokens")
            let dataToSet = {
                token: id,
                page_no: this.page_count,
                page_count: 10,
                search: ''
            }
            this.user.getConfirmationList(dataToSet).subscribe((data: any) => {
                this.alert.Loader.hide()
                console.log(data)
                if (data.data.total > 0) {
                    if (data.data.orders.length > 0) {
                        for (let i = 0; i < data.data.orders.length; i++) {
                            this.users.push(data.data.orders[i]);
                            this.OrgItems.push(data.data.orders[i]);
                            this.tempDataSearch = this.OrgItems
                        }
                    }
                }
                console.log(this.users)
                if (this.page_count != this.totalPages) {
                    event.complete();
                } else {
                    this.infiniteScrollEnabled = false;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        } else {
            console.log("in else")
            this.infiniteScrollEnabled = false;
        }
    }
    detail(value) {
        this.navCtrl.push("ConfirmationDetailPage", {
            data: value
        })
    }
    onFilter(far) {
        console.log(this.searchText)
        // this.users = this.OrgItems;
        //  this.users =  this.users.filter((confir) => {
        //       if (confir.userslist.usr_first_name.toLocaleLowerCase().includes(this.searchText.toLocaleLowerCase()) ) {
        //           return confir
        //       }
        //   })
        this.users = []
        this.page_count = 0;
        this.totalPages = 0;
        this.infiniteScrollEnabled = true;
        let token = localStorage.getItem("tokens")
        this.page_count = this.page_count + 1;
        let dataToSet = {
            token: token,
            page_no: 1,
            page_count: 10,
            search: this.searchText
        }
        this.user.getConfirmationList(dataToSet).subscribe((data: any) => {
            console.log(data, "Confirmation details")
            if (data.status == "ok") {
                if (data.data.orders[0].total > 0) {
                    if (data.data.orders.length > 0) {
                        for (let i = 0; i < data.data.orders.length; i++) {
                            this.users.push(data.data.orders[i]);
                        }
                    }
                }
                let temp: any = data.data.orders[0].total / 10
                if ((data.data.orders[0].total % 10) != 0) {
                    this.totalPages = parseInt(temp) + 1;
                } else {
                    this.totalPages = parseInt(temp)
                }
                this.hasMoreData = true
                this.alert.Loader.hide();
            } else {
                this.alert.Alert.alert(data.data)
                return
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    onCancel(far) {
        // this.users = this.tempDataSearch;
        this.apihit()
    }
    cancelOrder(value) {
        console.log(value, "value")
        this.alert.presentPromptFeedBack().then((res) => {
            let datas = localStorage.getItem("feedback")
            console.log("hit api", datas)
            console.log("cancel clicked")
            if (datas == "") {
                return this.alert.Alert.alert("Please Enter the Reason for Cancellation")
            }
            let id;
            let data = {
                "order_id": '',
                "ord_status": '',
                "reason_cancel": ''
            }
            data.order_id = value.ord_id;
            id = data.order_id;
            data.ord_status = "Cancelled"
            data.reason_cancel = datas;
            console.log(data)
            this.alert.Loader.show("Loading..")
            this.user.changeStatus(id, data).subscribe((data: any) => {
                console.log(data)
                if (data.status == 'ok') {
                    this.alert.Loader.hide()
                    this.alert.Toast.show("Order Cancelled")
                    this.navCtrl.setRoot('ConfirmationPage')
                } else {
                    this.alert.Loader.hide()
                    this.alert.Alert.alert(data.data)
                    return;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        }, err => {
            console.log(err)
        })
    }
    confirmOrder(value) {
        this.alert.presentConfirm().then((res) => {
            let id;
            let data = {
                "order_id": '',
                "ord_status": ''
            }
            data.order_id = value.ord_id;
            id = data.order_id;
            data.ord_status = "Confirmed"
            console.log(data)
            this.alert.Loader.show("Loading..")
            this.user.changeStatus(id, data).subscribe((data: any) => {
                console.log(data)
                if (data.status == 'ok') {
                    this.alert.Loader.hide()
                    this.alert.Toast.show("Order Confirmed")
                    this.navCtrl.setRoot('ConfirmationPage')
                } else {
                    this.alert.Loader.hide()
                    this.alert.Alert.alert(data.data)
                    return;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        }, err => {
            console.log(err)
        })
    }
    alertShow() {
        this.alert.presentPromptFeedBack().then((res) => {
            let data = localStorage.getItem("feedback")
            console.log("hit api", data)
        }, err => {
            console.log(err)
        })
    }
    timeconvert(tim) {
        var timeString = tim + ":00";
        var H = +timeString.substr(0, 2);
        var h = H % 12 || 12;
        var ampm = (H < 12 || H === 24) ? "AM" : "PM";
        return timeString = h + timeString.substr(2, 3) + ampm;
    }
    doRefresh(refresher) {
        this._zone.run(() => {
            this.OrgItems = []
            this.users = []
            this.page_count = 0;
            this.totalPages = 0;
            this.infiniteScrollEnabled = true
            this.alert.Loader.show("Loading")
            let token = localStorage.getItem("tokens")
            this.page_count = this.page_count + 1;
            let dataToSet = {
                token: token,
                page_no: this.page_count,
                page_count: 10,
                search: ''
            }
            this.user.getConfirmationList(dataToSet).subscribe((data: any) => {
                console.log(data, "Confirmation details")
                // this.users = data.data;
                // this.OrgItems = data.data;
                if (data.status == "ok") {
                    if (data.data.total > 0) {
                        if (data.data.orders.length > 0) {
                            for (let i = 0; i < data.data.orders.length; i++) {
                                this.users.push(data.data.orders[i]);
                                this.OrgItems.push(data.data.orders[i]);
                                this.tempDataSearch = this.OrgItems
                            }
                        }
                    }
                    // if(this.users.length > 0){
                    let temp: any = data.data.total / 10
                    if ((data.data.total % 10) != 0) {
                        this.totalPages = parseInt(temp) + 1;
                    } else {
                        this.totalPages = parseInt(temp)
                    }
                    this.hasMoreData = true
                    refresher.complete();
                    this.alert.Loader.hide();
                } else {
                    refresher.complete();
                    this.alert.Alert.alert(data.data)
                    return
                }
            }, async (error) => {
                refresher.complete();
                this.alert.Loader.hide();
                await this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        })
    }
}