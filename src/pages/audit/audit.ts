import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    AlertController,
    ToastController,
    LoadingController,
} from 'ionic-angular';
import {
    Api
} from '../../providers/api/api';
import {
    FileTransfer,
    FileTransferObject
} from '@ionic-native/file-transfer';
import {
    File
} from '@ionic-native/file';
import {
    Events
} from 'ionic-angular';
import { FileOpener } from '@ionic-native/file-opener';

@IonicPage()
@Component({
    selector: 'page-audit',
    templateUrl: 'audit.html',
})
export class AuditPage {
    filePath: any
    audit: any
    score: any
    filelocation
    firstArr: any
    constructor(public toastCtrl:ToastController,private fileOpener: FileOpener,public events: Events, public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider, public transfer: FileTransfer, public api: Api, private file: File) {}
    ionViewDidLoad() {
        this.alert.Loader.show("Loading..")
        this.filePath = this.api.filepath
        this.filelocation = this.user.getDownloadPath()
        let token: any = localStorage.getItem("tokens")
        let data: any = {
            token: '',
        }
        data.token = token;
        this.user.getCaterer(data).subscribe((data: any) => {
            this.events.publish('audit:status', data.data.audit_deactive);
            this.score = data.data.catt_score
            this.audit = data.data.audit;
            let index = this.audit.length
            this.firstArr = this.audit[index-1]
            console.log(this.firstArr)
            console.log(this.audit, "audit")
            this.alert.Loader.hide()
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    toDate(dat: any) {
        let date = new Date(dat)
        let data = date.toLocaleDateString()
        return data
    }
    downloadFile(urlName) {
        debugger
        this.alert.Loader.show("Downloading..")
        let url = this.filePath + urlName;
        const fileTransfer: FileTransferObject = this.transfer.create();
        fileTransfer.download(url, this.filelocation + urlName).then((entry) => {
            console.log('download complete: ' + entry.toURL());
            this.alert.Toast.show("File Downloaded Successfully");
            this.alert.Loader.hide()
        }, (error) => {
            this.alert.Loader.hide()
            this.alert.Alert.alert("Can't Download")
            console.log("download error", error)
        });
    }
    checkfile(urlName) {
        this.file.checkFile(this.filelocation, urlName).then(_ => {
            console.log("file exit",urlName)
         this.fileOpen(urlName)
        }).catch((err) => {
            this.downloadFile(urlName)
            console.log(err)
        })
    }
    reqAudit() {
        this.alert.requestAudit().then((res) => {
            this.alert.Loader.show("Requesting..")
            let token: any = localStorage.getItem("tokens")
            this.user.requestAudit(token).subscribe((data: any) => {
                console.log(data, "audit")
                this.alert.Loader.hide()
                this.alert.Toast.show("Audit Request Sent")
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        }, err => {
            this.alert.Loader.hide();
            console.log(err)
        })
    }
    bytesToSize(bytes) {
        console.log(bytes)
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        let i = Math.floor(Math.log(bytes) / Math.log(1024));
        let ii = Math.pow(1024, i);
        return Math.round(bytes / ii) + ' ' + sizes[i]
    };

    fileOpen(file_name){
       let fileExtn= file_name.split('.').reverse()[0];
         let fileMIMEType=this.getMIMEtype(fileExtn);
         console.log(fileMIMEType)
         this.fileOpener.open(this.filelocation+ file_name+"", fileMIMEType)
                .then(() => console.log('File is opened'))
                .catch(e => console.log('Error openening file', e));
    }

    getMIMEtype(extn){
  let ext=extn.toLowerCase();
  let MIMETypes={
      'aac' : 'audio/aac',
    'abw' : 'application/x-abiword',
    'arc' : 'application/x-freearc',
    'avi' : 'video/x-msvideo',
    'azw' : 'application/vnd.amazon.ebook',
    'bin' : 'application/octet-stream',
    'bmp' : 'image/bmp',
    'bz' : 'application/x-bzip',
    'bz2' : 'application/x-bzip2',
    'csh' : 'application/x-csh',
    'css' : 'text/css',
    'csv' : 'text/csv',
    'doc' : 'application/msword',
    'docx' : 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'eot' : 'application/vnd.ms-fontobject',
    'epub' : 'application/epub+zip',
    'gif' : 'image/gif',
    'htm' : 'text/html',
    'html' : 'text/html',
    'ico' : 'image/vnd.microsoft.icon',
    'ics' : 'text/calendar',
    'jar' : 'application/java-archive',
    'jpeg' : 'image/jpeg',
    'jpg' : 'image/jpeg',
    'js' : 'text/javascript',
    'json' : 'application/json',
    'jsonld' : 'application/ld+json',
    'mid' : 'audio/midi',
    'midi' : 'audio/midi',
    'mjs' : 'text/javascript',
    'mp3' : 'audio/mpeg',
    'mpeg' : 'video/mpeg',
    'mpkg' : 'application/vnd.apple.installer+xml',
    'odp' : 'application/vnd.oasis.opendocument.presentation',
    'ods' : 'application/vnd.oasis.opendocument.spreadsheet',
    'odt' : 'application/vnd.oasis.opendocument.text',
    'oga' : 'audio/ogg',
    'ogv' : 'video/ogg',
    'ogx' : 'application/ogg',
    'otf' : 'font/otf',
    'png' : 'image/png',
    'pdf' : 'application/pdf',
    'ppt' : 'application/vnd.ms-powerpoint',
    'pptx' : 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'rar' : 'application/x-rar-compressed',
    'rtf' : 'application/rtf',
    'sh' : 'application/x-sh',
    'svg' : 'image/svg+xml',
    'swf' : 'application/x-shockwave-flash',
    'tar' : 'application/x-tar',
    'tif' : 'image/tiff',
    'tiff' : 'image/tiff',
    'ttf' : 'font/ttf',
    'txt' : 'text/plain',
    'vsd' : 'application/vnd.visio',
    'wav' : 'audio/wav',
    'weba' : 'audio/webm',
    'webm' : 'video/webm',
    'webp' : 'image/webp',
    'woff' : 'font/woff',
    'woff2' : 'font/woff2',
    'xhtml' : 'application/xhtml+xml',
    'xls' : 'application/vnd.ms-excel',
    'xlsx' : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'xml' : 'application/xml&nbsp;',
    'xul' : 'application/vnd.mozilla.xul+xml',
    'zip' : 'application/zip',
    '3gp' : 'video/3gpp',
    '3g2' : 'video/3gpp2',
    '7z' : 'application/x-7z-compressed' 
  }
  return MIMETypes[ext];
}


}