import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    Platform,
    ActionSheetController
} from 'ionic-angular';
import {
    LoadingController,
    ToastController
} from 'ionic-angular';
import {
    FileTransfer,
    FileUploadOptions,
    FileTransferObject
} from '@ionic-native/file-transfer';
import {
    Camera,
    CameraOptions
} from '@ionic-native/camera';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    Api
} from '../../providers/api/api';
import {
    NgZone
} from '@angular/core';
import {
    PhotoViewer
} from '@ionic-native/photo-viewer';
import {
    Events
} from 'ionic-angular';
/**
 * Generated class for the CertificateEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-certificate-edit',
    templateUrl: 'certificate-edit.html',
})
export class CertificateEditPage {
    data: any = {
        GST_image: '',
        Food_image: '',
        picName: ''
    }
    imageURIs = ""
    imageURI
    getData: any
    Food_certi
    GST_path
    imageURI1
    imageURIss
    adduser
    is_nonveg: boolean
    showDiv: any = false
    catt_upload_file: any = []
    Certificate: any = []
    certi_count: any
    certifi_delete: any = false
    anArrays: any = []
    Holidays: any = []
    public alertPresented: any;
    constructor(public events: Events, public photoViewer: PhotoViewer, public actionSheetCtrl: ActionSheetController, public zone: NgZone, public navCtrl: NavController, public navParams: NavParams, private transfer: FileTransfer, private camera: Camera, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public user: UserProvider, public alert: AlertProvider, public api: Api, public platform: Platform) {
        this.alertPresented = false
        this.Food_certi = this.api.Food_certificate
        this.GST_path = this.api.GST_certificate
        this.getData = this.navParams.get("data")
        console.log(this.getData, "wholedata")
        this.imageURI = this.getData.catt_gst_certificate;
        this.imageURIs = this.getData.catt_food_certificate;
        this.Certificate = this.getData.ceritificates
        console.log(this.Certificate, "certi")
        this.certi_count = this.Certificate.length
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad CertificateEditPage');
    }
    fullView(value) {
        console.log(value)
        this.photoViewer.show(this.GST_path + value);
    }
    fullViews(value) {
        console.log(value)
        this.photoViewer.show(this.Food_certi + value);
    }
    gstPath(val) {
        let data = this.GST_path + val;
        return data
    }
    otherCertificatePath(val) {
        let data = this.Food_certi + val;
        return data
    }
    backButtonAction() {
        this.navCtrl.pop()
    }
    randomFileName() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        var n = new Date()
        var date = n.toISOString();
        return text + "-" + date + ".png";
    }
    takeSnap() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        }
        this.camera.getPicture(options).then((imageData) => {
            this.imageURI1 = imageData;
            this.uploadFileGST();
            // this.imagebox = this.imageURI;
            // console.log(this.imagebox,"uri")
        }, (err) => {
            console.log(err);
            this.presentToast(err);
        });
    }
    uploadFileGST() {
        return new Promise((resolve, reject) => {
            let loader = this.loadingCtrl.create({
                content: "Uploading..."
            });
            loader.present();
            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
                fileKey: 'gstpic',
                fileName: this.randomFileName(),
                chunkedMode: false,
                mimeType: "image/png",
                headers: {}
            }
            let data = [];
            data.push(new Promise((resolve, reject) => {
                resolve(fileTransfer.upload(this.imageURI1, 'http://platos.in/api/catgst_image', options).then((data) => {
                    console.log(data, " Uploaded Successfully");
                    let temp = JSON.parse(data.response)
                    let imgURL = temp.data;
                    this.data.GST_image = imgURL.gstpic;
                    this.imageURI = imgURL.gstpic;
                    console.log(this.imageURI, "gst")
                    // this.data.menu_images.push(imgURL)
                    loader.dismiss();
                    // this.presentToast("Image uploaded successfully");
                }, (err) => {
                    console.log(err);
                    loader.dismiss();
                    // this.presentToast(err);
                }))
            }))
            Promise.all(data).then((values) => {
                resolve(values)
                this.updateCertificate()
            });
        })
    }
    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }
    deleteImage(index) {
        console.log(index)
        // if (index > -1) {
        // this.imagebox.splice(index, 1);
        this.imageURI = ''
        // }
    }
    deleteImages(index) {
        console.log(index)
        // if (index > -1) {
        // this.imageboxFood.splice(index, 1);
        this.imageURIs = ''
        // }
    }
    updateCertificate() {
        this.alert.Loader.show("Uploading..")
        console.log("in upload")
        let item = localStorage.getItem("tokens")
        if (this.getData.addi_user != null) {
            this.adduser = [{
                addi_cat_id: this.getData.addi_user.cat_usr_id,
                name: this.getData.addi_user.cat_usr_name,
                password: this.getData.addi_user.cat_usr_password,
                email: this.getData.addi_user.cat_usr_email,
                phone: this.getData.addi_user.cat_usr_mobile,
                addprofile: this.getData.addi_user.cat_usr_profile_pic
            }]
        } else {
            this.adduser = []
        }
        this.anArrays = this.getData.delivery_days
        if (this.anArrays.length > 0) {
            for (let i = 0; i < this.anArrays.length; i++) {
                if (!this.anArrays[i].fromtime.hour) {
                    if (this.anArrays[i].fromtime) {
                        let hour = this.anArrays[i].fromtime.substring(0, 2)
                        let minute = this.anArrays[i].fromtime.substring(3, 5)
                        let data = {
                            hour: '',
                            minute: ''
                        }
                        data.hour = hour
                        data.minute = minute
                        this.anArrays[i].fromtime = data
                    }
                }
                if (!this.anArrays[i].totime.hour) {
                    if (this.anArrays[i].totime) {
                        let hour = this.anArrays[i].totime.substring(0, 2)
                        let minute = this.anArrays[i].totime.substring(3, 5)
                        let data = {
                            hour: '',
                            minute: ''
                        }
                        data.hour = hour
                        data.minute = minute
                        this.anArrays[i].totime = data
                    }
                }
            }
        }
        if (this.getData.holidays.length > 0) {
            for (var i = 0; i < this.getData.holidays.length; ++i) {
                this.Holidays.push(this.getData.holidays[i].date)
            }
        } else {
            this.Holidays = this.getData.holidays;
        }
        let datas = {
            "catt_first_name": this.getData.catt_first_name,
            "catt_email_address": this.getData.catt_email_address,
            "catt_mobile_no": this.getData.catt_mobile_no,
            // "catt_sur_name": this.getData.catt_sur_name,
            // "catt_delivery_fromtime": this.getData.catt_delivery_fromtime,
            // "catt_delivery_totime": this.getData.catt_delivery_totime,
            // "catt_event_type": this.getData.catt_event_type,
            "catt_min_order": this.getData.catt_min_order,
            "catt_max_order": this.getData.catt_max_order,
            "catt_lead_time": this.getData.catt_lead_time,
            // "catt_score": this.getData.catt_score,
            "catt_gst_certificate": this.imageURI,
            // "catt_food_certificate": this.imageURIs,
            // "newuser": this.getData.newuser,
            "catt_address": this.getData.catt_address,
            "catt_id": this.getData.catt_id,
            "caterer_image": this.getData.caterer_image,
            // "caterer_group" : this.getData.caterer_group,
            "catt_add_users": this.adduser,
            "meal_types": this.getData.meal_types,
            "catt_catering_name": this.getData.catt_caterering_name,
            "catt_catering_info": this.getData.catt_caterering_info,
            "catt_delivery_pincode": this.getData.cust,
            "delivery_day": this.anArrays || [],
            "catt_upload_file": this.catt_upload_file,
            "holidays": this.Holidays,
            "gst_number": this.getData.gst_number
        }
        console.log("final data sent", datas)
        this.user.updateProfile(item, datas).subscribe(async (data: any) => {
            console.log(data, "response")
            console.log("api hit")
            let token: any = localStorage.getItem("tokens")
            let datas: any = {
                token: '',
            }
            datas.token = token;
            this.user.getCaterer(datas).subscribe((data: any) => {
                 this.events.publish('audit:status', data.data.audit_deactive);
                this.getData = data.data;
                this.imageURI = this.getData.catt_gst_certificate;
                this.imageURIs = this.getData.catt_food_certificate;
                this.Certificate = this.getData.ceritificates
                console.log(this.Certificate, "certi")
                this.certi_count = this.Certificate.length
                this.data.picName = ''
                this.imageURIs = ''
                // this.catt_upload_file = []
                console.log(this.getData, "see")
                this.presentToast("Image uploaded successfully");
                this.showDiv = false
                this.alert.Loader.hide();
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
            //   if(data.data.status == 'ok'){
            // await this.apihit()
            //   }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    apihit() {
        console.log("api hit")
        let token: any = localStorage.getItem("tokens")
        let data: any = {
            token: '',
        }
        data.token = token;
        this.user.getCaterer(data).subscribe((data: any) => {
             this.events.publish('audit:status', data.data.audit_deactive);
            this.getData = data.data;
            console.log(this.getData, "see")
            this.presentToast("Image uploaded successfully");
            this.alert.Loader.hide();
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    onChangeName() {
        console.log("typesd something")
    }
    getImage() {
        if (this.data.picName) {
            const options: CameraOptions = {
                quality: 100,
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
            }
            this.camera.getPicture(options).then((imageData) => {
                this.imageURIss = imageData;
                this.uploadFileFood()
            }, (err) => {
                console.log(err);
                this.presentToast(err);
            });
        } else {
            this.alert.Alert.alert("Please Enter The Certificate Name")
            return
        }
    }
    uploadFileFood() {
        return new Promise((resolve, reject) => {
            let loader = this.loadingCtrl.create({
                content: "Loading..."
            });
            loader.present();
            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
                fileKey: 'foodpic[]',
                fileName: this.randomFileName(),
                chunkedMode: false,
                mimeType: "image/png",
                headers: {}
            }
            let data = [];
            data.push(new Promise((resolve, reject) => {
                resolve(fileTransfer.upload(this.imageURIss, 'http://platos.in/api/catfood_image', options).then((data) => {
                    console.log(data, " Uploaded Successfully");
                    let temp = JSON.parse(data.response)
                    let imgURL = temp.data[0];
                    this.imageURIs = imgURL;
                    debugger
                    // }
                    // else{
                    //   this.catt_upload_file = []
                    //    this.catt_upload_file.push({"certify_name" : this.data.picName, "certify_file":this.imageURIs, "certify_id":''})
                    // }
                    // this.data.menu_images.push(imgURL)
                    loader.dismiss();
                }, (err) => {
                    console.log(err);
                    loader.dismiss();
                    // this.presentToast(err);
                }))
            }))
            Promise.all(data).then((values) => {
                resolve(values)
            });
        })
    }
    async uploadPic() {
        let data = {
            name: this.data.picName,
            img: this.imageURIs
        }
        debugger
        if (data.name == '') {
            this.alert.Alert.alert("Certificate Name Can't Be Empty")
            return
        }
        if (!data.img) {
            this.alert.Alert.alert("Please Upload the Certificate")
            return
        }
        let temp_info = {
            "certify_name": this.data.picName,
            "certify_file": this.imageURIs,
            "certify_id": ''
        }
        this.catt_upload_file = temp_info;
        console.log(data, "check file")
        this.updateCertificate()
    }
    // pageRefresh(){
    //  this.alert.Loader.show("Loading")    
    //       let token:any = localStorage.getItem("tokens")
    // let data:any = {
    //   token:'',
    // }
    // data.token = token;
    //       this.user.getCaterer(data).subscribe((data: any) => {
    //       this.getData = data.data;
    //          this.imageURI = this.getData.catt_gst_certificate;
    //    this.imageURIs = this.getData.catt_food_certificate;
    //    this.Certificate = this.getData.ceritificates
    //    console.log(this.Certificate,"certi")
    //    this.certi_count = this.Certificate.length
    //       this.alert.Loader.hide();
    //         }, (error) => {
    //             this.alert.Loader.hide();
    //             this.alert.Alert.alert("Cannot Connect To Server")
    //             return;
    //         })
    //  }
    show() {
        if (this.certifi_delete == false) {
            this.certifi_delete = true
        } else {
            this.certifi_delete = false
        }
    }
    async CertificateDelete(certi: any) {
        console.log(certi)
        this.alert.Loader.show("Loading")
        this.user.deleteOtherCertificates(certi.certify_id).subscribe((data: any) => {
            if (data.status == "ok") {
                let token: any = localStorage.getItem("tokens")
                let data: any = {
                    token: '',
                }
                data.token = token;
                this.user.getCaterer(data).subscribe((data: any) => {
                     this.events.publish('audit:status', data.data.audit_deactive);
                    this.getData = data.data;
                    this.imageURI = this.getData.catt_gst_certificate;
                    this.imageURIs = this.getData.catt_food_certificate;
                    this.Certificate = this.getData.ceritificates
                    console.log(this.Certificate, "certi")
                    this.certi_count = this.Certificate.length
                    this.alert.Toast.show("Certificate Deleted")
                    this.certifi_delete = false
                    this.alert.Loader.hide();
                }, (error) => {
                    this.alert.Loader.hide();
                    this.alert.Alert.alert("Cannot Connect To Server")
                    return;
                })
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                this.certifi_delete = false
                return
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            this.certifi_delete = false
            return;
        })
    }
    public GSTCertificateDelete() {
        this.alertPresented = true
        localStorage.setItem("alert", this.alertPresented);
        let actionSheet = this.actionSheetCtrl.create({
            title: 'GST Certificate',
            buttons: [{
                text: 'Delete',
                icon: 'trash',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                    this.deleteGst()
                }
            }, {
                text: 'Cancel',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                    this.certifi_delete = false
                }
            }],
        });
        actionSheet.present();
    }
    public OtherCertificateDelete(name: any) {
        this.alertPresented = true
        localStorage.setItem("alert", this.alertPresented);
        let actionSheet = this.actionSheetCtrl.create({
            title: name.certify_file,
            buttons: [{
                text: 'Delete',
                icon: 'trash',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                    this.CertificateDelete(name)
                }
            }, {
                text: 'Cancel',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                    this.certifi_delete = false
                }
            }],
        });
        actionSheet.present();
    }
    deleteGst() {
        let token = localStorage.getItem("tokens")
        let data: any = {
            ceritificate: "gst",
            token: token
        }
        console.log(data)
        // this.alert.presentDelete().then((res) => { 
        this.user.deleteCertificate(data).subscribe((data: any) => {
            if (data.status == "ok") {
                this.alert.Loader.hide();
                // this.alert.Toast.show("Deleted Successfully")
                this.certifi_delete = false
                let token: any = localStorage.getItem("tokens")
                let data: any = {
                    token: '',
                }
                data.token = token;
                this.user.getCaterer(data).subscribe((data: any) => {
                     this.events.publish('audit:status', data.data.audit_deactive);
                    this.getData = data.data;
                    this.imageURI = this.getData.catt_gst_certificate;
                    this.imageURIs = this.getData.catt_food_certificate;
                    this.Certificate = this.getData.ceritificates
                    console.log(this.Certificate, "certi")
                    this.certi_count = this.Certificate.length
                    this.alert.Toast.show("Certificate Deleted")
                    this.alert.Loader.hide();
                }, (error) => {
                    this.alert.Loader.hide();
                    this.alert.Alert.alert("Cannot Connect To Server")
                    this.certifi_delete = false
                    return;
                })
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                this.certifi_delete = false
                return
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            this.certifi_delete = false
            return;
        })
        //   }, err => {
        // this.alert.Loader.hide();
        //      console.log(err)
        this.certifi_delete = false
        // }) 
    }
}