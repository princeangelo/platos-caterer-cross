import {
    Component,
    ViewChild,
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    ActionSheetController,
    Platform
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    NgZone
} from '@angular/core';
import {
    Api
} from '../../providers/api/api';
import {
    Content
} from 'ionic-angular';
import {
    PhotoViewer
} from '@ionic-native/photo-viewer';
import {
    Events
} from 'ionic-angular';
@IonicPage()
@Component({
    selector: 'page-vendor',
    templateUrl: 'vendor.html',
})
export class VendorPage {
    @ViewChild(Content) content: Content;
    vendorData: any
    Address: any
    Food_certi: any
    GST_path: any
    is_Admin: any
    Category: any = ""
    catge_flag = false
    showMinus = false
    groupname: any = ""
    showaddGroup: any = false;
    GroupNames: any
    Gropmenus: any
    ShowMinus: any = false
    certifi_delete: any = false
    pincodes: any
    DeliveryDays: any
    Certificate: any = []
    Holidays: any
    Cater_Img: any
    public alertPresented: any;
    Golive: any = false
    status: any
    show: any = false
    hasmorethanfive: any = false
    hasmorethanfifteen: any = false
    showAllPin: any = false
    showLess: any = false
    anArrays: any
    constructor(public events: Events, public photoViewer: PhotoViewer, public platform: Platform, public actionSheetCtrl: ActionSheetController, public zone: NgZone, public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider, public api: Api) {
        this.is_Admin = localStorage.getItem("is_admin")
        this.Cater_Img = this.api.filepath;
        this.alertPresented = false
    }
    catPath(val) {
        let data = this.Cater_Img + val;
        return data;
    }
    ionViewDidEnter() {
        this.alert.Loader.show("Loading..")
        this.Food_certi = this.api.Food_certificate
        this.GST_path = this.api.GST_certificate
        this.zone.run(() => {
            let token: any = localStorage.getItem("tokens")
            let data: any = {
                token: '',
            }
            data.token = token;
            this.user.getCaterer(data).subscribe((data: any) => {
                this.events.publish('audit:status', data.data.audit_deactive);
                this.vendorData = data.data;

                let name = this.vendorData.catt_first_name
                let mail = this.vendorData.catt_email_address
                this.events.publish('login:created', name, mail);

                if (this.vendorData.is_updated == 1) {
                    this.Golive = true
                    this.show = true
                    this.status = "GO LIVE"
                } else if (this.vendorData.is_updated == 2) {
                    this.Golive = false
                    this.show = true
                    this.status = "PENDING"
                } else if (this.vendorData.is_updated == 0) {
                    this.Golive = false
                    this.status = ""
                    this.show = false
                }
                if (this.vendorData.pincode_changed == true) {
                    this.Address = this.vendorData.old_pincode;
                } else {
                    this.Address = this.vendorData.new_pincode;
                }
                this.DeliveryDays = this.vendorData.delivery_days
                this.Holidays = this.vendorData.holidays
                this.Certificate = this.vendorData.ceritificates
                let temp = []
                if (this.Address.length > 0) {
                    if (this.Address.length > 5) {
                        for (let i = 0; i < 5; i++) {
                            temp.push(this.Address[i].area.area_name + "-" + this.Address[i].area.pincode)
                        }
                        this.pincodes = temp
                        this.hasmorethanfive = true
                    } else {
                        for (let i = 0; i < this.Address.length; i++) {
                            temp.push(this.Address[i].area.area_name + "-" + this.Address[i].area.pincode)
                        }
                        this.pincodes = temp
                    }
                }
                console.log(this.vendorData, "see")
                this.alert.Loader.hide();
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
            // let toke = localStorage.getItem("tokens")
            //   this.user.getGroupList(toke).subscribe((data: any) => {
            //  console.log(data,"group List")
            //  this.GroupNames = data.data;
            //  debugger
            //  if(data.data.length > 0){
            //    this.Gropmenus = data.data[0].menu_group
            //  }
            // this.alert.Loader.hide();
            //   }, (error) => {
            //       this.alert.Loader.hide();
            //       this.alert.Alert.alert("Cannot Connect To Server")
            //       return;
            //   })
        })
    }
    addUser() {
        this.navCtrl.push("AddAdditionaluserPage", {
            data: this.vendorData,
            edit: false
        })
    }
    showMore() {
        let temp = []
        if (this.Address.length > 15) {
            for (var j = 0; j < 15; ++j) {
                temp.push(this.Address[j].area.area_name + "-" + this.Address[j].area.pincode)
            }
            this.pincodes = temp
            this.hasmorethanfive = false
            this.hasmorethanfifteen = true
        } else {
            for (var k = 0; k < this.Address.length; ++k) {
                temp.push(this.Address[k].area.area_name + "-" + this.Address[k].area.pincode)
            }
            this.pincodes = temp
            this.hasmorethanfive = false
            this.hasmorethanfifteen = false
        }
    }
    showAll() {
        let temp = []
        if (this.hasmorethanfifteen == true) {
            for (var l = 0; l < this.Address.length; ++l) {
                temp.push(this.Address[l].area.area_name + "-" + this.Address[l].area.pincode)
            }
            this.pincodes = temp
            this.hasmorethanfifteen = false
            this.showLess = true
        }
    }
    showless() {
        let temp = []
        this.showLess = false
        this.hasmorethanfifteen = false
        this.hasmorethanfive = false
        if (this.Address.length > 0) {
            if (this.Address.length > 5) {
                for (let i = 0; i < 5; i++) {
                    temp.push(this.Address[i].area.area_name + "-" + this.Address[i].area.pincode)
                }
                this.pincodes = temp
                this.hasmorethanfive = true
            } else {
                for (let i = 0; i < this.Address.length; i++) {
                    temp.push(this.Address[i].area.area_name + "-" + this.Address[i].area.pincode)
                }
                this.pincodes = temp
            }
        }
    }
    fullView(value) {
        console.log(value)
        this.photoViewer.show(this.GST_path + value);
    }
    fullViews(value) {
        console.log(value)
        this.photoViewer.show(this.Food_certi + value);
    }
    fullViewCat(value) {
        console.log(value)
        this.photoViewer.show(this.Cater_Img + value);
    }
    gstPath(val) {
        let data = this.GST_path + val;
        return data
    }
    otherCertificatePath(val) {
        let data = this.Food_certi + val;
        return data
    }
    deleteadditional() {
        let AddUser = []
        let temp = {
            addi_cat_id: this.vendorData.addi_user.cat_usr_id,
            name: '',
            email: '',
            phone: '',
            addprofile: ''
        }
        AddUser.push(temp)
        let item = localStorage.getItem("tokens")
        this.anArrays = this.vendorData.delivery_days
        if (this.anArrays.length > 0) {
            for (let i = 0; i < this.anArrays.length; i++) {
                if (!this.anArrays[i].fromtime.hour) {
                    if (this.anArrays[i].fromtime) {
                        let hour = this.anArrays[i].fromtime.substring(0, 2)
                        let minute = this.anArrays[i].fromtime.substring(3, 5)
                        let data = {
                            hour: '',
                            minute: ''
                        }
                        data.hour = hour
                        data.minute = minute
                        this.anArrays[i].fromtime = data
                    }
                }
                if (!this.anArrays[i].totime.hour) {
                    if (this.anArrays[i].totime) {
                        let hour = this.anArrays[i].totime.substring(0, 2)
                        let minute = this.anArrays[i].totime.substring(3, 5)
                        let data = {
                            hour: '',
                            minute: ''
                        }
                        data.hour = hour
                        data.minute = minute
                        this.anArrays[i].totime = data
                    }
                }
            }
        }
        let datas = {
            "catt_first_name": this.vendorData.catt_first_name,
            "catt_email_address": this.vendorData.catt_email_address,
            "catt_mobile_no": this.vendorData.catt_mobile_no,
            // "catt_sur_name": this.vendorData.catt_sur_name,
            // "DeliveryTime_from": this.vendorData.catt_delivery_fromtime,
            // "DeliveryTime_to": this.vendorData.catt_delivery_totime,
            // "catt_event_type": this.vendorData.catt_event_type,
            "catt_min_order": this.vendorData.catt_min_order,
            "catt_max_order": this.vendorData.catt_max_order,
            "catt_lead_time": this.vendorData.catt_lead_time,
            // "catt_score": this.vendorData.catt_score,
            "catt_gst_certificate": this.vendorData.catt_gst_certificate,
            // "catt_food_certificate": this.vendorData.catt_food_certificate,
            // "newuser": this.vendorData.newuser,
            "catt_address": this.vendorData.catt_address,
            "catt_id": this.vendorData.catt_id,
            // "caterer_group" : this.vendorData.caterer_group,
            "catt_add_users": AddUser,
            "meal_types": this.vendorData.meal_types,
            "catt_catering_name": this.vendorData.catt_caterering_name,
            // "catt_catering_info":this.vendorData.catt_caterering_info,
            "catt_delivery_pincode": this.vendorData.cust,
            "delivery_day": this.anArrays || [],
            "catt_upload_file": this.vendorData.catt_upload_file || []
        }
        console.log("after add user", datas)
        this.user.updateProfile(item, datas).subscribe((data: any) => {
            console.log(data)
            if (data.status == 'ok') {
                this.zone.run(() => {
                    let token: any = localStorage.getItem("tokens")
                    let data: any = {
                        token: '',
                    }
                    data.token = token;
                    this.user.getCaterer(data).subscribe((data: any) => {
                         this.events.publish('audit:status', data.data.audit_deactive);
                        this.vendorData = data.data;
                        debugger
                        if (this.vendorData.is_updated == 1) {
                            this.Golive = true
                            this.show = true
                            this.status = "GO LIVE"
                        } else if (this.vendorData.is_updated == 2) {
                            this.Golive = false
                            this.show = true
                            this.status = "PENDING"
                        } else if (this.vendorData.is_updated == 0) {
                            this.Golive = false
                            this.status = ""
                            this.show = false
                        }
                        if (this.vendorData.pincode_changed == true) {
                            this.Address = this.vendorData.old_pincode;
                        } else {
                            this.Address = this.vendorData.new_pincode;
                        }
                        this.DeliveryDays = this.vendorData.delivery_days
                        this.Holidays = this.vendorData.holidays
                        this.Certificate = this.vendorData.ceritificates
                        let temp = []
                        this.showLess = false
                        this.hasmorethanfifteen = false
                        this.hasmorethanfive = false
                        if (this.Address.length > 0) {
                            if (this.Address.length > 5) {
                                for (let i = 0; i < 5; i++) {
                                    temp.push(this.Address[i].area.area_name + "-" + this.Address[i].area.pincode)
                                }
                                this.pincodes = temp
                                this.hasmorethanfive = true
                            } else {
                                for (let i = 0; i < this.Address.length; i++) {
                                    temp.push(this.Address[i].area.area_name + "-" + this.Address[i].area.pincode)
                                }
                                this.pincodes = temp
                            }
                        }
                        this.alert.Toast.show("Additional Deleted Successfully")
                        console.log(this.vendorData, "see")
                        this.alert.Loader.hide();
                    }, (error) => {
                        this.alert.Loader.hide();
                        this.alert.Alert.alert("Cannot Connect To Server")
                        return;
                    })
                })
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    editProfile() {
        this.navCtrl.push("VendorProfileEditPage", {
            data: this.vendorData,
            from: "vendor"
        })
    }
    showDelete() {
        if (this.certifi_delete == false) {
            this.certifi_delete = true
        } else {
            this.certifi_delete = false
        }
    }
    refreshVendor() {
        this.zone.run(() => {
            let token: any = localStorage.getItem("tokens")
            let data: any = {
                token: '',
            }
            data.token = token;
            this.user.getCaterer(data).subscribe((data: any) => {
                 this.events.publish('audit:status', data.data.audit_deactive);
                this.vendorData = data.data;
                this.Address = this.vendorData.cust;
                console.log(this.vendorData, "see")
                this.alert.Loader.hide();
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        })
    }
    deleteFood() {
        let token = localStorage.getItem("tokens")
        let data: any = {
            ceritificate: "food",
            token: token
        }
        console.log(data)
        // this.alert.presentDelete().then((res) => { 
        this.user.deleteCertificate(data).subscribe((data: any) => {
            if (data.status == "ok") {
                this.alert.Loader.hide();
                this.alert.Toast.show("Deleted Successfully")
                this.certifi_delete = false
                this.refreshVendor()
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        // }, err => {
        //  this.alert.Loader.hide();
        //       console.log(err)
        this.certifi_delete = false
        // })
    }
    deleteGst() {
        let token = localStorage.getItem("tokens")
        let data: any = {
            ceritificate: "gst",
            token: token
        }
        console.log(data)
        // this.alert.presentDelete().then((res) => { 
        this.user.deleteCertificate(data).subscribe((data: any) => {
            if (data.status == "ok") {
                this.alert.Loader.hide();
                this.alert.Toast.show("Deleted Successfully")
                this.certifi_delete = false
                this.refreshVendor()
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        //   }, err => {
        // this.alert.Loader.hide();
        //      console.log(err)
        this.certifi_delete = false
        // }) 
    }
    convertTime(value: any) {
        debugger
        // let hour = value.substring(0,2)
        // let minute = value.substring(3,5)
        // if(hour < 10)
        // {
        //   hour = "0"+hour;
        // }
        // if(minute < 10){
        //   minute = minute
        // }
        var timeString = value + ":00"
        var hourEnd = timeString.indexOf(":");
        var H = +timeString.substr(0, hourEnd);
        var h = H % 12 || 12;
        var ampm = (H < 12 || H === 24) ? "AM" : "PM";
        timeString = h + timeString.substr(hourEnd, 3) + ampm;
        return timeString
    }
    editCater() {
        this.navCtrl.push("CateringInfoEditPage", {
            data: this.vendorData,
        })
    }
    editAddedUser() {
        this.navCtrl.push("AddAdditionaluserPage", {
            edit: true,
            data: this.vendorData,
            from: "vendor"
        })
    }
    editCertificate() {
        this.navCtrl.push("CertificateEditPage", {
            data: this.vendorData
        })
    }
    onChangeName() {
        console.log(this.vendorData)
    }
    addCate() {
        if (this.catge_flag == false) {
            this.catge_flag = true
        } else {
            this.catge_flag = false
            this.Category = ""
        }
    }
    addmenuGroup() {
        if (this.showaddGroup == false) {
            this.showaddGroup = true
        } else {
            this.groupname = ""
            this.showaddGroup = false
        }
    }
    showIcon() {
        if (this.showMinus == false) {
            this.showMinus = true;
        } else {
            this.showMinus = false;
        }
    }
    deleteCate(value: any) {
        console.log(value.category_id)
        // this.alert.presentDelete().then((res) => { 
        this.alert.Loader.show("Deleting..")
        this.user.deleteCate(value.category_id).subscribe((data: any) => {
            console.log(data)
            if (data.status == "ok") {
                this.alert.Loader.hide()
                this.zone.run(() => {
                    let token: any = localStorage.getItem("tokens")
                    let data: any = {
                        token: '',
                    }
                    data.token = token;
                    this.user.getCaterer(data).subscribe((data: any) => {
                         this.events.publish('audit:status', data.data.audit_deactive);
                        this.vendorData = data.data;
                        this.Address = this.vendorData.cust;
                        console.log(this.vendorData, "see")
                        this.showMinus = false;
                        this.alert.Toast.show("Removed Successfully")
                        this.alert.Loader.hide();
                    }, (error) => {
                        this.alert.Loader.hide();
                        this.alert.Alert.alert("Cannot Connect To Server")
                        return;
                    })
                })
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        //        }, err => {
        // this.alert.Loader.hide();
        //      console.log(err)
        this.showMinus = false;
        // }) 
    }
    editMenu() {
        this.navCtrl.push("MenugroupPage")
    }
    deleteGr() {
        if (this.ShowMinus == false) {
            this.ShowMinus = true;
        } else {
            this.ShowMinus = false
        }
    }
    scrollToBottom() {
        this.content.scrollToBottom();
    }
    public FoodCertificateDelete() {
        this.alertPresented = true
        localStorage.setItem("alert", this.alertPresented);
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Food Certificate',
            buttons: [{
                text: 'Delete',
                icon: 'trash',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                    this.deleteFood()
                }
            }, {
                text: 'Cancel',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                    this.certifi_delete = false
                }
            }]
        });
        actionSheet.present();
    }
    public GSTCertificateDelete() {
        this.alertPresented = true
        localStorage.setItem("alert", this.alertPresented);
        let actionSheet = this.actionSheetCtrl.create({
            title: 'GST Certificate',
            buttons: [{
                text: 'Delete',
                icon: 'trash',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                    this.deleteGst()
                }
            }, {
                text: 'Cancel',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                    this.certifi_delete = false
                }
            }],
        });
        actionSheet.present();
    }
    public DeleteAdditionalUser() {
        this.alertPresented = true
        localStorage.setItem("alert", this.alertPresented);
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Additional User',
            buttons: [{
                text: 'Delete',
                icon: 'trash',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                    this.deleteadditional()
                }
            }, {
                text: 'Cancel',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                }
            }],
        });
        actionSheet.present();
    }
    convertDay(val) {
        let data = {
            mon: "Mon",
            tue: "Tue",
            wed: "Wed",
            thu: "Thu",
            fri: "Fri",
            sat: "Sat",
            sun: "Sun"
        }
        if (val == 1) {
            return data.mon
        } else if (val == 2) {
            return data.tue
        } else if (val == 3) {
            return data.wed
        } else if (val == 4) {
            return data.thu
        } else if (val == 5) {
            return data.fri
        } else if (val == 6) {
            return data.sat
        } else if (val == 7) {
            return data.sun
        }
    }
    DateConv(val) {
        let date = val.substring(0, 10)
        return date
    }
    vendorLive() {
        this.alert.goLiveAlert().then((res) => {
            let toke = localStorage.getItem("tokens")
            this.user.vendoegoLive(toke).subscribe((data: any) => {
                if (data.status == "ok") {
                    this.alert.Toast.show("Go Live Requested")
                } else {
                    this.alert.Alert.alert(data.data)
                    return
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.ShowMinus = false;
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
            this.alert.Loader.show("Loading..")
            this.Food_certi = this.api.Food_certificate
            this.GST_path = this.api.GST_certificate
            this.zone.run(() => {
                let token: any = localStorage.getItem("tokens")
                let data: any = {
                    token: '',
                }
                data.token = token;
                this.user.getCaterer(data).subscribe((data: any) => {
                     this.events.publish('audit:status', data.data.audit_deactive);
                    this.vendorData = data.data;
                    debugger
                    if (this.vendorData.is_updated == 1) {
                        this.Golive = true
                        this.status = "GO LIVE"
                    } else if (this.vendorData.is_updated == 2) {
                        this.Golive = false
                        this.status = "PENDING"
                    }
                    if (this.vendorData.pincode_changed == true) {
                        this.Address = this.vendorData.old_pincode;
                    } else {
                        this.Address = this.vendorData.new_pincode;
                    }
                    this.DeliveryDays = this.vendorData.delivery_days
                    this.Holidays = this.vendorData.holidays
                    this.Certificate = this.vendorData.ceritificates
                    let temp = []
                    this.showLess = false
                    this.hasmorethanfifteen = false
                    this.hasmorethanfive = false
                    if (this.Address.length > 0) {
                        if (this.Address.length > 5) {
                            for (let i = 0; i < 5; i++) {
                                temp.push(this.Address[i].area.area_name + "-" + this.Address[i].area.pincode)
                            }
                            this.pincodes = temp
                            this.hasmorethanfive = true
                        } else {
                            for (let i = 0; i < this.Address.length; i++) {
                                temp.push(this.Address[i].area.area_name + "-" + this.Address[i].area.pincode)
                            }
                            this.pincodes = temp
                        }
                    }
                    console.log(this.vendorData, "see")
                    this.alert.Loader.hide();
                }, (error) => {
                    this.alert.Loader.hide();
                    this.alert.Alert.alert("Cannot Connect To Server")
                    return;
                })
            })
        }, err => {
            this.alert.Loader.hide();
            console.log(err)
        })
    }
}