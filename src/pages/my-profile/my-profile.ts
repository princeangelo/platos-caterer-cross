import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    ToastController,
    ActionSheetController,
    Platform,
    LoadingController,
    Loading
} from 'ionic-angular';
import {
    FileTransfer,
    FileUploadOptions,
    FileTransferObject
} from '@ionic-native/file-transfer';
import {
    Camera,
    CameraOptions
} from '@ionic-native/camera';
import {
    Api
} from '../../providers/api/api';
import {
    Events
} from 'ionic-angular';
@IonicPage()
@Component({
    selector: 'page-my-profile',
    templateUrl: 'my-profile.html',
})
export class MyProfilePage {
    imageURI1
    imageURI
    profileDatas: any
    address: any
    imgPath: any
    is_Admin: any
    lastImage: string = null;
    loading: Loading;
    pincodes: any
    constructor(public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider, public api: Api, private transfer: FileTransfer, private camera: Camera, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public events: Events, public actionSheetCtrl: ActionSheetController, public platform: Platform) {
        this.is_Admin = localStorage.getItem("is_admin")
        this.imgPath = this.api.profile_pic;
        let token: any = localStorage.getItem("tokens")
        let data: any = {
            token: '',
        }
        data.token = token;
        this.alert.Loader.show("Loading")
        this.user.getCaterer(data).subscribe((data: any) => {
             this.events.publish('audit:status', data.data.audit_deactive);
            this.profileDatas = data.data;
            // this.address = this.profileDatas.cust;
            //       let temp  = []
            // for(let i = 0;i<this.address.length;i++){
            // temp.push(this.address[i].catt_pincode)
            // }
            // this.pincodes = temp.join();
            console.log(this.profileDatas, "see")
            this.alert.Loader.hide()
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    public presentActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [{
                text: 'Library',
                icon: 'folder',
                cssClass: 'EditionIcon',
                handler: () => {
                    this.takeSnap(this.camera.PictureSourceType.PHOTOLIBRARY);
                }
            }, {
                text: 'Use Camera',
                icon: 'camera',
                cssClass: 'EditionIcon',
                handler: () => {
                    this.takeSnap(this.camera.PictureSourceType.CAMERA);
                }
            }, {
                text: 'Cancel',
                role: 'cancel',
                icon: 'close',
                cssClass: 'EditionIcon',
            }]
        });
        actionSheet.present();
    }
    // ionViewWillEnter(){
    //    this.platform.registerBackButtonAction(() => {
    //             this.navCtrl.setRoot(DashboardPage)
    //         })
    // }
    /*********************************************************************************************************/
    ionViewDidLoad() {}
    backButtonAction() {
        this.alert.presentConfirmExit().then((res) => {
            this.platform.exitApp();
        }, err => {})
    }
    change() {
        this.navCtrl.push("ChangepasswordPage")
    }
    editProfile() {
        if (this.is_Admin == "true") {
            this.navCtrl.push("VendorProfileEditPage", {
                data: this.profileDatas,
                from: "profile"
            })
        } else {
            this.navCtrl.push("AddAdditionaluserPage", {
                data: this.profileDatas,
                edit: true,
                from: "profile"
            })
        }
    }
    randomFileName() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        var n = new Date()
        var date = n.toISOString();
        return text + "-" + date + ".png";
    }
    takeSnap(source) {
        const options: CameraOptions = {
            quality: 40,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: source,
            cameraDirection: 1,
            correctOrientation: true,
        }
        this.camera.getPicture(options).then((imageData) => {
            this.imageURI1 = imageData;
            this.uploadFileProfile();
            // this.imagebox = this.imageURI;
            // console.log(this.imagebox,"uri")
        }, (err) => {
            console.log(err);
            this.presentToast(err);
        });
    }
    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }
    uploadFileProfile() {
        return new Promise((resolve, reject) => {
            let loader = this.loadingCtrl.create({
                content: "Uploading..."
            });
            loader.present();
            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
                fileKey: 'profile_pic',
                fileName: this.randomFileName(),
                chunkedMode: false,
                mimeType: "image/png",
                headers: {}
            }
            let data = [];
            data.push(new Promise((resolve, reject) => {
                resolve(fileTransfer.upload(this.imageURI1, 'http://platos.in/api/catt_profile', options).then((data) => {
                    console.log(data, " Uploaded Successfully");
                    let temp = JSON.parse(data.response)
                    let imgURL = temp.data;
                    this.imageURI = imgURL.profile_pic;
                    console.log(this.imageURI, "profile")
                    // this.data.menu_images.push(imgURL)
                    loader.dismiss();
                    // this.presentToast("Image uploaded successfully");
                }, (err) => {
                    console.log(err);
                    loader.dismiss();
                    // this.presentToast(err);
                }))
            }))
            Promise.all(data).then((values) => {
                resolve(values)
                this.updateProfilePic()
            });
        })
    }
    updateProfilePic() {
        // this.alert.Loader.show("Uploading..")  
        console.log("in upload")
        let item = localStorage.getItem("tokens")
        if (this.is_Admin == "true") {
            debugger
            this.profileDatas.catt_profile_pic = this.imageURI;
            let adduser = []
            if (this.profileDatas.addi_user) {
                let user: any = {
                    addi_cat_id: this.profileDatas.addi_user.cat_usr_id,
                    name: this.profileDatas.addi_user.cat_usr_name,
                    password: this.profileDatas.addi_user.cat_usr_password,
                    email: this.profileDatas.addi_user.cat_usr_email,
                    phone: this.profileDatas.addi_user.cat_usr_mobile,
                    addprofile: this.profileDatas.addi_user.cat_usr_profile_pic
                }
                adduser.push(user)
            }
            this.profileDatas.catt_address = this.profileDatas.catt_address
            this.profileDatas.catt_add_users = adduser
            this.profileDatas.catt_delivery_pincode = this.profileDatas.cust
            this.profileDatas.catt_catering_name = this.profileDatas.catt_caterering_name
            this.profileDatas.catt_catering_info = this.profileDatas.catt_caterering_info
            this.profileDatas.DeliveryTime_from = this.profileDatas.catt_delivery_fromtime
            this.profileDatas.DeliveryTime_to = this.profileDatas.catt_delivery_totime
        } else {
            debugger
            let adduser = []
            let user: any = {
                addi_cat_id: this.profileDatas.addi_user.cat_usr_id,
                name: this.profileDatas.addi_user.cat_usr_name,
                password: this.profileDatas.addi_user.cat_usr_password,
                email: this.profileDatas.addi_user.cat_usr_email,
                phone: this.profileDatas.addi_user.cat_usr_mobile,
                addprofile: this.imageURI
            }
            adduser.push(user)
            this.profileDatas.catt_address = this.profileDatas.catt_address
            this.profileDatas.catt_add_users = adduser
            this.profileDatas.catt_delivery_pincode = this.profileDatas.cust
            this.profileDatas.catt_catering_name = this.profileDatas.catt_caterering_name
            this.profileDatas.catt_catering_info = this.profileDatas.catt_caterering_info
            this.profileDatas.DeliveryTime_from = this.profileDatas.catt_delivery_fromtime
            this.profileDatas.DeliveryTime_to = this.profileDatas.catt_delivery_totime
        }
        this.user.updateProfile(item, this.profileDatas).subscribe(async (data: any) => {
            console.log(data, "response")
            console.log("api hit")
            let token: any = localStorage.getItem("tokens")
            let datas: any = {
                token: '',
            }
            datas.token = token;
            this.user.getCaterer(datas).subscribe((data: any) => {
                 this.events.publish('audit:status', data.data.audit_deactive);
                this.profileDatas = data.data;
                this.address = this.profileDatas.cust;
                let temp = []
                for (let i = 0; i < this.address.length; i++) {
                    temp.push(this.address[i].catt_pincode)
                }
                this.pincodes = temp.join();
                // let profile
                // if (this.is_Admin == "true") {
                //     profile = this.profileDatas.catt_profile_pic
                //     this.events.publish('profilepic:created', profile);
                //     localStorage.setItem("pic", profile)
                // } else {
                //     profile = this.profileDatas.addi_user.cat_usr_profile_pic;
                //     this.events.publish('profilepic:created', profile);
                //     localStorage.setItem("pic", profile)
                // }
                // this.presentToast("Image uploaded successfully");
                this.alert.Loader.hide();
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
            //   if(data.data.status == 'ok'){
            // await this.apihit()
            //   }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    showPic(value) {
        let data = this.imgPath + value;
        return data
    }
    createUser(user) {
        console.log('User created!')
        this.events.publish('user:created', user, Date.now());
    }
}