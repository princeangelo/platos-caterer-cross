import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CateringInfoEditPage } from './catering-info-edit';
import {CalendarModule} from "ion2-calendar";

@NgModule({
  declarations: [
    CateringInfoEditPage,
  ],
  imports: [
    CalendarModule,
    IonicPageModule.forChild(CateringInfoEditPage),
  ],
})
export class CateringInfoEditPageModule {}
