import {
    Component,
    ViewChild,
    ElementRef
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    Platform,
    ActionSheetController
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    CalendarComponentOptions
} from 'ion2-calendar'
import {
    LoadingController,
    ToastController
} from 'ionic-angular';
import {
    FileTransfer,
    FileUploadOptions,
    FileTransferObject
} from '@ionic-native/file-transfer';
import {
    Camera,
    CameraOptions
} from '@ionic-native/camera';
import {
    CalendarModal,
    CalendarModalOptions,
    DayConfig,
    CalendarResult
} from "ion2-calendar";
import {
    Api
} from '../../providers/api/api';
import {
    DomSanitizer
} from '@angular/platform-browser';
import {
    PhotoViewer
} from '@ionic-native/photo-viewer';
import * as moment from 'moment';
@IonicPage()
@Component({
    selector: 'page-catering-info-edit',
    templateUrl: 'catering-info-edit.html',
})
export class CateringInfoEditPage {
    @ViewChild('myInput') myInput: ElementRef;
    data: any = {
        // DeliveryTime_from:'',
        // DeliveryTime_to:'',
        catt_lead_time: '',
        catt_min_order: '',
        catt_max_order: '',
        GST_no: '',
        meal_types: '',
        catt_address: '',
        catt_catering_name: '',
        holiday: '',
        catt_caterering_info: '',
        gst_number: ''
    }
    error: any = {
        from_time: '',
        lead_time: '',
        min_order: '',
        max_order: '',
        meal_types: '',
        catt_address: '',
        catt_catering_name: '',
        catt_caterering_info: '',
        gst_number: ''
    }
    caterData: any
    anArray: any = []
    anArrays: any = []
    adduser: any
    AddressCount: any = 0
    DaysCount: any = 0
    DayNames: any = [{
        "day": 7,
        "name": "Sun"
    }, {
        "day": 1,
        "name": "Mon"
    }, {
        "day": 2,
        "name": "Tue"
    }, {
        "day": 3,
        "name": "Wed"
    }, {
        "day": 4,
        "name": "Thu"
    }, {
        "day": 5,
        "name": "Fri"
    }, {
        "day": 6,
        "name": "Sat"
    }, ]
    holiday: any = []
    imageURI1
    imageURI
    x = new Date('01/01/2019')
    dateMulti = [];
    // type: 'object'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
    optionsMulti: CalendarComponentOptions = {
        pickMode: 'multi',
        from: this.x
    };
    showCal: any = false;
    temp = []
    tempor: any = []
    ImgPath: any
    NewPic: any = false
    fromApi: any
    public alertPresented: any;
    pincodes: any
    Area_Pincodes: any = []
    selectOptions2: any
    selectOptions3: any
    selectOptions4: any
    constructor(public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, private camera: Camera, public api: Api, public photoViewer: PhotoViewer, public _DomSanitizer: DomSanitizer, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public actionSheetCtrl: ActionSheetController, private transfer: FileTransfer, public alert: AlertProvider, public platform: Platform) {
        this.caterData = this.navParams.get("data")
        console.log(this.caterData, "got the data")
        this.ImgPath = this.api.filepath;
        this.selectOptions2 = {
            title: 'Days',
        };
        this.selectOptions3 = {
            title: 'Delivery Areas',
            subTitle: '(Scroll for more)',
        };
        this.selectOptions4 = {
            title: 'Vegetarian',
        };
        this.alertPresented = false
        // this.data.DeliveryTime_from = this.caterData.catt_delivery_fromtime;
        // this.data.DeliveryTime_to = this.caterData.catt_delivery_totime;
        if (this.caterData.holidays.length > 0) {
            this.showCal = true;
            for (var i = 0; i < this.caterData.holidays.length; ++i) {
                this.dateMulti.push(this.caterData.holidays[i].date)
            }
        } else {
            this.showCal = false;
            this.dateMulti = []
        }
        console.log(this.data.holiday)
        if (this.caterData.meal_type == "Veg") {
            this.data.meal_types = "Veg"
        } else {
            this.data.meal_types = "Non-Veg"
        }
        if (this.caterData.catering_name_changed == true) {
            this.data.catt_catering_name = this.caterData.old_catt_caterering_name;
        } else {
            this.data.catt_catering_name = this.caterData.catt_caterering_name;
        }
        if (this.caterData.caterering_info_changed == true) {
            this.data.catt_caterering_info = this.caterData.old_catt_caterering_info;
        } else {
            this.data.catt_caterering_info = this.caterData.catt_caterering_info;
        }
        if (this.caterData.catt_address_changed == true) {
            this.data.catt_address = this.caterData.old_catt_address;
        } else {
            this.data.catt_address = this.caterData.catt_address;
        }
        if (this.caterData.caterer_image_changed == true) {
            this.imageURI = this.caterData.old_caterer_image
            if (this.imageURI) {
                this.ImgPath = this.api.filepath;
                this.fromApi = true
            } else {
                this.fromApi = false
            }
        } else {
            this.imageURI = this.caterData.caterer_image
            if (this.imageURI) {
                this.ImgPath = this.api.filepath;
                this.fromApi = true
            } else {
                this.fromApi = false
            }
        }
        if (this.caterData.lead_time_changed == true) {
            this.data.catt_lead_time = this.caterData.old_catt_lead_time;
        } else {
            this.data.catt_lead_time = this.caterData.catt_lead_time;
        }
        if (this.caterData.min_order_changed == true) {
            this.data.catt_min_order = this.caterData.old_catt_min_order;
        } else {
            this.data.catt_min_order = this.caterData.catt_min_order;
        }
        if (this.caterData.max_order_changed == true) {
            this.data.catt_max_order = this.caterData.old_catt_max_order;
        } else {
            this.data.catt_max_order = this.caterData.catt_max_order;
        }
        if (this.caterData.gst_no_changed == true) {
            this.data.gst_number = this.caterData.old_gst_number;
        } else {
            this.data.gst_number = this.caterData.gst_number;
        }
        // for(let i = 0;i<this.caterData.cust.length;i++){
        //   this.anArray.push({'catt_pincode':this.caterData.cust[i].catt_pincode,'cat_add_id':this.caterData.cust[i].cat_add_id})
        // }
        // this.anArray = this.caterData.cust
        let Temp_Pin = []
        if (this.caterData.pincode_changed == true) {
            Temp_Pin = this.caterData.old_pincode;
        } else {
            Temp_Pin = this.caterData.new_pincode;
        }
        if (Temp_Pin.length > 0) {
            for (var k = 0; k < Temp_Pin.length; ++k) {
                this.Area_Pincodes.push(Temp_Pin[k].area.pin_id)
            }
        }
        this.anArrays = this.caterData.delivery_days
        this.AddressCount = this.anArray.length
        this.DaysCount = this.anArrays.length
        this.caterData.newuser = ""
    }
    ionViewDidLoad() {
        this.getPincodes()
        console.log('ionViewDidLoad CateringInfoEditPage');
    }
    fullView(value) {
        console.log(value)
        this.photoViewer.show(this.ImgPath + value);
    }
    fullViews(value) {
        console.log(value)
        this.photoViewer.show(value.changingThisBreaksApplicationSecurity);
    }
    randomFileName() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        var n = new Date()
        var date = n.toISOString();
        return text + "-" + date + ".png";
    }
    backButtonAction() {
        this.navCtrl.pop()
    }
    onChangeFrom() {
        this.error.from_time = "";
    }
    onChangeTo() {
        this.error.from_time = ""
    }
    onChangelead() {
        this.error.lead_time = ""
    }
    onChangemin() {
        this.error.min_order = ""
    }
    onChangemax() {
        this.error.max_order = ""
    }
    onChangeDay() {}
    convertDate(val) {
        var da = new Date(val).toLocaleDateString()
        var d = new Date(da),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return [year, day, month].join('-');
    }
    onChangeCaterName() {
        this.error.catt_catering_name = ""
    }
    onChangeCaterAddre() {
        this.error.catt_address = ""
    }
    onChangeisType() {
        this.error.meal_types = ""
    }
    onChangeCaterInfo() {
        this.error.catt_caterering_info = ""
    }
    convert(val: any) {
        var d = new Date(val)
        return d
    }
    onChange($event) {
        console.log($event);
        console.log(this.dateMulti)
    }
    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }
    catPath(val) {
        let data = this.ImgPath + val;
        return data;
    }
    takeSnap() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        }
        this.camera.getPicture(options).then((imageData) => {
            this.fromApi = false
            // this.imageURI1 = imageData;
            this.imageURI = this._DomSanitizer.bypassSecurityTrustUrl(imageData);
            console.log(this.imageURI, "img")
            this.NewPic = true
        }, (err) => {
            console.log(err);
            this.presentToast(err);
        });
    }
    toYMD(date: Date) {
        return moment(date).format("YYYY-MM-DD h:mm")
    }
    uploadFileCatererImg() {
        return new Promise((resolve, reject) => {
            let loader = this.loadingCtrl.create({
                content: "Uploading..."
            });
            loader.present();
            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
                fileKey: 'caterer_image',
                fileName: this.randomFileName(),
                chunkedMode: false,
                mimeType: "image/png",
                headers: {}
            }
            let data = [];
            debugger
            data.push(new Promise((resolve, reject) => {
                resolve(fileTransfer.upload(this.imageURI.changingThisBreaksApplicationSecurity, 'http://platos.in/api/caterer_image', options).then((data) => {
                    debugger
                    console.log(data, " Uploaded Successfully");
                    let temp = JSON.parse(data.response)
                    let imgURL = temp.data;
                    this.imageURI = '';
                    this.imageURI = imgURL.filepic;
                    console.log(this.imageURI, "caterer image")
                    // this.data.menu_images.push(imgURL)
                    loader.dismiss();
                    // this.presentToast("Image uploaded successfully");
                }, (err) => {
                    console.log(err);
                    loader.dismiss();
                    // this.presentToast(err);
                }))
            }))
            Promise.all(data).then((values) => {
                resolve(values)
                this.updateProfile()
            });
        })
    }
    checkImg() {
        console.log("pin", this.Area_Pincodes)
        if (this.data.catt_catering_name == "") {
            this.error.catt_catering_name = "Please Enter the Catering Name"
            return
        }
        // if (this.data.catt_caterering_info == "" || this.data.catt_caterering_info == null) {
        //     this.error.catt_caterering_info = "Please Enter the Catering Info"
        //     return
        // }
        if (this.data.catt_address == "") {
            this.error.catt_address = "Please Enter the Caterer Address"
            return
        }
        // if(this.anArray.length == 0){
        //   this.alert.Alert.alert("Please Add the Delivery Areas")
        //   return
        // }
        if (this.Area_Pincodes.length <= 0) {
            this.alert.Alert.alert("Please Select the Delivery Pincodes")
            return
        }
        if (this.anArrays.length == 0) {
            this.alert.Alert.alert("Please Add the Delivery Days")
            return
        }
        if (this.anArrays.length > 0) {
            debugger
            for (let j = 0; j < this.anArrays.length; j++) {
                if (this.anArrays[j].days) {
                    if (this.anArrays[j].fromtime) {
                        if (this.anArrays[j].totime) {
                            console.log("Good to go")
                        } else {
                            this.alert.Alert.alert("Please Select the To Time")
                            return
                        }
                    } else {
                        this.alert.Alert.alert("Please Select the From Time")
                        return
                    }
                } else {
                    this.alert.Alert.alert("Please Select the Day")
                    return
                }
            }
        }
        if (this.data.catt_lead_time == "" || this.data.catt_lead_time == null) {
            this.error.lead_time = "Please Enter the lead time"
            return
        }
        if (this.data.catt_min_order == "" || this.data.catt_min_order == null) {
            this.error.min_order = "Please Enter the Minimum Number"
            return
        }
        if (this.data.catt_max_order == "" || this.data.catt_max_order == null) {
            this.error.max_order = "Please Enter the Maximum Order"
            return
        }
        debugger
        if (this.data.catt_min_order > this.data.catt_max_order) {
            this.alert.Alert.alert("Min Order Should Not Greater Than Max Order")
            return
        }
        if (this.data.catt_min_order == this.data.catt_max_order) {
            this.alert.Alert.alert("Min and Max Order Can't Be Same")
            return
        }
        if (this.data.gst_number == "" || this.data.gst_number == null) {
            this.alert.Alert.alert("Please Enter The GST Number")
            return
        }
        if (this.data.meal_types == "" || this.data.meal_types == null) {
            this.error.meal_types = "Please Fill The Vegetarian Field"
            return
        }
        let dummmyValue = []
        if (this.dateMulti.length > 0) {
            for (var i = 0; i < this.dateMulti.length; ++i) {
                if (this.dateMulti[i]) {
                    this.holiday.push(this.toYMD(this.dateMulti[i]))
                    // dummmyValue.push(this.toYMD(this.dateMulti[i]))
                }
            }
        }
        console.log(dummmyValue)
        if (this.NewPic == true) {
            this.uploadFileCatererImg();
        } else {
            this.updateProfile()
        }
    }
    updateProfile() {
        // if(this.data.holiday.length > 0){
        //   for (var l = 0; l < this.data.holiday.length; ++l) {
        //   }
        // }
        // else{
        //    this.alert.Alert.alert("Please Select the Holiday")
        //    return
        // }
        // if(this.data.DeliveryTime_from == ""){
        // this.error.from_time = "Please Enter the From Time"
        // return
        // }
        //  if(this.data.DeliveryTime_to == ""){
        // this.error.from_time = "Please Enter the To Time"
        // return
        // }
        // if(this.data.DeliveryTime_from == this.data.DeliveryTime_to){
        //   this.alert.Alert.alert("Delivery Time Can't be Same")
        //   return
        // }
        let item = localStorage.getItem("tokens")
        if (this.caterData.addi_user != null) {
            this.adduser = [{
                addi_cat_id: this.caterData.addi_user.cat_usr_id,
                name: this.caterData.addi_user.cat_usr_name,
                password: this.caterData.addi_user.cat_usr_password,
                email: this.caterData.addi_user.cat_usr_email,
                phone: this.caterData.addi_user.cat_usr_mobile,
                addprofile: this.caterData.addi_user.cat_usr_profile_pic
            }]
        } else {
            this.adduser = []
        }
        let datas = {
            "catt_first_name": this.caterData.catt_first_name,
            "catt_email_address": this.caterData.catt_email_address,
            "catt_mobile_no": this.caterData.catt_mobile_no,
            // "catt_sur_name": this.caterData.catt_sur_name,
            // "DeliveryTime_from": this.data.DeliveryTime_from,
            // "DeliveryTime_to": this.data.DeliveryTime_to,
            // "catt_event_type": this.caterData.catt_event_type,
            "catt_min_order": this.data.catt_min_order,
            "catt_max_order": this.data.catt_max_order,
            "catt_lead_time": this.data.catt_lead_time,
            "caterer_image": this.imageURI,
            // "catt_score": this.caterData.catt_score,
            // "catt_gst_certificate": this.caterData.catt_gst_certificate,
            // "catt_food_certificate": this.caterData.catt_food_certificate,
            // "newuser": this.caterData.newuser,
            "catt_address": this.data.catt_address,
            "catt_id": this.caterData.catt_id,
            // "caterer_group" : this.caterData.caterer_group,
            "catt_add_users": this.adduser,
            "meal_types": this.data.meal_types,
            "catt_catering_name": this.data.catt_catering_name,
            "catt_catering_info": this.data.catt_caterering_info,
            "catt_delivery_pincode": this.Area_Pincodes,
            "delivery_day": this.anArrays,
            "catt_upload_file": this.caterData.catt_upload_file || [],
            "holidays": this.holiday,
            "gst_number": this.data.gst_number
        }
        debugger
        //  for (var k = 0; k < this.anArrays.length; k++) {
        //      if(this.anArrays[k].days){
        //         this.anArrays[k].day = this.anArrays[k].days;
        //      }
        // }
        console.log(this.anArrays)
        console.log(datas)
        for (let i = 0; i < this.anArrays.length; i++) {
            if (!this.anArrays[i].fromtime.hour) {
                if (this.anArrays[i].fromtime) {
                    let hour = this.anArrays[i].fromtime.substring(0, 2)
                    let minute = this.anArrays[i].fromtime.substring(3, 5)
                    let data = {
                        hour: '',
                        minute: ''
                    }
                    data.hour = hour
                    data.minute = minute
                    this.anArrays[i].fromtime = data
                }
            }
            if (!this.anArrays[i].totime.hour) {
                if (this.anArrays[i].totime) {
                    let hour = this.anArrays[i].totime.substring(0, 2)
                    let minute = this.anArrays[i].totime.substring(3, 5)
                    let data = {
                        hour: '',
                        minute: ''
                    }
                    data.hour = hour
                    data.minute = minute
                    this.anArrays[i].totime = data
                }
            }
        }
        debugger
        this.alert.Loader.show("Loading..")
        this.user.updateProfile(item, datas).subscribe((data: any) => {
            console.log(data)
            if (data.status == 'ok') {
                this.alert.Loader.hide()
                this.alert.Toast.show("Catering Info Updated")
                this.navCtrl.setRoot('VendorPage')
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    Add() {
        this.AddressCount = this.AddressCount + 1;
        this.anArray.push({
            'pincode': '',
            'cat_add_id': ''
        });
    }
    Adddays() {
        this.DaysCount = this.DaysCount + 1;
        this.anArrays.push({
            'day': '',
            'fromtime': '',
            'totime': ''
        });
    }
    RemoveDays() {
        this.DaysCount = this.DaysCount - 1;
        this.anArrays.splice(this.anArrays.length - 1, 1);
    }
    removeAddress(index) {
        this.AddressCount = this.AddressCount - 1;
        this.anArray.splice(this.anArray.length - 1, 1);
    }
    resize() {
        var element = this.myInput['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
        var scrollHeight = element.scrollHeight;
        element.style.height = scrollHeight + 'px';
        this.myInput['_elementRef'].nativeElement.style.height = (scrollHeight + 16) + 'px';
    }
    onChangedate(val: any) {
        debugger
        //  let date = new Date(val) 
        //  this.holiday = [];
        // this.holiday.push(date)
        // console.log(this.holiday)
        // this.error.holiday = ""
    }
    DeleteCatImg() {
        let id = localStorage.getItem('token')
        this.user.removeCatererImg(id).subscribe((data: any) => {
            console.log(data)
            if (data.status == 'ok') {
                this.imageURI = ""
                this.alert.Toast.show("Image Removed Successfully")
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    RemoveCatImg() {
        this.imageURI = ""
    }
    public DeleteCatererImage() {
        this.alertPresented = true
        localStorage.setItem("alert", this.alertPresented);
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Caterer Image',
            buttons: [{
                text: 'Delete',
                icon: 'trash',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                    this.DeleteCatImg()
                }
            }, {
                text: 'Cancel',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                }
            }],
        });
        actionSheet.present();
    }
    getPincodes() {
        this.user.getPincodes().subscribe((data: any) => {
            console.log(data)
            if (data.status == 'ok') {
                console.log(data.data)
                this.pincodes = data.data;
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    onChangePin() {
        console.log("pin", this.Area_Pincodes)
    }
}