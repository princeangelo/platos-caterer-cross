import {
    Component,
    ViewChild,
    ElementRef
} from '@angular/core';
import {
    IonicPage,
    MenuController,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {
    LoadingController,
    ToastController
} from 'ionic-angular';
import {
    FileTransfer,
    FileUploadOptions,
    FileTransferObject
} from '@ionic-native/file-transfer';
import {
    Camera,
    CameraOptions
} from '@ionic-native/camera';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    DomSanitizer
} from '@angular/platform-browser';
@IonicPage()
@Component({
    selector: 'page-signup',
    templateUrl: 'signup.html',
})
export class SignupPage {
    @ViewChild('myInput') myInput: ElementRef;
    imageURI: any = ''
    imageFoodURI: any
    imageFileName: any;
    data: any = {
        catt_first_name: '',
        catt_sur_name: '',
        catt_email_address: '',
        catt_mobile_no: '',
        // catt_min_order:'',
        // catt_max_order:'',
        catt_password: '',
        // DeliveryTime_from:'',
        // DeliveryTime_to:'',
        // catt_score:'',
        catt_address: '',
        // catt_event_type:'',
        // catt_lead_time:'',
        // Food_image:'',
        // GST_image:'',
        // audit_filesize:'',
        catt_catering_name: '',
        // catt_catering_info:'',
        // GST_No:'',
        // Audit_image:'',
        // meal_types:'',
    }
    error: any = {
        first_name: '',
        sur_name: '',
        email_address: '',
        mobile_no: '',
        // min_order:'',
        // max_order:'',
        password: '',
        // from_time:'',
        // to_time:'',
        // score:'',
        address: '',
        // event_type:'',
        // lead_time:'',
        catt_catering_name: '',
        // catt_catering_info:'',
        // GST_No:'',
        // meal_types:'',
        // catt_address:''
    }
    imageURIs: any = ''
    imagebox: any = []
    imageboxFood: any = []
    public anArray: any = [];
    // count:any = 0
    eventList: any
    displayImage: any
    passwordType: string = 'password';
    passwordIcon: string = 'eye-off';
    gst_flag = false
    food_flag = false
    AddressCount: any = 0
    showAddress: any = true
    constructor(public navCtrl: NavController, public navParams: NavParams, private transfer: FileTransfer, private camera: Camera, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public user: UserProvider, public alert: AlertProvider, private _DomSanitizer: DomSanitizer, public menu: MenuController, public platform: Platform) {
        this.menu.enable(false);
    }
    showHide() {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
    goTo() {
        console.log('this.anArray', this.anArray);
        this.data = true;
    }
    Add() {
        this.AddressCount = this.AddressCount + 1;
        this.anArray.push({
            'pincode': ''
        });
    }
    removeAddress(index) {
        this.AddressCount = this.AddressCount - 1;
        this.anArray.splice(index, 1);
    }
    moveFocus(nextElement) {
        nextElement.setFocus();
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad SignupPage');
    }
    randomFileName() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        var n = new Date()
        var date = n.toISOString();
        return text + "-" + date + ".png";
    }
    takeSnap() {
        const options: CameraOptions = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        }
        this.camera.getPicture(options).then((imageData) => {
            console.log(imageData);
            this.imageURI = this._DomSanitizer.bypassSecurityTrustUrl(imageData);
            console.log(this.imageURI)
            this.gst_flag = true
            // this.imagebox = this.imageURI;
            // console.log(this.imagebox,"uri")
        }, (err) => {
            console.log(err);
            this.presentToast(err);
        });
    }
    uploadFileGST() {
        return new Promise((resolve, reject) => {
            let loader = this.loadingCtrl.create({
                content: "Uploading..."
            });
            loader.present();
            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
                fileKey: 'gstpic',
                fileName: this.randomFileName(),
                chunkedMode: false,
                mimeType: "image/png",
                headers: {}
            }
            let data = [];
            data.push(new Promise((resolve, reject) => {
                resolve(fileTransfer.upload(this.imageURI.changingThisBreaksApplicationSecurity, 'http://platos.in/api/catgst_image', options).then((data) => {
                    console.log(data, " Uploaded Successfully");
                    let temp = JSON.parse(data.response)
                    let imgURL = temp.data;
                    this.data.GST_image = imgURL.gstpic;
                    // this.data.menu_images.push(imgURL)
                    loader.dismiss();
                    // this.presentToast("Image uploaded successfully");
                }, (err) => {
                    console.log(err);
                    loader.dismiss();
                    // this.presentToast(err);
                }))
            }))
            Promise.all(data).then((values) => {
                resolve(values)
            });
        })
    }
    uploadFileFood() {
        return new Promise((resolve, reject) => {
            let loader = this.loadingCtrl.create({
                content: "Uploading..."
            });
            loader.present();
            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
                fileKey: 'foodpic',
                fileName: this.randomFileName(),
                chunkedMode: false,
                mimeType: "image/png",
                headers: {}
            }
            let data = [];
            data.push(new Promise((resolve, reject) => {
                resolve(fileTransfer.upload(this.imageURIs.changingThisBreaksApplicationSecurity, 'http://platos.in/api/catfood_image', options).then((data) => {
                    console.log(data, " Uploaded Successfully");
                    let temp = JSON.parse(data.response)
                    let imgURL = temp.data;
                    this.data.Food_image = imgURL.foodpic;
                    // this.data.menu_images.push(imgURL)
                    loader.dismiss();
                    // this.presentToast("Image uploaded successfully");
                }, (err) => {
                    console.log(err);
                    loader.dismiss();
                    // this.presentToast(err);
                }))
            }))
            Promise.all(data).then((values) => {
                resolve(values)
            });
        })
    }
    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }
    takeSnapFood() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        }
        this.camera.getPicture(options).then((imageDatas) => {
            this.imageURIs = this._DomSanitizer.bypassSecurityTrustUrl(imageDatas);
            // this.imageboxFood = this.imageURIs;
            // console.log(this.imageboxFood,"uri")
        }, (err) => {
            console.log(err);
            this.presentToast(err);
        });
    }
    deleteImage(index) {
        console.log(index)
        // if (index > -1) {
        // this.imagebox.splice(index, 1);
        this.imageURI = ''
        // }
    }
    deleteImages(index) {
        console.log(index)
        // if (index > -1) {
        // this.imageboxFood.splice(index, 1);
        this.imageURIs = ''
        // }
    }
    onChangeName() {
        this.error.first_name = "";
    }
    onChangeSurName() {
        this.error.sur_name = "";
    }
    onChangenumber() {
        this.error.mobile_no = "";
    }
    onChangePass() {
        this.error.password = "";
    }
    onChangeFrom() {
        this.error.from_time = "";
    }
    onChangeTo() {
        this.error.from_time = ""
    }
    onChangelead() {
        this.error.lead_time = ""
    }
    onChangetype() {
        this.error.event_type = ""
    }
    onChangescore() {
        this.error.score = ""
    }
    onChangeAddress() {
        this.error.address = ""
    }
    onChangeCaterName() {
        this.error.catt_catering_name = ""
    }
    onChangeCaterinfo() {
        this.error.onChangeCaterinfo = ""
    }
    onChangeminOrder() {
        this.error.min_order = ""
    }
    onChangemaxOrder() {
        this.error.max_order = ""
    }
    onChangeGstName() {
        this.error.GST_No = ""
    }
    onChangeisType() {
        this.error.meal_types = ""
    }
    onChangeCaterAddre() {
        this.error.catt_address = ""
    }
    onChangeemail(value) {
        if (value == '') {
            this.error.email_address = "Please Enter the Email ID"
            return
        }
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(value) == false) {
            console.log("Invalid Email Address");
            this.error.email_address = "Invalid Mail Address"
        } else {
            console.log("Valid Address")
            this.error.email_address = ''
        }
    }
    async signup() {
        /**
        Error message to all when clicked
         **/
        if (this.data.catt_first_name == '') {
            this.error.first_name = "Please Enter the First Name"
        }
        if (this.data.catt_sur_name == '') {
            this.error.sur_name = "Please Enter the Last Name"
        }
        if (this.data.catt_catering_name == '') {
            this.error.catt_catering_name = "Please Enter the Caterer Name"
        }
        if (this.data.catt_email_address == '') {
            this.error.email_address = "Please Enter the Email Id"
        }
        if (this.data.catt_mobile_no == '') {
            this.error.mobile_no = "Please Enter the Mobile Number"
        }
        if (this.data.catt_password == '') {
            this.error.password = "Please Enter the Password"
        }
        if (this.data.catt_password.length < 6) {
            this.error.password = "Password Length Should be Atleast 6"
        }
        if (this.data.catt_address == '') {
            this.error.catt_address = "Please Enter the Caterer Address"
        }
        if (this.data.catt_first_name == '') {
            this.error.first_name = "Please Enter the First Name"
            return
        }
        if (this.data.catt_sur_name == '') {
            this.error.sur_name = "Please Enter the Surname"
            return
        }
        if (this.data.catt_catering_name == '') {
            this.error.catt_catering_name = "Please Enter the Caterer Name"
            return
        }
        if (this.data.catt_email_address == '') {
            this.error.email_address = "Please Enter the Email Id"
            return
        }
        if (this.data.catt_mobile_no == '') {
            this.error.mobile_no = "Please Enter the Mobile Number"
            return
        }
        // if(this.data.catt_catering_info == ''){
        //   this.error.catt_catering_info = "Please Enter the Caterer Info"
        //   return
        // }
        //   if(this.data.catt_min_order == ''){
        //   this.error.min_order = "Please Enter the Minimum Order"
        //   return
        // }
        //   if(this.data.catt_max_order == ''){
        //   this.error.max_order = "Please Enter the Maximum Order"
        //   return
        // }
        //     if(this.data.catt_min_order > this.data.catt_max_order){
        //   this.alert.Alert.alert("Min Order Should Not Greater Than Max Order")
        //   return
        // }
        // if(this.data.catt_min_order == this.data.catt_max_order){
        //   this.alert.Alert.alert("Min and Max Order Can't Be Same")
        //   return
        // }
        if (this.data.catt_password == '') {
            this.error.password = "Please Enter the Password"
            return
        }
        if (this.data.catt_password.length < 6) {
            this.error.password = "Password Length Should be Atleast 6"
            return
        }
        //     if(this.data.DeliveryTime_from == ''){
        //     this.error.from_time = "Please Enter the From Time"
        //     return
        //   }
        //       if(this.data.DeliveryTime_to == ''){
        //     this.error.from_time = "Please Enter the To Time"
        //     return
        //   }  
        // if(this.data.DeliveryTime_from == this.data.DeliveryTime_to){
        //   this.alert.Alert.alert("Delivery Time Can't be Same")
        //   return
        // }
        //     if(this.data.catt_lead_time == ''){
        //     this.error.lead_time= "Please Enter the Lead Time"
        //     return
        //   }
        //  if(this.data.GST_No == ''){
        //   this.error.GST_No = "Please Enter the GST Number"
        //   return
        // }
        // if(this.data.meal_types == ''){
        //   this.error.meal_types = "Please Select the Meal Type"
        //   return
        // }
        if (this.data.catt_address == '') {
            this.error.catt_address = "Please Enter the Caterer Address"
            return
        }
        // this.data.catt_delivery_pincode = this.anArray
        //  if(this.data.catt_delivery_pincode.length == 0){
        //   this.alert.Alert.alert("Please Add the Delivery Pincode")
        //   return
        // }
        // if(this.data.catt_delivery_pincode.length > 0){
        //    for(let j=0;j<this.data.catt_delivery_pincode.length;j++){
        //      if(this.data.catt_delivery_pincode[j].pincode){
        //        console.log("good to go")
        //      }
        //      else{
        //        this.alert.Alert.alert("Pincode Field Can't be Empty")
        //        return
        //      }
        // }
        // }
        //  if(this.gst_flag == true){
        //     await  this.uploadFileGST();
        //  }
        // if(this.food_flag == true){
        //    await  this.uploadFileFood();
        // }
        this.alert.Loader.show("Loading")
        console.log(this.data)
        this.user.signUp(this.data).subscribe((data: any) => {
            console.log(data)
            if (data.status == 'ok') {
                this.alert.Loader.hide()
                this.navCtrl.setRoot('LoginPage')
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    getImage() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
        }
        this.camera.getPicture(options).then((imageData) => {
            this.imageURIs = this._DomSanitizer.bypassSecurityTrustUrl(imageData);
            this.food_flag = true
        }, (err) => {
            console.log(err);
            this.presentToast(err);
        });
    }
    uploadFile() {
        let loader = this.loadingCtrl.create({
            content: "Uploading..."
        });
        loader.present();
        const fileTransfer: FileTransferObject = this.transfer.create();
        let options: FileUploadOptions = {
            fileKey: 'ionicfile',
            fileName: 'ionicfile',
            chunkedMode: false,
            mimeType: "image/jpeg",
            headers: {}
        }
        fileTransfer.upload(this.imageURIs, 'http://192.168.0.7:8080/api/uploadImage', options).then((data) => {
            console.log(data + " Uploaded Successfully");
            this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
            loader.dismiss();
            this.presentToast("Image uploaded successfully");
        }, (err) => {
            console.log(err);
            loader.dismiss();
            this.presentToast(err);
        });
    }
    upload_data: FormData = new FormData();
    uploadImage(event: any) {
        this.upload_data = new FormData();
        let fileList: FileList = event.target.files;
        let file: File = fileList[0];
        console.log(fileList)
        console.log(this.upload_data)
        this.upload_data.append("filepic", file)
        console.log(this.upload_data, "check")
        this.alert.Loader.show("Uploading")
        this.user.AuditUpload(this.upload_data).subscribe((data: any) => {
            console.log(data, "res")
            if (data.status == 'ok') {
                this.data.Audit_image = data.data.filepic;
                this.data.audit_filesize = data.data.filesize
                this.alert.Loader.hide()
                // this.alert.Alert.alert("success")
                this.alert.Toast.show("File Uploaded")
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    resize() {
        var element = this.myInput['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
        var scrollHeight = element.scrollHeight;
        element.style.height = scrollHeight + 'px';
        this.myInput['_elementRef'].nativeElement.style.height = (scrollHeight + 16) + 'px';
    }
}