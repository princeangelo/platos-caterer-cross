import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    ToastController
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    NgZone
} from '@angular/core';
import {
    Platform
} from 'ionic-angular';
import {
    Events
} from 'ionic-angular';
@IonicPage()
@Component({
    selector: 'page-dashboard',
    templateUrl: 'dashboard.html',
})
export class DashboardPage {
    order_list: any
    total_order: any = 0
    confirmation_pending_count: any = 0
    confirmed_count: any = 0
    Out_to_delivery_count: any = 0
    Delivered_count: any = 0
    Cancelled_count: any = 0
    CarterName: any
    auditDeactivate:any = false
    constructor(public events: Events, public toast: ToastController, public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider,
        // public modalCtrl: ModalController,
        public _zone: NgZone, public platform: Platform) {
        // this.CarterName = localStorage.getItem("carter_name")
        this._zone.run(() => {
            let id: any = localStorage.getItem("tokens")
            this.user.mis_report(id).subscribe((data: any) => {
                console.log(data, "dash")
                this.order_list = data.data;
                this.total_order = this.order_list.length;
                for (var i = this.order_list.length - 1; i >= 0; i--) {
                    if (this.order_list[i].ord_status == 'Order_placed') {
                        this.confirmation_pending_count = this.confirmation_pending_count + 1;
                    }
                    if (this.order_list[i].ord_status == 'Confirmed') {
                        this.confirmed_count = this.confirmed_count + 1;
                    }
                    if (this.order_list[i].ord_status == 'Out_to_delivery') {
                        this.Out_to_delivery_count = this.Out_to_delivery_count + 1;
                    }
                    if (this.order_list[i].ord_status == 'Delivered') {
                        this.Delivered_count = this.Delivered_count + 1;
                    }
                    if (this.order_list[i].ord_status == 'Cancelled') {
                        this.Cancelled_count = this.Cancelled_count + 1;
                    }
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        })

        this.checkAuditDeactive()
    }
    showToast(msg) {
        let toast = this.toast.create({
            message: msg,
            duration: 2000
        });
        toast.present();
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad DashboardPage');
    }
    apihit() {
        this.total_order = 0
        this.confirmation_pending_count = 0
        this.confirmed_count = 0
        this.Out_to_delivery_count = 0
        this.Delivered_count = 0
        this.Cancelled_count = 0
        this._zone.run(() => {
            let id: any = localStorage.getItem("tokens")
            this.user.mis_report(id).subscribe((data: any) => {
                console.log(data, "dash")
                this.order_list = data.data;
                this.total_order = this.order_list.length;
                for (var i = this.order_list.length - 1; i >= 0; i--) {
                    if (this.order_list[i].ord_status == 'Order_placed') {
                        this.confirmation_pending_count = this.confirmation_pending_count + 1;
                    }
                    if (this.order_list[i].ord_status == 'Confirmed') {
                        this.confirmed_count = this.confirmed_count + 1;
                    }
                    if (this.order_list[i].ord_status == 'Out_to_delivery') {
                        this.Out_to_delivery_count = this.Out_to_delivery_count + 1;
                    }
                    if (this.order_list[i].ord_status == 'Delivered') {
                        this.Delivered_count = this.Delivered_count + 1;
                    }
                    if (this.order_list[i].ord_status == 'Cancelled') {
                        this.Cancelled_count = this.Cancelled_count + 1;
                    }
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        })
    }
    toConfir() {
        this.navCtrl.setRoot('ConfirmationPage')
    }
    toOrders() {
        this.navCtrl.setRoot('OrdersPage')
    }


    checkAuditDeactive(){
        let token: any = localStorage.getItem("tokens")
        let data: any = {
            token: '',
        }
        data.token = token;
        this.alert.Loader.show("Loading")
        this.user.getCaterer(data).subscribe((data: any) => {
            console.log(data.data, "caterer details")
            this.CarterName = data.data.catt_first_name

                this.CarterName = this.CarterName.substring(0,13)
                var len = this.CarterName.length
                if(len > 13){
                this.CarterName = this.CarterName + "..."
                }

                console.log(this.CarterName,"name")

             let name = data.data.catt_first_name
             let mail = data.data.catt_email_address
             this.events.publish('login:created', name, mail);

            if(data.data){
                console.log("event")
                this.auditDeactivate = data.data.audit_deactive;
                this.events.publish('audit:status', data.data.audit_deactive);
            }
            this.alert.Loader.hide()
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
}