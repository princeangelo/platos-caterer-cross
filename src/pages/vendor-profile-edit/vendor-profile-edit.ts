import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    Events
} from 'ionic-angular';
/**
 * Generated class for the VendorProfileEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-vendor-profile-edit',
    templateUrl: 'vendor-profile-edit.html',
})
export class VendorProfileEditPage {
    data: any = {
        catt_first_name: '',
        // catt_sur_name:'',
        catt_email_address: '',
        catt_mobile_no: '',
        // catt_catering_name:'',
        // catt_catering_info:''
    }
    error: any = {
        first_name: '',
        // sur_name:'',
        mobile_no: '',
        email_address: '',
        // catt_catering_name:'',
        // catt_catering_info:''
    }
    vendor_Profile: any
    adduser: any
    // is_nonveg:boolean = false
    anArrays: any
    Holidays: any = []
    from
    constructor(public platform: Platform, public events: Events, public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider) {
        this.vendor_Profile = this.navParams.get("data")
        this.from = this.navParams.get("from")
        console.log(this.from)
        console.log(this.vendor_Profile)
        this.data.catt_first_name = this.vendor_Profile.catt_first_name;
        // this.data.catt_sur_name = this.vendor_Profile.catt_sur_name;
        this.data.catt_email_address = this.vendor_Profile.catt_email_address;
        this.data.catt_mobile_no = this.vendor_Profile.catt_mobile_no;
        // this.data.catt_catering_name = this.vendor_Profile.catt_caterering_name
        // this.data.catt_catering_info = ""
        this.vendor_Profile.newuser = ""
    }
    onChangeName() {
        this.error.first_name = "";
    }
    onChangeSurName() {
        this.error.sur_name = "";
    }
    onChangenumber() {
        this.error.mobile_no = "";
    }
    onChangeCaterName() {
        this.error.catt_catering_name = ""
    }
    onChangeCaterinfo() {
        this.error.catt_catering_info = ""
    }
    onChangeemail(value) {
        if (value == '') {
            this.error.email_address = "Please Enter the Email ID"
            return
        }
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(value) == false) {
            console.log("Invalid Email Address");
            this.error.email_address = "Invalid Mail Address"
        } else {
            console.log("Valid Address")
            this.error.email_address = ''
        }
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad VendorProfileEditPage');
    }
    updateProfile() {
        if (this.data.catt_first_name == "") {
            this.error.first_name = "Please Enter the First Name"
            return
        }
        //  if(this.data.catt_sur_name == ""){
        // this.error.sur_name = "Please Enter the Surname"
        // return
        // }
        if (this.data.catt_email_address == "") {
            this.error.email_address = "Please Enter the Email Address"
            return
        }
        if (this.data.catt_mobile_no == "") {
            this.error.mobile_no = "Please Enter the Mobile Number"
            return
        }
        // if(this.data.catt_catering_name == ""){
        // this.error.catt_catering_name = "Please Enter the Caterer Name"
        // return
        // }
        //  if(this.data.catt_catering_info == ""){
        // this.error.catt_catering_info = "Please Enter the Caterer Info"
        // return
        // }
        let item = localStorage.getItem("tokens")
        if (this.vendor_Profile.addi_user != null) {
            this.adduser = [{
                addi_cat_id: this.vendor_Profile.addi_user.cat_usr_id,
                name: this.vendor_Profile.addi_user.cat_usr_name,
                password: this.vendor_Profile.addi_user.cat_usr_password,
                email: this.vendor_Profile.addi_user.cat_usr_email,
                phone: this.vendor_Profile.addi_user.cat_usr_mobile,
                addprofile: this.vendor_Profile.addi_user.cat_usr_profile_pic
            }]
        } else {
            this.adduser = []
        }
        this.anArrays = this.vendor_Profile.delivery_days
        if (this.anArrays.length > 0) {
            for (let i = 0; i < this.anArrays.length; i++) {
                if (!this.anArrays[i].fromtime.hour) {
                    if (this.anArrays[i].fromtime) {
                        let hour = this.anArrays[i].fromtime.substring(0, 2)
                        let minute = this.anArrays[i].fromtime.substring(3, 5)
                        let data = {
                            hour: '',
                            minute: ''
                        }
                        data.hour = hour
                        data.minute = minute
                        this.anArrays[i].fromtime = data
                    }
                }
                if (!this.anArrays[i].totime.hour) {
                    if (this.anArrays[i].totime) {
                        let hour = this.anArrays[i].totime.substring(0, 2)
                        let minute = this.anArrays[i].totime.substring(3, 5)
                        let data = {
                            hour: '',
                            minute: ''
                        }
                        data.hour = hour
                        data.minute = minute
                        this.anArrays[i].totime = data
                    }
                }
            }
        }
        if (this.vendor_Profile.holidays.length > 0) {
            for (var i = 0; i < this.vendor_Profile.holidays.length; ++i) {
                this.Holidays.push(this.vendor_Profile.holidays[i].date)
            }
        } else {
            this.Holidays = this.vendor_Profile.holidays;
        }
        let datas = {
            "catt_first_name": this.data.catt_first_name,
            "catt_email_address": this.data.catt_email_address,
            "catt_mobile_no": this.data.catt_mobile_no,
            // "catt_sur_name": this.data.catt_sur_name,
            // "DeliveryTime_from": this.vendor_Profile.catt_delivery_fromtime,
            // "DeliveryTime_to": this.vendor_Profile.catt_delivery_totime,
            // "catt_event_type": this.vendor_Profile.catt_event_type,
            "catt_min_order": this.vendor_Profile.catt_min_order,
            "catt_max_order": this.vendor_Profile.catt_max_order,
            "catt_lead_time": this.vendor_Profile.catt_lead_time,
            "caterer_image": this.vendor_Profile.caterer_image,
            // "catt_score": this.vendor_Profile.catt_score,
            // "catt_gst_certificate": this.vendor_Profile.catt_gst_certificate,
            // "catt_food_certificate": this.vendor_Profile.catt_food_certificate,
            // "newuser": this.vendor_Profile.newuser,
            "catt_address": this.vendor_Profile.catt_address,
            "catt_id": this.vendor_Profile.catt_id,
            // "caterer_group" : this.vendor_Profile.caterer_group,
            "catt_add_users": this.adduser,
            "catt_catering_name": this.vendor_Profile.catt_caterering_name,
            "catt_catering_info": this.vendor_Profile.catt_caterering_info,
            "meal_types": this.vendor_Profile.meal_type,
            // "catt_delivery_pincode":this.vendor_Profile.cust,
            "delivery_day": this.anArrays || [],
            "catt_upload_file": this.vendor_Profile.catt_upload_file || [],
            "holidays": this.Holidays,
            "gst_number": this.vendor_Profile.gst_number
        }
        console.log(datas, "check here")
        this.alert.Loader.show("Loading..")
        this.user.updateProfile(item, datas).subscribe((data: any) => {
            console.log(data)
            if (data.status == 'ok') {
                let name = this.data.catt_first_name
                let mail = this.data.catt_email_address
                this.events.publish('login:created', name, mail);
                this.alert.Loader.hide()
                this.alert.Toast.show("Vendor Profile Updated")
                if(this.from == 'vendor'){
                   this.navCtrl.setRoot('VendorPage') 
                }
                else if(this.from == 'profile'){
                   this.navCtrl.setRoot('MyProfilePage') 
                }
                
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
}