import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VendorProfileEditPage } from './vendor-profile-edit';

@NgModule({
  declarations: [
    VendorProfileEditPage,
  ],
  imports: [
    IonicPageModule.forChild(VendorProfileEditPage),
  ],
})
export class VendorProfileEditPageModule {}
