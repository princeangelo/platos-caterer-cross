import {
    Component
} from '@angular/core';
import {
    IonicPage,
    MenuController,
    NavController,
    NavParams
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    Events
} from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    data: any = {
        username: '',
        password: '',
        is_mob_login: true
    }
    error: any = {
        username: '',
        password: ''
    }
    passwordType: string = 'password';
    passwordIcon: string = 'eye-off';
    constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider, public menu: MenuController) {
        this.menu.enable(false);
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }
    createUser(user) {
        console.log('User created!', user)
        this.events.publish('user:created', user, Date.now());
    }
    moveFocus(nextElement) {
        nextElement.setFocus();
    }
    login() {
        if (!this.data.username) {
            this.error.username = "Please Enter The Email or Phone Number"
            if (!this.data.password) {
                this.error.password = "Please Enter The Password"
                if (this.data.password.length < 6) {
                    this.error.password = "Minimum Password Length Should be 6"
                }
            }
        }
        if (this.data.username == "") {
            this.error.username = "Please Enter The Email or Mobile Number"
            return
        }
        if (this.data.password == "") {
            this.error.password = "Please Enter The Password"
            return
        }
        if (this.data.password.length < 6) {
            this.error.password = "Minimum Password Length Should be 6"
            return
        }
        this.alert.Loader.show("Logging In")
        console.log(this.data)
        this.user.login(this.data).subscribe((data: any) => {
            console.log(data)
            if (data.status == 'ok') {
                this.alert.Loader.hide();
                // if(data.data.is_reseted_password == true){
                //    localStorage.setItem("token", data.data.catt_id);
                //    localStorage.setItem("tokens", data.data.token);
                //     this.navCtrl.push("ChangepasswordPage",{
                //       token: data.data.catt_id,
                //     })
                // }
                // else{
                debugger
                if (data.data.Role == "Caterer_Admin") {
                    localStorage.setItem("carter_name", data.data.catt_first_name)
                    localStorage.setItem("carter_email", data.data.catt_email_address)
                    localStorage.setItem("pic", data.data.catt_profile_pic)
                    let name = data.data.catt_first_name
                    let mail = data.data.catt_email_address
                    let profile = data.data.catt_profile_pic
                    let admin: any = true
                    localStorage.setItem("is_admin", admin);
                    this.events.publish('login:created', name, mail);
                    this.events.publish('profilepic:created', profile);
                    this.events.publish('isAdmin:created', admin);
                } else {
                    localStorage.setItem("carter_name", data.data.cat_usr_name)
                    localStorage.setItem("carter_email", data.data.cat_usr_email)
                    localStorage.setItem("pic", data.data.cat_usr_profile_pic)
                    let name = data.data.cat_usr_name
                    let mail = data.data.cat_usr_email
                    let profile = data.data.cat_usr_profile_pic
                    let admin: any = false
                    localStorage.setItem("is_admin", admin);
                    this.events.publish('login:created', name, mail);
                    this.events.publish('profilepic:created', profile);
                    this.events.publish('isAdmin:created', admin);
                }
                localStorage.setItem("token", data.data.catt_id);
                localStorage.setItem("tokens", data.data.token);
                this.menu.enable(true)
                this.createUser(data.data.catt_first_name)
                this.navCtrl.setRoot('DashboardPage')
                // }
            } else {
                this.alert.Loader.hide();
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    signup() {
        this.navCtrl.push("SignupPage")
    }
    forgot() {
        this.navCtrl.push("ForgotPage")
    }
    showHide() {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
    // onChangeTime(value:any){
    //  console.log("value",value)
    //  this.error.username = ''
    // }
    onChangePass() {
        if (this.data.password.length >= 6) {
            this.error.password = ''
        }
    }
    onChangeTime(value) {
        this.error.username = ''
        // if(value == '')
        // {
        //   this.error.username = "Please Enter the Email ID"
        //   return
        // }
        // var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        // if (reg.test(value) == false) 
        // {
        //     console.log("Invalid Email Address");
        //     this.error.username = "Invalid Mail Address"
        // }
        // else{
        //   console.log("Valid Address")
        //   this.error.username = ''
        // }
    }
}