import {
    Component,
    ViewChild,
    ElementRef
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {
    Api
} from '../../providers/api/api';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    LoadingController,
    ToastController
} from 'ionic-angular';
import {
    FileTransfer,
    FileUploadOptions,
    FileTransferObject
} from '@ionic-native/file-transfer';
import {
    Camera,
    CameraOptions
} from '@ionic-native/camera';
import {
    NgZone
} from '@angular/core';
@IonicPage()
@Component({
    selector: 'page-menu-edit',
    templateUrl: 'menu-edit.html',
})
export class MenuEditPage {
    @ViewChild('myInput') myInput: ElementRef;
    desc: any;
    img1: any;
    imagebox: any = []
    menu_items: any = []
    mealsList: any
    cuisineList: any
    Packinglist: any
    gropsList: any
    Caterer_List: any
    imageURI: any;
    data: any = {
        catt_id: '',
        cuisine_type_id: '',
        // group_id:'',
        is_recommended: true,
        is_veg: '',
        max_order: '',
        meal_type_id: '',
        item_group: '',
        menu_description: '',
        menu_images: [],
        menu_name: '',
        min_order: '',
        package_type_id: '',
        per_person_cost: '',
        is_combo: false,
        no_of_combo: '',
        hsn_number: ''
    }
    ImagePath: any
    editData: any
    apihitImages: any = []
    showCat: any = false
    menuarray
    menuList: any = []
    selected: any = []
    count: any = 0;
    combo: any
    Combo_arr: any = [{
        "id": 1,
        "value": 2,
    }, {
        "id": 2,
        "value": 3
    }, {
        "id": 3,
        "value": 4
    }, {
        "id": 4,
        "value": 5
    }, ]
    Combo_List: any = []
    items: any
    combo_items: any = []
    is_menu_taken: any = false;
    showDiscount = false
    selectOptions: any
    selectOptions1: any
    selectOptions2: any
    selectOptions3: any
    selectOptions4: any
    selectOptions5: any
    constructor(public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider, private transfer: FileTransfer, private camera: Camera, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public api: Api, public _zone: NgZone, public platform: Platform) {
        this.selectOptions = {
            title: 'Vegetarian',
        };
        this.selectOptions1 = {
            title: 'Meal Type',
            subTitle: '(Scroll for more)',
        };
        this.selectOptions2 = {
            title: 'Cuisine Type',
            subTitle: '(Scroll for more)',
            cssClass: 'myclass'
        };
        this.selectOptions3 = {
            title: 'Packing Type',
            subTitle: '(Scroll for more)',
        };
        this.selectOptions4 = {
            title: 'Menu group',
            subTitle: '(Scroll for more)',
        };
        this.selectOptions5 = {
            title: 'Item List',
            subTitle: '(Scroll for more)',
        };
        let token: any = localStorage.getItem("tokens")
        this.user.getMenu(token).subscribe((data: any) => {
            console.log(data, "all menu")
            this.items = data.data;
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        this._zone.run(() => {
            this.editData = this.navParams.get("data")
            console.log(this.editData, "editdata")
            this.ImagePath = this.api.Menu_image;
            this.data.catt_id = this.editData.catt_id
            // this.data.group_id = this.editData.group_id
            this.data.is_recommended = this.editData.is_recommended
            if (this.editData.is_veg == true) {
                this.data.is_veg = 1
            } else {
                this.data.is_veg = 0
            }
            if (this.editData.cuisine_changed == true) {
                this.data.cuisine_type_id = this.editData.old_cuisine_id
            } else {
                this.data.cuisine_type_id = this.editData.cuisine_type_id
            }
            if (this.editData.meal_type_changed == true) {
                this.data.meal_type_id = this.editData.old_meal_type_id
            } else {
                this.data.meal_type_id = this.editData.meal_type_id
            }
            if (this.editData.menu_description_changed == true) {
                this.data.menu_description = this.editData.old_menu_description
            } else {
                this.data.menu_description = this.editData.menu_description
            }
            if (this.editData.menu_name_changed == true) {
                this.data.menu_name = this.editData.old_menu_name
            } else {
                this.data.menu_name = this.editData.menu_name
            }
            if (this.editData.package_changed == true) {
                this.data.package_type_id = this.editData.old_packaged_id
            } else {
                this.data.package_type_id = this.editData.package_type_id
            }
            if (this.editData.price_changed == true) {
                this.data.per_person_cost = this.editData.old_price_person_cost
            } else {
                this.data.per_person_cost = this.editData.per_person_cost
            }
            if (this.editData.discount_off_changed == true) {
                this.data.discount = this.editData.old_discount_off
            } else {
                this.data.discount = this.editData.discount_off
            }
            if (this.editData.discount_duration_changed == true) {
                this.data.discount_duration = this.editData.old_discount_duration
            } else {
                this.data.discount_duration = this.editData.discount_duration
            }
            if (this.data.discount) {
                this.showDiscount = true
            }
            if (this.data.discount_duration) {
                this.showDiscount = true
            }
            if (this.editData.gst_changed == true) {
                this.data.gst_percent = this.editData.old_gst_percent
            } else {
                this.data.gst_percent = this.editData.gst_percent
            }
            if (this.editData.max_order_changed == true) {
                this.data.max_order = this.editData.old_max_order
            } else {
                this.data.max_order = this.editData.max_order
            }
            if (this.editData.min_order_changed == true) {
                this.data.min_order = this.editData.old_min_order
            } else {
                this.data.min_order = this.editData.min_order
            }
            this.data.menu_images = this.editData.images
            this.data.is_combo = this.editData.is_combo
            this.data.no_of_combo = this.editData.no_of_combo
            this.data.hsn_number = this.editData.hsn_number
            let comboItems = []
            let temp = []
            if (this.data.is_combo) {
                if (this.editData.combos.length > 0) {
                    for (var i = 0; i < this.editData.combos.length; ++i) {
                        temp = []
                        for (var j = 0; j < this.editData.combos[i].combo_items.length; ++j) {
                            temp.push(this.editData.combos[i].combo_items[j].menu_id)
                        }
                        comboItems.push({
                            "combo_item": temp,
                            "combo_name": this.editData.combos[i].group_name
                        })
                    }
                }
            }
            this.Combo_List = comboItems;
            console.log(this.Combo_List)
            if (this.editData.menu_id) {
                this.data.item_group = this.editData.menu_id
            }
            this.imagebox = this.editData.images;
            this.count = this.imagebox.length
        })
    }
    ionViewDidLoad() {
        this.dropDowndetails();
    }
    dropDowndetails() {
        this._zone.run(() => {
            this.alert.Loader.show("Loading")
            this.user.getCuisineDropdownList().subscribe((data: any) => {
                console.log(data, "cuisine")
                this.cuisineList = data.data;
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
            this.user.getGroupsDropdownList().subscribe((data: any) => {
                console.log(data, "groups")
                this.gropsList = data.data;
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
            this.user.getMealsDropdownList().subscribe((data: any) => {
                console.log(data, "meals")
                this.mealsList = data.data;
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
            this.user.getPackageDropdownList().subscribe((data: any) => {
                console.log(data, "package")
                this.Packinglist = data.data;
                this.alert.Loader.hide()
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
            let id: any = localStorage.getItem("tokens")
            this.user.getGroupList(id).subscribe((data: any) => {
                this.alert.Loader.hide()
                console.log(data, "item group")
                this.menuarray = data.data;
                for (let i = 0; i < this.menuarray.length; i++) {
                    this.menuList.push({
                        "menu": this.menuarray[i].menugroup_name,
                        "menu_id": this.menuarray[i].menugroup_id
                    })
                }
                console.log("menu list", this.menuList)
                for (let j = 0; j < this.menuarray.length; j++) {
                    debugger
                    for (let k = 0; k < this.menuarray[j].items.length; k++) {
                        if (this.editData.menu_id == this.menuarray[j].items[k].menu_id) {
                            this.data.item_group = this.menuarray[j].menugroup_id
                        }
                    }
                }
                console.log("menu group id", this.menu_items)
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
            //  let token:any = localStorage.getItem("tokens")
            // let data:any = {
            //   token:'',
            // }
            // data.token = token;
            //       this.user.getCaterer(data).subscribe((data: any) => {
            //         debugger
            //       this.Caterer_List = data.data.special_category
            //      if(this.Caterer_List.length == 0){
            //        this.showCat = false
            //      }
            //      else {
            //        this.showCat = true
            //        this.data.special_category = this.editData.special.category_id
            //      }
            //       this.alert.Loader.hide();
            //         }, (error) => {
            //             this.alert.Loader.hide();
            //             this.alert.Alert.alert("Cannot Connect To Server")
            //             return;
            //         })
        })
    }
    getImage() {
        const options: CameraOptions = {
            quality: 30,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        }
        this.camera.getPicture(options).then(async (imageData) => {
            this.imageURI = imageData;
            console.log(this.imagebox)
            await this.uploadFile();
        }, (err) => {
            console.log(err);
            this.presentToast(err);
        });
    }
    onChangeName() {
        if (this.data.menu_name) {
            let token = localStorage.getItem('tokens')
            let data2 = {
                "menu_name": this.data.menu_name,
                "token": token
            }
            this.user.checkMenuNameExists(data2).subscribe((data: any) => {
                console.log(data)
                if (data.status == 'ok') {
                    this.is_menu_taken = false;
                } else {
                    this.is_menu_taken = true;
                    return;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        } 
    }

    randomFileName() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        var n = new Date()
        var date = n.toISOString();
        return text + "-" + date + ".png";
    }
    backButtonAction() {
        this.navCtrl.pop()
    }
    onChangecom() {
        console.log("combo value", this.data.no_of_combo)
        let combo_count = this.data.no_of_combo;
        combo_count = parseInt(combo_count)
        if (this.Combo_List.length > 0) {
            if (combo_count < this.Combo_List.length) {
                combo_count = this.Combo_List.length - combo_count;
                for (var j = 0; j < combo_count; ++j) {
                    this.Combo_List.splice(this.Combo_List.length - 1, 1);
                }
            } else if (combo_count > this.Combo_List.length) {
                combo_count = combo_count - this.Combo_List.length;
                //splice
                for (var i = 0; i < combo_count; ++i) {
                    this.Combo_List.push({
                        "combo_name": '',
                        "combo_item": ''
                    })
                }
            } else if (combo_count == this.Combo_List) {
                console.log("Good to go")
            }
        } else {
            for (var k = 0; k < combo_count; ++k) {
                this.Combo_List.push({
                    "combo_name": '',
                    "combo_item": ''
                })
            }
        }
        console.log(this.Combo_List, "Combo list")
    }
    uploadFile() {
        return new Promise((resolve, reject) => {
            let loader = this.loadingCtrl.create({
                content: "Uploading..."
            });
            if (this.imageURI) {
                loader.present();
            }
            const fileTransfer: FileTransferObject = this.transfer.create();
            let options: FileUploadOptions = {
                fileKey: 'pic[]',
                fileName: this.randomFileName(),
                chunkedMode: false,
                mimeType: "image/png",
                headers: {}
            }
            let data = [];
            data.push(new Promise((resolve, reject) => {
                resolve(fileTransfer.upload(this.imageURI, 'http://platos.in/api/AddMenu_images', options).then((data) => {
                    console.log(data, " Uploaded Successfully");
                    let temp = JSON.parse(data.response)
                    let imgURL = temp.data;
                    console.log(imgURL, "url")
                    let datas: any = {
                        menu_imgname: '',
                    }
                    datas.menu_imgname = imgURL[0]
                    this.data.menu_images.push(datas)
                    this.apihitImages.push(imgURL)
                    this.count = this.count + 1;
                    loader.dismiss();
                    // this.presentToast("Image uploaded successfully");
                }, (err) => {
                    console.log(err);
                    loader.dismiss();
                }))
            }))
            // }
            Promise.all(data).then((values) => {
                resolve(values)
            });
        })
    }
    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }
    deleteImage(index, img) {
        if (img.menu_imgid) {
            this.user.deleteImageMenu(img.menu_imgid).subscribe((data: any) => {
                console.log(data)
                if (data.status == 'ok') {
                    if (index > -1) {
                        this.imagebox.splice(index, 1);
                        this.apihitImages.splice(index, 1);
                        this.count = this.count - 1;
                        this.presentToast("Image Removed Successfully");
                    }
                } else {
                    this.alert.Alert.alert(data.data)
                    return;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        } else {
            if (index > -1) {
                this.imagebox.splice(index, 1);
                this.apihitImages.splice(index, 1);
            }
        }
    }
    showImage(value: any) {
        let img = this.ImagePath + value;
        return img
    }
    apihit() {
        console.log(this.imagebox, "images")
        if (this.imagebox.length > 0) {
            this.data.is_recommended = true
        } else {
            this.data.is_recommended = false
        }
        this.data.menu_images = this.apihitImages;
        if (this.data.menu_name == "") {
            this.alert.Alert.alert("Please Enter The Menu Name")
            return;
        }
        
        if (this.data.menu_name) {
            if (this.is_menu_taken == true) {
                this.alert.Alert.alert("Menu Name Already Exists")
                return;
            }
        }
        if (this.data.min_order) {
             if(this.data.min_order > 0){
            if (this.data.max_order) {
              if (this.data.max_order > 0) {
                if (this.data.min_order > this.data.max_order) {
                    return this.alert.Alert.alert("Minimum Order Can't be more than Maximum Order")
                } else if (this.data.max_order < this.data.min_order) {
                    return this.alert.Alert.alert("Maximum Order Can't be less than Minimum Order")
                }
              }
              else{
                 return this.alert.Alert.alert("Invalid Maximum Order") 
              }
            } 

            else {
                return this.alert.Alert.alert("Please Enter the Maximum Order")
            }
          }
          else{
               return this.alert.Alert.alert("Invalid Minimum Order")
          }
        }

        if (this.data.max_order) {
          if(this.data.max_order > 0){ 
            if (this.data.min_order) {
                if (this.data.min_order > 0) {
                if (this.data.min_order > this.data.max_order) {
                    return this.alert.Alert.alert("Minimum Order Can't be more than Maximum Order")
                } else if (this.data.max_order < this.data.min_order) {
                    return this.alert.Alert.alert("Maximum Order Can't be less than Minimum Order")
                }
             }
             else{
                  return this.alert.Alert.alert("Invalid Minimum Order")
             }

            } else {
                return this.alert.Alert.alert("Please Enter the Minimum Order")
            }
          }
          else{
              return this.alert.Alert.alert("Invalid Maximum Order")
          } 
        }


        if (this.data.per_person_cost == "") {
            this.alert.Alert.alert("Please Enter The Per Person Cost")
            return;
        }

         if (this.data.per_person_cost <= 0) {
            this.alert.Alert.alert("Invalid Per Person Cost")
            return;
        }

        if (this.data.gst_percent == "") {
            this.alert.Alert.alert("Please Enter The GST Percentage")
            return;
        }

        if (this.data.gst_percent <= 0) {
            this.alert.Alert.alert("Invalid GST Percentage")
            return;
        }

        if (this.data.gst_percent > 100) {
            return this.alert.Alert.alert("GST Percentage Should Not Exceed More Than 100")
        }
        if (this.data.hsn_number == "") {
            return this.alert.Alert.alert("Please Enter The HSN Number")
        }

        if (this.data.discount) {
          if(this.data.discount > 0){
            if (this.data.discount_duration == "") {
                return this.alert.Alert.alert("Please Enter the Discount Duration")
            }

            if (this.data.discount_duration <= 0) {
                return this.alert.Alert.alert("Invalid Discount Duration")
            }
          }
          else{
              return this.alert.Alert.alert("Invalid Discount Percentage") 
          }  
        }


        if (this.data.discount_duration) {
          if(this.data.discount_duration > 0){  
            if (this.data.discount == "") {
                return this.alert.Alert.alert("Please Enter the Discount Percentage")
            }

            if (this.data.discount <= 0) {
                return this.alert.Alert.alert("Invalid Discount Percentage")
            }
           } 
           else{
             return this.alert.Alert.alert("Invalid Discount Duration") 
          }  
        }


        let id
        id = this.editData.menu_id;
        console.log(this.data)
        console.log(this.menu_items)
        console.log(this.Combo_List, "before api hit")
        let temp_item = []
        let combo_item = []
        debugger
        for (var i = 0; i < this.Combo_List.length; ++i) {
            temp_item = []
            for (var j = 0; j < this.Combo_List[i].combo_item.length; ++j) {
                for (var k = 0; k < this.items.length; ++k) {
                    if (this.Combo_List[i].combo_item[j] == this.items[k].menu_id) {
                        temp_item.push({
                            "menu_id": this.Combo_List[i].combo_item[j],
                            "menu_name": this.items[k].menu_name
                        })
                    }
                }
            }
            combo_item.push({
                "combo_item": temp_item,
                "combo_name": this.Combo_List[i].combo_name
            })
        }
        this.data.combo_items = combo_item;
        // if(this.menu_items){
        //              for(let i = 0;i < this.menu_items.length;i++){
        //                   this.selected.push({ "menugroup_id": this.menu_items[i]})
        //                }
        //                this.data.item_group = this.selected
        //            }
        this.user.updateMenu(id, this.data).subscribe((data: any) => {
            console.log(data)
            if (data.status == 'ok') {
                this.presentToast("Menu Updated Successfully");
                this.navCtrl.setRoot('MenusPage')
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    resize() {
        var element = this.myInput['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
        var scrollHeight = element.scrollHeight;
        element.style.height = scrollHeight + 'px';
        this.myInput['_elementRef'].nativeElement.style.height = (scrollHeight + 16) + 'px';
    }
    checkBox(){
        if(this.data.is_combo == false){
           this.data.no_of_combo = '';
           this.Combo_List = []
        }
    }
}