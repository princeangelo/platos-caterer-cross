// The page the user lands on after opening the app and without a session

export const FirstRunPage = 'LoginPage';
// export const FirstRunPage = 'HelppagePage';

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'DashboardPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'ConfirmationPage';
export const Tab2Root = 'OrdersPage';
export const Tab3Root = 'PaymentPage';
export const Tab4Root = 'VendorPage';
export const Tab6Root = 'MenusPage';
export const Tab7Root = 'AuditPage';
export const Tab8Root = 'MyProfilePage';



