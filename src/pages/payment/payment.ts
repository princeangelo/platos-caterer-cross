import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {
    Api
} from '../../providers/api/api';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
@IonicPage()
@Component({
    selector: 'page-payment',
    templateUrl: 'payment.html',
})
export class PaymentPage {
    flagStatus = false
    searchText = ""
    OrgItems
    misreport: any
    constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public api: Api, public user: UserProvider, public alert: AlertProvider) {}
    ionViewDidLoad() {
        this.apihit()
    }
    apihit() {
        let token = localStorage.getItem("tokens")
        this.alert.Loader.show("Loading..")
        this.user.mis_report(token).subscribe((data: any) => {
            console.log(data)
            this.misreport = data.data;
            this.OrgItems = data.data;
            this.alert.Loader.hide();
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    onFilter(far) {
        console.log(this.searchText)
        this.misreport = this.OrgItems;
        this.misreport = this.misreport.filter((confir) => {
            if (confir.userslist.usr_first_name.toLocaleLowerCase().includes(this.searchText.toLocaleLowerCase())) {
                return confir
            }
        })
    }
    onCancel(far) {
        this.misreport = [];
        this.OrgItems = [];
        console.log("Load ...");
        this.apihit();
    }
    toDate(dat: any) {
        let date = new Date(dat)
        let data = date.toLocaleDateString()
        return data
    }
}