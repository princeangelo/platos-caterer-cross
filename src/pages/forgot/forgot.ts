import {
    Component
} from '@angular/core';
import {
    IonicPage,
    MenuController,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    LoadingController,
    ToastController
} from 'ionic-angular';
@IonicPage()
@Component({
    selector: 'page-forgot',
    templateUrl: 'forgot.html',
})
export class ForgotPage {
    email: any
    error: any
    constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public user: UserProvider, public alert: AlertProvider, public menu: MenuController) {
        this.menu.enable(false)
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ForgotPage');
    }
    submit() {
        if (this.email == '') {
            this.alert.Alert.alert("Please Enter the Email or Phone Number")
            return
        }
        let data: any = {
            email: '',
        }
        data.email = this.email;
        this.alert.Loader.show("Sending Reset Link..")
        this.user.forgot(data).subscribe((data: any) => {
            console.log(data)
            if (data.status == 'ok') {
                this.alert.Loader.hide()
                this.alert.Toast.show("Link Sent Successfully");
                // this.navCtrl.setRoot('LoginPage',{
                //   change:true,
                // })
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
}