import {
    Component,
    ViewChild
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    ActionSheetController,
    Platform
} from 'ionic-angular';
import {
    Slides
} from 'ionic-angular';
import {
    Api
} from '../../providers/api/api';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
@IonicPage()
@Component({
    selector: 'page-menu-detail',
    templateUrl: 'menu-detail.html',
})
export class MenuDetailPage {
    @ViewChild('slides') slides: Slides;
    defaultMenuPic = "assets/imgs/no_image.jpg"
    SlideArr: any
    MenuDetail: any
    ImagePath: any
    is_Admin: any
    Menu_Name: any
    isBegin: boolean
    isEnd: boolean
    Items: any
    public alertPresented: any;
    constructor(public platform: Platform, public actionSheetCtrl: ActionSheetController, public navCtrl: NavController, public alert: AlertProvider, public navParams: NavParams, public api: Api, public user: UserProvider) {
        this.alertPresented = false
        this.Items = this.navParams.get('menu')
        this.ImagePath = this.api.Menu_image;
        this.is_Admin = localStorage.getItem("is_admin")
        let detail = navParams.get('data');
        this.MenuDetail = detail
        debugger
        this.SlideArr = this.MenuDetail.images;
        console.log(this.MenuDetail, "menu Detail")
        if (!this.MenuDetail.menu_name_changed) {
            this.Menu_Name = this.MenuDetail.menu_name;
        } else {
            this.Menu_Name = this.MenuDetail.old_menu_name;
        }
    }
    getImage(value: any) {
        let image = this.ImagePath + value;
        return image;
    }
    next() {
        this.slides.slideNext();
    }
    prev() {
        this.slides.slidePrev();
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad MenuDetailPage');
    }
    menuEdit() {
        this.navCtrl.push("MenuEditPage", {
            data: this.MenuDetail,
            item: this.Items
        })
    }
    menuDelete() {
        // this.alert.presentDelete().then((res) => {
        this.alert.Loader.show("Deleting..")
        this.user.deleteMenu(this.MenuDetail.menu_id).subscribe((data: any) => {
            console.log(data)
            if (data.status == "ok") {
                this.alert.Loader.hide()
                this.alert.Toast.show("Menu Deleted Successfully")
                this.navCtrl.setRoot('MenusPage')
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        // }, err => {
        //   this.alert.Loader.hide();
        //   console.log(err)
        // })
    }
    public MenuDelete() {
        this.alertPresented = true
        localStorage.setItem("alert", this.alertPresented);
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Menu',
            buttons: [{
                text: 'Delete',
                icon: 'trash',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                    this.menuDelete()
                }
            }, {
                text: 'Cancel',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    this.alertPresented = false
                    localStorage.setItem("alert", this.alertPresented);
                }
            }],
        });
        actionSheet.present();
    }
    backward() {
        console.log("Back Clicked", this.slides)
        this.slides.slidePrev();
        this.isBegin = this.slides._isBeginning
        this.isEnd = this.slides._isEnd
        console.log(this.isEnd, "end")
        console.log(this.isBegin, "begin")
    }
    forward() {
        console.log("Forward Clicked")
        this.slides.slideNext();
        this.isBegin = this.slides._isBeginning
        this.isEnd = this.slides._isEnd
        console.log(this.isEnd, "end")
        console.log(this.isBegin, "begin")
    }
}