import {
    Component,
    ViewChild
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    App,
    ViewController
} from 'ionic-angular';
import {
    Api
} from '../../providers/api/api';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    NgZone
} from '@angular/core';
import {
    Platform,
    Content,
    FabContainer
} from 'ionic-angular';
/**
 * Generated class for the MenusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-menus',
    templateUrl: 'menus.html',
})
export class MenusPage {
    @ViewChild(Content) content: Content;
    defaultMenuPic = "assets/imgs/no_image.jpg"
    menuarray: any = []
    flagStatus = false
    ImagePath: any
    searchText = ""
    OrgItems: any = []
    is_Admin: any
    page_count: any = 0
    totalPages: any = 0;
    hasMoreData: any = false
    tempDataSearch: any
    goLive: any = false
    MenuEmpty = false
    backCount: any = 0;
    infiniteScrollEnabled = true
    constructor(public viewCtrl: ViewController, public _zone: NgZone, public app: App, public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider, public api: Api) {
        this.is_Admin = localStorage.getItem("is_admin")
        this.ImagePath = this.api.Menu_image;
    }
    ionViewDidLoad() {
        this.getmenuList()
    }
    addmenu(fab: FabContainer) {
        fab.close();
        let count: any = 0;
        let id: any = localStorage.getItem("tokens")
        this.user.getGroupList(id).subscribe((data: any) => {
            console.log(data, "groups")
            count = data.data.length;
            debugger
            if (count > 0) {
                this.navCtrl.push("AddmenuPage", {
                    data: this.menuarray
                })
            } else if (count == 0) {
                this.alert.Alert.alert("Please Add The Group Menu")
                return
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    menuDetail(list: any) {
        this.navCtrl.push("MenuDetailPage", {
            data: list,
            menu: this.menuarray
        })
    }
    getImage(value: any) {
        let image = this.ImagePath + value;
        return image;
    }
    onFilter(far) {
        // Normal Search
        //    console.log(this.searchText)
        // this.menuarray = this.tempDataSearch;
        // console.log(this.menuarray)
        //  this.menuarray = this.menuarray.filter((menu) => {
        //       if (menu.menu_name.toLocaleLowerCase().includes(this.searchText.toLocaleLowerCase()) || menu.cuisint_type.cuis_name.toLocaleLowerCase().includes(this.searchText.toLocaleLowerCase()) || menu.meal_type.mealtype_name.toLocaleLowerCase().includes(this.searchText.toLocaleLowerCase()) ||menu.package_type.pack_name.toLocaleLowerCase().includes(this.searchText.toLocaleLowerCase()) ) {
        //           return menu
        //       }
        //   })
        // Search By Api
        this.page_count = 0
        this.menuarray = []
        this.OrgItems = []
        this.totalPages = 0
        this.infiniteScrollEnabled = true;
        let id: any = localStorage.getItem("tokens")
        let dataToSet = {
            token: id,
            page_no: 1,
            page_count: 10,
            search: this.searchText
        }
        this.user.getAllMenu({
            "data": dataToSet
        }).subscribe((data: any) => {
            this.alert.Loader.hide()
            console.log(data)
            if (data.status == "ok") {
                for (let i = 0; i < data.data.length; i++) {
                    this.menuarray.push(data.data[i]);
                }
            }
            console.log(this.menuarray)
            if (this.menuarray.length > 0) {
                let temp: any = this.menuarray[0].total / 10
                if ((this.menuarray[0].total % 10) != 0) {
                    this.totalPages = parseInt(temp) + 1;
                } else {
                    this.totalPages = parseInt(temp)
                }
                this.hasMoreData = true
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    onCancel(far) {
        // this.menuarray = this.tempDataSearch;
        this.getmenuList();
    }
    scrollToTop() {
        this.content.scrollToTop();
    }
    getmenuList() {
        this._zone.run(() => {
            this.page_count = 0
            this.menuarray = []
            this.OrgItems = []
            this.totalPages = 0
            this.infiniteScrollEnabled = true;
            this.alert.Loader.show("Loading..")
            let id: any = localStorage.getItem("tokens")
            this.page_count = this.page_count + 1;
            let dataToSet = {
                token: id,
                page_no: this.page_count,
                page_count: 10,
                search: ''
            }
            this.user.getAllMenu({
                "data": dataToSet
            }).subscribe((data: any) => {
                this.alert.Loader.hide()
                console.log(data)
                for (let i = 0; i < data.data.length; i++) {
                    this.menuarray.push(data.data[i]);
                    this.OrgItems.push(data.data[i]);
                    this.tempDataSearch = this.OrgItems
                }
                if (this.menuarray.length > 0) {
                    for (let j = 0; j < this.menuarray.length; j++) {
                        if (this.menuarray[j].newly_added == "1") {
                            this.goLive = true
                        }
                    }
                }
                console.log(this.menuarray)
                if (this.menuarray.length > 0) {
                    let temp: any = this.menuarray[0].total / 10
                    if ((this.menuarray[0].total % 10) != 0) {
                        this.totalPages = parseInt(temp) + 1;
                    } else {
                        this.totalPages = parseInt(temp)
                    }
                    this.hasMoreData = true
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        })
    }
    LoadData(event) {
        debugger;
        this.page_count = this.page_count + 1;
        debugger
        if (this.page_count <= this.totalPages) {
            // this.alert.Loader.show("Loading")
            let id: any = localStorage.getItem("tokens")
            let dataToSet = {
                token: id,
                page_no: this.page_count,
                page_count: 10,
                search: ''
            }
            this.user.getAllMenu({
                "data": dataToSet
            }).subscribe((data: any) => {
                this.alert.Loader.hide()
                console.log(data)
                for (let i = 0; i < data.data.length; i++) {
                    this.menuarray.push(data.data[i]);
                    this.OrgItems.push(data.data[i]);
                    this.tempDataSearch = this.OrgItems
                }
                console.log(this.menuarray)
                if (this.menuarray.length > 0) {
                    for (let j = 0; j < this.menuarray.length; j++) {
                        if (this.menuarray[j].newly_added == "1") {
                            this.goLive = true
                        }
                    }
                }
                // event.target.disabled = true;
                if (this.page_count != this.totalPages) {
                    event.complete();
                } else {
                    this.infiniteScrollEnabled = false;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        } else {
            console.log("in else")
            this.infiniteScrollEnabled = false;
            // event.target.disabled = true;
            // this.alert.Alert.alert("Reached End Of the Page")
        }
    }
    golive() {
        this.alert.goLiveAlert().then((res) => {
            let token = localStorage.getItem("tokens")
            this.user.goLive(token).subscribe((data: any) => {
                if (data.status == "ok") {
                    this.alert.Toast.show("Menu Pushed to Live")
                    this.navCtrl.setRoot("MenusPage")
                } else {
                    this.alert.Alert.alert(data.data)
                    return
                }
                this.alert.Loader.hide();
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        }, err => {
            this.alert.Loader.hide();
            console.log(err)
        })
    }
    addGroup(fab: FabContainer) {
        fab.close();
        this.alert.presentAddGroupMenu().then((res) => {
            let datas = localStorage.getItem("group")
            if (datas == "") {
                return this.alert.Alert.alert("Please Enter the Group Name")
            } else {
                let token = localStorage.getItem("tokens")
                let data = {
                    "group_name": datas,
                    "token": token
                }
                this.user.addGroupName(data).subscribe((data: any) => {
                    if (data.status == "ok") {
                        this.alert.Toast.show("Added Successfully")
                    } else {
                        this.alert.Alert.alert(data.data)
                        return
                    }
                }, (error) => {
                    this.alert.Loader.hide();
                    this.alert.Alert.alert("Cannot Connect To Server")
                    return;
                })
            }
        }, err => {
            console.log(err)
        })
    }
    doRefresh(refresher) {
        this._zone.run(() => {
            this.page_count = 0
            this.menuarray = []
            this.OrgItems = []
            this.totalPages = 0
            this.infiniteScrollEnabled = true;
            let id: any = localStorage.getItem("tokens")
            this.page_count = this.page_count + 1;
            let dataToSet = {
                token: id,
                page_no: this.page_count,
                page_count: 10,
                search: ''
            }
            this.user.getAllMenu({
                "data": dataToSet
            }).subscribe((data: any) => {
                this.alert.Loader.hide()
                console.log(data)
                if (data.status == "ok") {
                    for (let i = 0; i < data.data.length; i++) {
                        this.menuarray.push(data.data[i]);
                        this.OrgItems.push(data.data[i]);
                        this.tempDataSearch = this.OrgItems
                    }
                    if (this.menuarray.length > 0) {
                        for (let j = 0; j < this.menuarray.length; j++) {
                            if (this.menuarray[j].newly_added == "1") {
                                this.goLive = true
                            }
                        }
                    }
                    console.log(this.menuarray)
                    if (this.menuarray.length > 0) {
                        let temp: any = this.menuarray[0].total / 10
                        if ((this.menuarray[0].total % 10) != 0) {
                            this.totalPages = parseInt(temp) + 1;
                        } else {
                            this.totalPages = parseInt(temp)
                        }
                        this.hasMoreData = true
                    }
                    refresher.complete();
                } else {
                    refresher.complete();
                    this.alert.Alert.alert(data.message)
                }
            }, async (error) => {
                refresher.complete();
                await this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        })
    }
    viewGroup(fab: FabContainer) {
        fab.close()
        this.navCtrl.push("MenugroupPage")
    }
}