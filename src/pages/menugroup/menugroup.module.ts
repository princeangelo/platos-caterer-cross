import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenugroupPage } from './menugroup';

@NgModule({
  declarations: [
    MenugroupPage,
  ],
  imports: [
    IonicPageModule.forChild(MenugroupPage),
  ],
})
export class MenugroupPageModule {}
