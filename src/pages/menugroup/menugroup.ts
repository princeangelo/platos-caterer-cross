import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    Platform,
    ActionSheetController,
    ItemSliding,
    ViewController
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    Api
} from '../../providers/api/api';
import {
    reorderArray
} from 'ionic-angular';
@IonicPage()
@Component({
    selector: 'page-menugroup',
    templateUrl: 'menugroup.html',
})
export class MenugroupPage {
    data: any = {
        menu_name: '',
        menu_items: ''
    }
    error: any = {
        menu_name: '',
        menu_items: ''
    }
    menuarray
    menuList: any = []
    menuNames: any = []
    selected: any = []
    ShowMinus = false;
    is_Admin: any
    items = [];
    constructor(public viewCtrl: ViewController, public actionSheetCtrl: ActionSheetController, public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider, public api: Api) {
        this.is_Admin = localStorage.getItem("is_admin")
        //   for (let x = 0; x < 5; x++) {
        //   this.items.push(x);
        // }
    }
    ionViewDidLoad() {
        this.alert.Loader.show("Loading..")
        let toke = localStorage.getItem("tokens")
        this.user.getGroupList(toke).subscribe((data: any) => {
            console.log(data, "group List")
            this.menuNames = data.data;
            this.alert.Loader.hide();
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    onChangeName(val: any) {
        let temp = []
        if (this.data.menu_name) {
            this.error.menu_name = ""
        }
        if (this.menuNames.length > 0) {
            for (let i = 0; i < this.menuNames.length; i++) {
                if (val == this.menuNames[i].menugroup_id) {
                    if (this.menuNames[i].items.length > 0) {
                        for (let j = 0; j < this.menuNames[i].items.length; j++) {
                            temp.push(this.menuNames[i].items[j].menu_id)
                        }
                    }
                }
            }
        }
        this.data.menu_items = temp
    }
    onChangeValue() {
        this.error.menu_items = ""
    }
    updateMenugroup() {
        if (this.data.menu_name == "") {
            this.error.menu_name = "Please Select the Group Name"
            return
        }
        if (this.data.menu_items == "") {
            this.error.menu_items = "Select Atleast One Menu"
            return
        }
        console.log(this.data)
        let token = localStorage.getItem("tokens")
        let data3 = {
            "menugroup_id": this.data.menu_name,
            "menu_list": ''
        }
        for (let i = 0; i < this.data.menu_items.length; i++) {
            this.selected.push({
                "menu_id": this.data.menu_items[i]
            })
        }
        data3.menu_list = this.selected
        // this.data3.push({ menu_id: data.data[i].menu.menu_id, menu_name: data.data[i].menu.menu_name});
        this.user.updateGroupMenu(token, data3).subscribe((data: any) => {
            if (data.status == "ok") {
                this.alert.Loader.hide();
                this.alert.Toast.show("Group Menu Updated")
                this.navCtrl.setRoot('VendorPage')
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
            this.alert.Loader.hide();
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    public presentActionSheetGroupMenu(val: any, slidingItem: ItemSliding) {
        let actionSheet = this.actionSheetCtrl.create({
            title: val.menugroup_name,
            buttons: [{
                text: 'Delete',
                icon: 'trash',
                handler: () => {
                    this.deleteGrp(val);
                }
            }, {
                text: 'Cancel',
                icon: 'close',
                role: 'cancel',
                handler: () => {
                    this.ShowMinus = false;
                    slidingItem.close();
                }
            }]
        });
        actionSheet.present();
    }
    deleteGrp(value) {
        debugger
        console.log(value)
        this.user.deleteGroupMenu(value.menugroup_id).subscribe((data: any) => {
            this.ShowMinus = false;
            debugger
            this.alert.Toast.show("Removed Successfully")
            this.alert.Loader.hide();
            let toke = localStorage.getItem("tokens")
            this.user.getGroupList(toke).subscribe((data: any) => {
                console.log(data, "group List")
                this.menuNames = data.data;
                this.alert.Loader.hide();
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        }, (error) => {
            this.alert.Loader.hide();
            this.ShowMinus = false;
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
        this.ShowMinus = false;
    }
    show() {
        this.ShowMinus = true
        console.log(this.menuNames)
    }
    done() {
        this.ShowMinus = false
        console.log(this.menuNames)
        this.alert.Loader.show("Loading")
        this.user.updateGroupMenuList(this.menuNames).subscribe((data: any) => {
            if (data.status == "ok") {
                this.alert.Loader.hide();
                this.alert.Toast.show("Menu Group Updated")
            }
            this.alert.Loader.hide();
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    reorderItems(indexes) {
        this.menuNames = reorderArray(this.menuNames, indexes);
        console.log(this.menuNames)
    }
    Edit(slidingItem: ItemSliding, value: any) {
        console.log("edit group value", value)
        let name = value.menugroup_name;
        this.alert.presentEditGroupMenu(name).then((res) => {
            slidingItem.close();
        }, err => {
            slidingItem.close();
            console.log(err)
        })
    }
}