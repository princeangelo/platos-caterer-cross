import {
    Component,
    ViewChild
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    Platform
} from 'ionic-angular';
import {
    Content
} from 'ionic-angular';
@IonicPage()
@Component({
    selector: 'page-orders',
    templateUrl: 'orders.html',
})
export class OrdersPage {
    @ViewChild(Content) content: Content;
    flagStatus = false
    orderList: any = []
    searchText = ""
    OrgItems: any = []
    List: any
    present_flag: boolean = true;
    future_flag: boolean = false;
    past_flag: boolean = false;
    is_Admin: any
    infiniteScrollEnabled = true
    page_count: any = 0
    totalPages: any = 0
    hasMoreData: any = false
    tempDataSearch: any
    tabview: any = "tab-present"
    constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider) {
        // this.List = this.users1;
        this.is_Admin = localStorage.getItem("is_admin")
    }
    ionViewDidLoad() {
        this.apihit(this.tabview)
    }
    apihit(value) {
        this.orderList = []
        this.OrgItems = []
        this.page_count = 0;
        this.totalPages = 0;
        this.infiniteScrollEnabled = true
        this.alert.Loader.show("Loading..")
        let token: any = localStorage.getItem('tokens')
        this.page_count = this.page_count + 1;
        let dataToSet = {
            token: token,
            page_no: this.page_count,
            page_count: 10,
            view: value,
            search: ""
        }
        this.user.getOrders(dataToSet).subscribe((data: any) => {
            console.log(data, "from order")
            if (data.status == "ok") {
                if (data.data[0].total > 0) {
                    for (let i = 0; i < data.data.length; i++) {
                        this.orderList.push(data.data[i]);
                        this.OrgItems.push(data.data[i]);
                        this.tempDataSearch = this.OrgItems
                    }
                }
                let temp: any = data.data[0].total / 10
                if ((data.data[0].total % 10) != 0) {
                    this.totalPages = parseInt(temp) + 1;
                } else {
                    this.totalPages = parseInt(temp)
                }
                this.hasMoreData = true
                this.alert.Loader.hide()
            } else {
                this.alert.Alert.alert(data.data)
                return
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    scrollToTop() {
        this.content.scrollToTop();
    }
    LoadData(event) {
        this.page_count = this.page_count + 1;
        debugger
        if (this.page_count <= this.totalPages) {
            // this.alert.Loader.show("Loading")
            let id: any = localStorage.getItem("tokens")
            let dataToSet = {
                token: id,
                page_no: this.page_count,
                page_count: 10,
                view: this.tabview,
                search: ""
            }
            this.user.getOrders(dataToSet).subscribe((data: any) => {
                this.alert.Loader.hide()
                console.log(data)
                if (data.data[0].total > 0) {
                    if (data.data.length > 0) {
                        for (let i = 0; i < data.data.length; i++) {
                            this.orderList.push(data.data[i]);
                            this.OrgItems.push(data.data[i]);
                            this.tempDataSearch = this.OrgItems
                        }
                    }
                }
                console.log(this.orderList)
                if (this.page_count != this.totalPages) {
                    event.complete();
                } else {
                    this.infiniteScrollEnabled = false;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        } else {
            console.log("in else")
            this.infiniteScrollEnabled = false;
        }
    }
    orderDetail(value: any) {
        this.navCtrl.push("OrderDetailsPage", {
            data: value,
            tab: this.tabview
        })
    }
    onFilter(far) {
        console.log(this.searchText)
        // this.orderList = this.tempDataSearch;
        // debugger;
        //  this.orderList =  this.orderList.filter((menu) => {
        //       if (menu.userslist.usr_first_name.toLocaleLowerCase().includes(this.searchText.toLocaleLowerCase()) ) {
        //           return menu
        //       }
        //   })
        this.orderList = []
        this.OrgItems = []
        this.page_count = 0;
        this.totalPages = 0;
        this.infiniteScrollEnabled = true
        let token: any = localStorage.getItem('tokens')
        this.page_count = this.page_count + 1;
        let dataToSet = {
            token: token,
            page_no: this.page_count,
            page_count: 10,
            view: this.tabview,
            search: this.searchText
        }
        this.user.getOrders(dataToSet).subscribe((data: any) => {
            console.log(data, "from order")
            if (data.status == "ok") {
                if (data.data[0].total > 0) {
                    for (let i = 0; i < data.data.length; i++) {
                        this.orderList.push(data.data[i]);
                        this.OrgItems.push(data.data[i]);
                        this.tempDataSearch = this.OrgItems
                    }
                }
                let temp: any = data.data[0].total / 10
                if ((data.data[0].total % 10) != 0) {
                    this.totalPages = parseInt(temp) + 1;
                } else {
                    this.totalPages = parseInt(temp)
                }
                this.hasMoreData = true
                this.alert.Loader.hide()
            } else {
                this.alert.Alert.alert(data.data)
                return
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    onCancel(far) {
        // this.orderList = this.tempDataSearch;
        this.apihit(this.tabview)
    }
    Present() {
        this.present_flag = true
        this.future_flag = false
        this.past_flag = false
        let value = "tab-present"
        this.tabview = value
        this.apihit(value)
    }
    Past() {
        this.present_flag = false
        this.future_flag = false
        this.past_flag = true
        let value = "tab-past"
        this.tabview = value
        this.apihit(value)
    }
    Future() {
        this.present_flag = false
        this.future_flag = true
        this.past_flag = false
        let value = "tab-future"
        this.tabview = value
        this.apihit(value)
    }
    show() {
        if (this.flagStatus == false) {
            this.flagStatus = true
        } else {
            this.flagStatus = false
            this.searchText = ""
        }
    }
    timeconvert(tim) {
        var timeString = tim + ":00";
        var H = +timeString.substr(0, 2);
        var h = H % 12 || 12;
        var ampm = (H < 12 || H === 24) ? "AM" : "PM";
        return timeString = h + timeString.substr(2, 3) + ampm;
    }
}