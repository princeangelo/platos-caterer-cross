import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams
} from 'ionic-angular';
import {
    Api
} from '../../providers/api/api';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    Platform
} from 'ionic-angular';
/**
 * Generated class for the ConfirmationDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-confirmation-detail',
    templateUrl: 'confirmation-detail.html',
})
export class ConfirmationDetailPage {
    confirmationDeatil: any
    menus: any
    ImagePath
    defaultMenuPic = "assets/imgs/no_image.jpg"
    constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public api: Api, public alert: AlertProvider) {
        this.confirmationDeatil = this.navParams.get("data")
        console.log(this.confirmationDeatil, "detail yuvi")
        this.menus = this.confirmationDeatil.order_list
        this.ImagePath = this.api.Menu_image;
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ConfirmationDetailPage');
    }
    toconfirm() {
        this.alert.presentConfirm().then((res) => {
            let id;
            let data = {
                "order_id": '',
                "ord_status": ''
            }
            data.order_id = this.confirmationDeatil.ord_id;
            id = data.order_id;
            data.ord_status = "Confirmed"
            console.log(data)
            this.alert.Loader.show("Loading..")
            this.user.changeStatus(id, data).subscribe((data: any) => {
                console.log(data)
                if (data.status == 'ok') {
                    this.alert.Loader.hide()
                    this.navCtrl.setRoot('ConfirmationPage')
                } else {
                    this.alert.Loader.hide()
                    this.alert.Alert.alert(data.data)
                    return;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        }, err => {
            this.alert.Loader.hide()
            console.log(err)
        })
    }
    tocancel() {
        this.alert.presentPromptFeedBack().then((res) => {
            let datas = localStorage.getItem("feedback")
            console.log("hit api", datas)
            console.log("cancel clicked")
            if (datas == "") {
                return this.alert.Alert.alert("Please Enter the Reason for Cancellation")
            }
            let id;
            let data = {
                "order_id": '',
                "ord_status": '',
                "reason_cancel": ''
            }
            data.order_id = this.confirmationDeatil.ord_id;
            id = data.order_id;
            data.ord_status = "Cancelled"
            data.reason_cancel = datas
            console.log(data)
            this.alert.Loader.show("Loading..")
            this.user.changeStatus(id, data).subscribe((data: any) => {
                console.log(data)
                if (data.status == 'ok') {
                    this.alert.Loader.hide()
                    this.navCtrl.setRoot('ConfirmationPage')
                } else {
                    this.alert.Loader.hide()
                    this.alert.Alert.alert(data.data)
                    return;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        }, err => {
            this.alert.Loader.hide()
            console.log(err)
        })
    }
    showImage(value: any) {
        let img = this.ImagePath + value;
        return img
    }
    timeconvert(tim) {
        var timeString = tim + ":00";
        var H = +timeString.substr(0, 2);
        var h = H % 12 || 12;
        var ampm = (H < 12 || H === 24) ? "AM" : "PM";
        return timeString = h + timeString.substr(2, 3) + ampm;
    }
}