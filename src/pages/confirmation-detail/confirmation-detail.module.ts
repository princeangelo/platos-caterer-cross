import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmationDetailPage } from './confirmation-detail';

@NgModule({
  declarations: [
    ConfirmationDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmationDetailPage),
  ],
})
export class ConfirmationDetailPageModule {}
