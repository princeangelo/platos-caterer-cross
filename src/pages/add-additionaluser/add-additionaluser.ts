import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    Events
} from 'ionic-angular';
@IonicPage()
@Component({
    selector: 'page-add-additionaluser',
    templateUrl: 'add-additionaluser.html',
})
export class AddAdditionaluserPage {
    data: any = {
        name: '',
        email: '',
        phone: '',
        password: '',
        con_password: ''
    }
    error: any = {
        name: '',
        email: '',
        phone: '',
        password: '',
        con_password: ''
    }
    passwordType: string = 'password';
    passwordIcon: string = 'eye-off';
    passwordTypes: string = 'password';
    passwordIcons: string = 'eye-off';
    vendordata: any
    fromEdit: any = false
    tit: any
    is_Admin: any
    is_nonveg: boolean
    anArrays: any = []
    Holidays: any = []
    temp: any
    from:any
    constructor(public platform: Platform, public events: Events, public user: UserProvider, public navCtrl: NavController, public navParams: NavParams, public alert: AlertProvider) {
        this.vendordata = this.navParams.get("data")
        this.fromEdit = this.navParams.get("edit")
        this.from = this.navParams.get("from")
        this.is_Admin = localStorage.getItem("is_admin")
        this.temp = this.vendordata.delivery_days
        if (this.fromEdit == true) {
            this.tit = "Add User Edit"
            this.data.name = this.vendordata.addi_user.cat_usr_name;
            this.data.email = this.vendordata.addi_user.cat_usr_email;
            this.data.phone = this.vendordata.addi_user.cat_usr_mobile;
            console.log(this.data.phone)
        } else {
            this.tit = "Add User"
        }
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad AddAdditionaluserPage');
    }
    backButtonAction() {
        this.navCtrl.pop()
    }
    showHide() {
        this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
        this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
    showHides() {
        this.passwordTypes = this.passwordTypes === 'text' ? 'password' : 'text';
        this.passwordIcons = this.passwordIcons === 'eye-off' ? 'eye' : 'eye-off';
    }
    onChangenumber() {
        this.error.phone = "";
    }
        moveFocus(nextElement) {
        nextElement.setFocus();
    }
    onChangeemail(value) {
        if (value == '') {
            this.error.email = "Please Enter the Email ID"
            return
        }
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(value) == false) {
            console.log("Invalid Email Address");
            this.error.email = "Invalid Mail Address"
        } else {
            console.log("Valid Address")
            this.error.email = ''
        }
    }
    onChangeName() {
        this.error.name = "";
    }
    onChangePass() {
        this.error.password = "";
        this.error.con_password = "";
    }
    onChangePasss() {
        this.error.con_password = "";
    }
    toVendor() {
        if (this.data.name == "") {
            this.error.name = "Please Enter the Name"
            return
        }
        if (this.data.email == "") {
            this.error.email = "Please Enter the Email"
            return
        }
        if (this.data.phone == "") {
            this.error.phone = "Please Enter the Phone Number"
            return
        }
        if (this.data.phone.length < 10) {
            this.error.phone = "Invalid Phone Number"
            return
        }
        if (this.fromEdit == false) {
            if (this.data.password == "") {
                this.error.password = "Please Enter the Password"
                return
            }
            if (this.data.password.length < 6) {
                this.error.password = "Min Password length Should be 6"
                return
            }
            if (this.data.con_password == "") {
                this.error.con_password = "Please Enter the Confirm Password"
                return
            }
            if (this.data.con_password.length < 6) {
                this.error.con_password = "Minimum Password length Should be 6"
                return
            }
            if (this.data.password != this.data.con_password) {
                this.error.con_password = "Password Mismatch"
                return
            }
        }
        debugger
        console.log(this.data)
        this.anArrays = []
        this.anArrays = this.temp
        if (this.anArrays.length > 0) {
            for (let i = 0; i < this.anArrays.length; i++) {
                if (!this.anArrays[i].fromtime.hour) {
                    if (this.anArrays[i].fromtime) {
                        let hour = this.anArrays[i].fromtime.substring(0, 2)
                        let minute = this.anArrays[i].fromtime.substring(3, 5)
                        let data = {
                            hour: '',
                            minute: ''
                        }
                        data.hour = hour
                        data.minute = minute
                        this.anArrays[i].fromtime = data
                    }
                }
                if (!this.anArrays[i].totime.hour) {
                    if (this.anArrays[i].totime) {
                        let hour = this.anArrays[i].totime.substring(0, 2)
                        let minute = this.anArrays[i].totime.substring(3, 5)
                        let data = {
                            hour: '',
                            minute: ''
                        }
                        data.hour = hour
                        data.minute = minute
                        this.anArrays[i].totime = data
                    }
                }
            }
        }
        if (this.vendordata.holidays.length > 0) {
            for (var i = 0; i < this.vendordata.holidays.length; ++i) {
                this.Holidays.push(this.vendordata.holidays[i].date)
            }
        } else {
            this.Holidays = this.vendordata.holidays;
        }
        let AddUser = []
        if (this.fromEdit == true) {
            let temp = {
                addi_cat_id: this.vendordata.addi_user.cat_usr_id,
                name: this.data.name,
                email: this.data.email,
                phone: this.data.phone,
                password: this.data.password
            }
            AddUser.push(temp)
            let item = localStorage.getItem("tokens")
            let datas = {
                "catt_first_name": this.vendordata.catt_first_name,
                "catt_email_address": this.vendordata.catt_email_address,
                "catt_mobile_no": this.vendordata.catt_mobile_no,
                // "catt_sur_name": this.vendordata.catt_sur_name,
                // "DeliveryTime_from": this.vendordata.catt_delivery_fromtime,
                // "DeliveryTime_to": this.vendordata.catt_delivery_totime,
                // "catt_event_type": this.vendordata.catt_event_type,
                "catt_min_order": this.vendordata.catt_min_order,
                "catt_max_order": this.vendordata.catt_max_order,
                "catt_lead_time": this.vendordata.catt_lead_time,
                // "catt_score": this.vendordata.catt_score,
                "catt_gst_certificate": this.vendordata.catt_gst_certificate,
                // "catt_food_certificate": this.vendordata.catt_food_certificate,
                // "newuser": this.vendordata.newuser,
                "catt_address": this.vendordata.catt_address,
                "catt_id": this.vendordata.catt_id,
                "caterer_image": this.vendordata.caterer_image,
                // "caterer_group" : this.vendordata.caterer_group,
                "catt_add_users": AddUser,
                "meal_types": this.vendordata.meal_types,
                "catt_catering_name": this.vendordata.catt_caterering_name,
                "catt_catering_info": this.vendordata.catt_caterering_info,
                "catt_delivery_pincode": this.vendordata.cust,
                "delivery_day": this.anArrays || [],
                "catt_upload_file": this.vendordata.catt_upload_file || [],
                "holidays": this.Holidays,
                "gst_number": this.vendordata.gst_number
            }
            console.log("after add user", datas)
            this.alert.Loader.show("Loading..")
            this.user.updateProfile(item, datas).subscribe((data: any) => {
                console.log(data)
                if (data.status == 'ok') {
                    if (this.is_Admin == "false") {
                        let name = this.data.name
                        let mail = this.data.email
                        this.events.publish('login:created', name, mail);
                    }
                    this.alert.Loader.hide()
                    if (this.fromEdit == false) {
                        this.alert.Toast.show("Additional User Added")
                    } else {
                        this.alert.Toast.show("Updated Successfully")
                    }

                    if(this.from == 'vendor'){
                       this.navCtrl.setRoot('VendorPage')
                    }
                    else if(this.from == 'profile'){
                         this.navCtrl.setRoot('MyProfilePage')
                    }
                   
                } else {
                    this.alert.Loader.hide()
                    this.alert.Alert.alert(data.data)
                    return;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        } else {
            let temp = {
                addi_cat_id: '',
                name: this.data.name,
                password: this.data.password,
                email: this.data.email,
                phone: this.data.phone,
            }
            AddUser.push(temp)
            let item = localStorage.getItem("tokens")
            let datas = {
                "catt_first_name": this.vendordata.catt_first_name,
                "catt_email_address": this.vendordata.catt_email_address,
                "catt_mobile_no": this.vendordata.catt_mobile_no,
                // "catt_sur_name": this.vendordata.catt_sur_name,
                // "DeliveryTime_from": this.vendordata.catt_delivery_fromtime,
                // "DeliveryTime_to": this.vendordata.catt_delivery_totime,
                // "catt_event_type": this.vendordata.catt_event_type,
                "catt_min_order": this.vendordata.catt_min_order,
                "catt_max_order": this.vendordata.catt_max_order,
                "catt_lead_time": this.vendordata.catt_lead_time,
                // "catt_score": this.vendordata.catt_score,
                "catt_gst_certificate": this.vendordata.catt_gst_certificate,
                // "catt_food_certificate": this.vendordata.catt_food_certificate,
                // "newuser": this.vendordata.newuser,
                "catt_address": this.vendordata.catt_address,
                "catt_id": this.vendordata.catt_id,
                "caterer_image": this.vendordata.caterer_image,
                // "caterer_group" : this.vendordata.caterer_group,
                "catt_add_users": AddUser,
                "meal_types": this.vendordata.meal_types,
                "catt_catering_name": this.vendordata.catt_caterering_name,
                "catt_catering_info": this.vendordata.catt_caterering_info,
                "catt_delivery_pincode": this.vendordata.cust,
                "delivery_day": this.anArrays || [],
                "catt_upload_file": this.vendordata.catt_upload_file || [],
                "holidays": this.Holidays,
                "gst_number": this.vendordata.gst_number
            }
            console.log("after add user", datas)
            this.alert.Loader.show("Loading..")
            this.user.updateProfile(item, datas).subscribe((data: any) => {
                console.log(data)
                if (data.status == 'ok') {
                    this.alert.Loader.hide()
                    if (this.fromEdit == false) {
                        this.alert.Toast.show("Additional User Added")
                    } else {
                        this.alert.Toast.show("Updated Successfully")
                    }
                    this.navCtrl.setRoot('VendorPage')
                } else {
                    this.alert.Loader.hide()
                    this.alert.Alert.alert(data.data)
                    return;
                }
            }, (error) => {
                this.alert.Loader.hide();
                this.alert.Alert.alert("Cannot Connect To Server")
                return;
            })
        }
    }
}