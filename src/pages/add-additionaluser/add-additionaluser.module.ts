import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddAdditionaluserPage } from './add-additionaluser';

@NgModule({
  declarations: [
    AddAdditionaluserPage,
  ],
  imports: [
    IonicPageModule.forChild(AddAdditionaluserPage),
  ],
})
export class AddAdditionaluserPageModule {}
