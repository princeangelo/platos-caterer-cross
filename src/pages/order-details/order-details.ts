import {
    Component,
    ViewChild
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {
    Api
} from '../../providers/api/api';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
import {
    Events
} from 'ionic-angular';
@IonicPage()
@Component({
    selector: 'page-order-details',
    templateUrl: 'order-details.html',
})
export class OrderDetailsPage {
    menus: any = []
    order_DetailData: any
    ImagePath: any
    confirmed: any = false
    status: any = ''
    is_Admin: any
    rating_delivery: number = 0;
    rating_food: number = 0;
    rating_Client: number = 0;
    showFood = false
    showDelivery = false
    showClient = true
    defaultMenuPic = "assets/imgs/no_image.jpg"
    @ViewChild('rating') rating: any;
    submit_delivery = true
    submit_food = true
    submit_client = true
    readonly_delivery = "false"
    readonly_food = "false"
    readonly_client = false
    orderTab: any
    constructor(public platform: Platform, public navCtrl: NavController, public events: Events, public navParams: NavParams, public api: Api, public user: UserProvider, public alert: AlertProvider) {
        this.is_Admin = localStorage.getItem("is_admin")
        this.orderTab = this.navParams.get("tab")
        console.log(this.orderTab)
        events.subscribe('star-rating:changed', (starRating) => {
            if (this.showDelivery == true) {
                this.rating_delivery = starRating
            } else if (this.showFood == true) {
                this.rating_food = starRating
            } else {
                this.rating_Client = starRating
            }
            console.log(starRating)
        });
        this.ImagePath = this.api.Menu_image;
        this.order_DetailData = this.navParams.get("data")
        console.log(this.order_DetailData, "detail")
        debugger;
        if (this.order_DetailData.rating) {
            for (let i = 0; i < this.order_DetailData.rating.length; i++) {
                // if(this.order_DetailData.rating[i].cat_rate_id == 1){
                //   this.submit_food = false
                //   this.readonly_food = "true"
                //   this.rating_food = this.order_DetailData.rating[i].cat_rating
                // }
                // else if(this.order_DetailData.rating[i].cat_rate_id == 2){
                //   this.submit_delivery = false
                //   this.readonly_delivery = "true"
                //   this.rating_delivery = this.order_DetailData.rating[i].cat_rating
                // }
                // else 
                if (this.order_DetailData.rating[i].cat_rate_id == 3) {
                    this.submit_client = false
                    this.readonly_client = true
                    this.rating_Client = this.order_DetailData.rating[i].cat_rating
                }
            }
        }
        this.menus = this.order_DetailData.order_list
        if (this.order_DetailData.ord_status == "Confirmed") {
            this.confirmed = "true"
        }
        if (this.order_DetailData.ord_status == "Out_to_delivery") {
            this.confirmed = "Delivered"
        }
        if (this.order_DetailData.ord_status == "Delivered") {
            this.confirmed = "rating"
        }
        if (this.order_DetailData.ord_status == "Cancelled") {
            this.confirmed = "cancel"
        }
        // if
        // if(this.confirmed == "Confirmed"){
        //   this.status = "Out to Delivery"
        // }
        // else if(this.confirmed == "Out_to_delivery"){
        //   this.status = "Delivered"
        // }
        // else if(this.confirmed == "Delivered"){
        //   this.status = "rate"
        // }
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad OrderDetailsPage');
    }
    showImage(value: any) {
        let img = this.ImagePath + value;
        return img
    }
    statusUpdate() {
        let id;
        let data = {
            "order_id": '',
            "ord_status": ''
        }
        data.order_id = this.order_DetailData.ord_id;
        id = data.order_id;
        if (this.order_DetailData.ord_status == "Confirmed") {
            data.ord_status = "Out_to_delivery"
        }
        if (this.order_DetailData.ord_status == "Out_to_delivery") {
            data.ord_status = "Delivered"
        }
        if (this.order_DetailData.ord_status == "Delivered") {
            return
        }
        // data.ord_status = this.status;
        console.log(data)
        this.alert.Loader.show("Loading..")
        if (data.ord_status == "Out_to_delivery") {
            this.alert.presentOuttoDelivery().then((res) => {
                this.user.changeStatus(id, data).subscribe((data: any) => {
                    console.log(data)
                    if (data.status == 'ok') {
                        this.alert.Loader.hide()
                        this.navCtrl.setRoot('OrdersPage')
                    } else {
                        this.alert.Loader.hide()
                        this.alert.Alert.alert(data.data)
                        return;
                    }
                }, (error) => {
                    this.alert.Loader.hide();
                    this.alert.Alert.alert("Cannot Connect To Server")
                    return;
                })
            }, err => {
                this.alert.Loader.hide()
                console.log(err)
            })
        } else if (data.ord_status == "Delivered") {
            this.alert.presentDelivered().then((res) => {
                this.user.changeStatus(id, data).subscribe((data: any) => {
                    console.log(data)
                    if (data.status == 'ok') {
                        this.alert.Loader.hide()
                        this.navCtrl.setRoot('OrdersPage')
                    } else {
                        this.alert.Loader.hide()
                        this.alert.Alert.alert(data.data)
                        return;
                    }
                }, (error) => {
                    this.alert.Loader.hide();
                    this.alert.Alert.alert("Cannot Connect To Server")
                    return;
                })
            }, err => {
                this.alert.Loader.hide()
                console.log(err)
            })
        }
    }
    rateClient() {
        console.log("client", this.rating_Client)
        let token = localStorage.getItem("tokens")
        let data = {
            cat_cat_id: token,
            cat_rate_id: 3,
            cat_order_id: this.order_DetailData.ord_id,
            cat_rating: this.rating_Client
        }
        this.user.rating(data).subscribe((data: any) => {
            if (data.status == "ok") {
                this.alert.Loader.hide()
                this.navCtrl.setRoot("OrdersPage")
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
            this.alert.Loader.hide();
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    rateDelivery() {
        let token = localStorage.getItem("tokens")
        console.log("delivery", this.rating_delivery)
        let data = {
            cat_cat_id: token,
            cat_rate_id: 2,
            cat_order_id: this.order_DetailData.ord_id,
            cat_rating: this.rating_delivery
        }
        this.user.rating(data).subscribe((data: any) => {
            if (data.status == "ok") {
                this.showDelivery = false
                this.showFood = false
                this.showClient = true
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
            this.alert.Loader.hide();
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    rateFood() {
        let token = localStorage.getItem("tokens")
        console.log("food", this.rating_food)
        let data = {
            cat_cat_id: token,
            cat_rate_id: 1,
            cat_order_id: this.order_DetailData.ord_id,
            cat_rating: this.rating_food
        }
        this.user.rating(data).subscribe((data: any) => {
            if (data.status == "ok") {
                this.showDelivery = true
                this.showFood = false
                this.showClient = false
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
            this.alert.Loader.hide();
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
    showclient() {
        this.showFood = false
        this.showDelivery = false
        this.showClient = true
    }
    showDelive() {
        this.showFood = false
        this.showDelivery = true
        this.showClient = false
    }
    timeconvert(tim) {
        var timeString = tim + ":00";
        var H = +timeString.substr(0, 2);
        var h = H % 12 || 12;
        var ampm = (H < 12 || H === 24) ? "AM" : "PM";
        return timeString = h + timeString.substr(2, 3) + ampm;
    }
}