import {
    Component
} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams
} from 'ionic-angular';
import {
    UserProvider
} from '../../providers/user/user';
import {
    AlertProvider
} from '../../providers/alert/alert';
@IonicPage()
@Component({
    selector: 'page-profile-edit',
    templateUrl: 'profile-edit.html',
})
export class ProfileEditPage {
    data: any = {
        catt_first_name: '',
        catt_sur_name: '',
        catt_email_address: '',
        catt_mobile_no: '',
    }
    error: any = {
        first_name: '',
        sur_name: '',
        mobile_no: '',
        email_address: '',
    }
    getProfileData: any
    anArray: any
    constructor(public navCtrl: NavController, public navParams: NavParams, public user: UserProvider, public alert: AlertProvider) {
        this.getProfileData = this.navParams.get("data")
        console.log(this.getProfileData)
        this.data.catt_first_name = this.getProfileData.catt_first_name;
        this.data.catt_sur_name = this.getProfileData.catt_sur_name;
        this.data.catt_email_address = this.getProfileData.catt_email_address;
        this.data.catt_mobile_no = this.getProfileData.catt_mobile_no;
        this.anArray = this.getProfileData.cust;
        this.getProfileData.newuser = ""
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ProfileEditPage');
    }
    onChangeName() {
        this.error.first_name = "";
    }
    onChangeSurName() {
        this.error.sur_name = "";
    }
    onChangenumber() {
        this.error.mobile_no = "";
    }
    onChangeemail(value) {
        if (value == '') {
            this.error.email_address = "Please Enter the Email ID"
            return
        }
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (reg.test(value) == false) {
            console.log("Invalid Email Address");
            this.error.email_address = "Invalid Mail Address"
        } else {
            console.log("Valid Address")
            this.error.email_address = ''
        }
    }
    updateProfile() {
        if (this.data.catt_first_name == "") {
            this.error.first_name = "Please Enter the Firstname"
            return
        }
        if (this.data.catt_sur_name == "") {
            this.error.sur_name = "Please Enter the Surname"
            return
        }
        if (this.data.catt_email_address == "") {
            this.error.email_address = "Please Enter the EmailId"
            return
        }
        if (this.data.catt_mobile_no == "") {
            this.error.mobile_no = "Please Enter the Mobile Number"
            return
        }
        let item = localStorage.getItem("tokens")
        let datas = {
            "catt_first_name": this.data.catt_first_name,
            "catt_email_address": this.data.catt_email_address,
            "catt_mobile_no": this.data.catt_mobile_no,
            "catt_sur_name": this.data.catt_sur_name,
            "catt_delivery_fromtime": this.getProfileData.catt_delivery_fromtime,
            "catt_delivery_totime": this.getProfileData.catt_delivery_totime,
            "catt_event_type": this.getProfileData.catt_event_type,
            "catt_min_order": this.getProfileData.catt_min_order,
            "catt_max_order": this.getProfileData.catt_max_order,
            "catt_lead_time": this.getProfileData.catt_lead_time,
            "catt_score": this.getProfileData.catt_score,
            "catt_gst_certificate": this.getProfileData.catt_gst_certificate,
            "catt_food_certificate": this.getProfileData.catt_food_certificate,
            "newuser": this.getProfileData.newuser,
            "catt_address": this.getProfileData.catt_address,
            "catt_id": this.getProfileData.catt_id,
            "caterer_group": this.getProfileData.caterer_group
        }
        this.alert.Loader.show("Loading..")
        this.user.updateProfile(item, datas).subscribe((data: any) => {
            console.log(data)
            if (data.data.status == 'ok') {
                this.alert.Loader.hide()
                this.navCtrl.setRoot('VendorPage')
            } else {
                this.alert.Loader.hide()
                this.alert.Alert.alert(data.data)
                return;
            }
        }, (error) => {
            this.alert.Loader.hide();
            this.alert.Alert.alert("Cannot Connect To Server")
            return;
        })
    }
}