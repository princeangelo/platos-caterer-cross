import { Component, ViewChild } from '@angular/core';
import { App,Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserProvider } from '../providers/user/user';
import {
    FirstRunPage
} from '../pages/pages';
import {
    MainPage,
} from '../pages/pages';
import {
    File
} from '@ionic-native/file';
import { AlertProvider } from '../providers/alert/alert';
import { NgZone } from '@angular/core';
import { Events,AlertController } from 'ionic-angular';
import { Api } from '../providers/api/api';
import { Observable } from 'rxjs/Observable';
import {IonicApp } from 'ionic-angular';

@Component({
  templateUrl: 'app-nav.html',
   styles: ["app-nav.scss"]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  image:'../assets/imgs/cater_banner.png'
  pages: Array<{title: string, component: any}>;
  carter_name:any
  carter_mail:any
  
  filelocation:any
is_Admin:any
imgPath:any
auditDeactivate:any = false
profilePic:any
defaultPic = "assets/imgs/cater_banner.png"
arr = [{"name":'AddAdditionaluserPage'},{"name":'AddmenuPage'},
        {"name":'CateringInfoEditPage'},
        {"name":'CertificateEditPage'},
        {"name":'ChangepasswordPage'},
        {"name":'ConfirmationDetailPage'},
        {"name":'ForgotPage'},
        {"name":'MenuDetailPage'},
        {"name":'MenuEditPage'},{"name":'OrderDetailsPage'},{"name":'SignupPage'},{"name":'VendorProfileEditPage'},{"name":'MenugroupPage'}]

public alertPresented: any;
temp
  constructor(private ionicApp: IonicApp,private alertCtrl: AlertController,public api:Api,public events: Events,public _zone: NgZone,public alert:AlertProvider,public file:File,public app:App,public user: UserProvider,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    

 this.alertPresented = false

      this.imgPath = this.api.profile_pic;
      
      let pic = localStorage.getItem("pic");
debugger
      if(pic){
        this.defaultPic = this.imgPath + pic;
      }
      else if(pic == null){
        this.defaultPic = "assets/imgs/cater_banner.png"
      }
      else{
        this.defaultPic = "assets/imgs/cater_banner.png"
      }
      

events.subscribe('profilepic:created', (profile) => {
    this._zone.run(() => { 
    console.log('picName', profile);
    localStorage.setItem("pic",profile);
    if(profile){
      this.defaultPic = this.imgPath + profile;
    }
    else if(profile == null){
     this.defaultPic = "assets/imgs/cater_banner.png"
    }
    else{
       this.defaultPic = "assets/imgs/cater_banner.png"
    }
    
  })
    
  });

events.subscribe('audit:status', (status) => {
    this._zone.run(() => { 
    console.log('status', status);
    this.auditDeactivate = status
  })

  });


events.subscribe('isAdmin:created', (admin) => {
    this._zone.run(() => { 
    console.log('admin', admin);
    localStorage.setItem("is_admin", admin);
    let temp = localStorage.getItem("is_admin")
    this.is_Admin = temp;
  })

  });


events.subscribe('login:created', (name, mail) => {
     this._zone.run(() => { 
    console.log('Welcome', name, mail);
    this.carter_name = name;
    this.carter_mail = mail;
    localStorage.setItem("carter_name",  this.carter_name)
    localStorage.setItem("carter_email",  this.carter_mail)
    this.is_Admin = localStorage.getItem("is_admin")
  })

  });

    this.initializeApp();

    // used for an example of ngFor and navigation
  // this.pages = [

  //    { title: 'Home', component: DashboardPage },
  //    { title: 'Menus', component: MenusPage },
  //    { title: 'Orders', component: OrdersPage },
  //    { title: 'My-Profile', component: MyProfilePage },
  //    { title: 'Vendor', component: VendorPage },
  //  ];
  }

  initializeApp() {

 this.platform.registerBackButtonAction(() => {
      // Catches the active view
      let nav = this.app.getActiveNavs()[0];
      let overlayView = this.ionicApp._overlayPortal._views[0];
      // let nav = this.app._appRoot._getActivePortal() || this.app.getActiveNav();
      console.log(nav);
      let activeView = nav.getActive();
      console.log(activeView)                
      // Checks if can go back before show up the alert
      if(activeView.name === MainPage) {
     this.temp = localStorage.getItem("alert");
      if(this.temp == 'false') {
        if(!this.alertPresented) {
         this.alertPresented = true
           const  alert = this.alertCtrl.create({
                  title: 'Exit',
                  message: 'Do you want to exit ?',
                  buttons: [{
                      text: 'Cancel',
                      role: 'cancel',
                      handler: () => {
                        this.alertPresented = false
                      }
                  },{
                      text: 'Ok',
                      handler: () => {
                        this.alertPresented = false
                        this.platform.exitApp();
                      }
                  }]
              });
              alert.present();

            }
          }
      }

      else {
        
         this.temp = localStorage.getItem("alert");
         console.log(this.temp)

       for (var i = 0; i < this.arr.length; ++i) {
            if(activeView.name == this.arr[i].name){
             if(this.temp == 'false'){
                if (nav.canGoBack()){
                  if (overlayView && overlayView.dismiss) {
                    overlayView.dismiss();
                  }
                    nav.pop();
                    return
                   }
                 }
              }  
          }
  }

  if(this.temp == 'false'){
    if(activeView.name === FirstRunPage){
           this.temp = localStorage.getItem("alert");
      if(this.temp == 'false') {
        if(!this.alertPresented) {
         this.alertPresented = true
           const  alert = this.alertCtrl.create({
                  title: 'Exit',
                  message: 'Do you want to exit ?',
                  buttons: [{
                      text: 'Cancel',
                      role: 'cancel',
                      handler: () => {
                        this.alertPresented = false
                      }
                  },{
                      text: 'Ok',
                      handler: () => {
                        this.alertPresented = false
                        this.platform.exitApp();
                      }
                  }]
              });
              alert.present();

            }
          }
    }
    else{
       this.nav.setRoot(MainPage);
       console.log("good to go")
    }
   
  }
  else{
    console.log("stop")
  }


      // this.nav.setRoot(MainPage);
    // if(activeView.name === Tab1Root || Tab2Root || Tab3Root || Tab4Root || Tab6Root || Tab7Root || Tab8Root){
    //              this.nav.setRoot(MainPage);
    //   }
  


  });

    this.platform.ready().then(() => {

let token = localStorage.getItem("tokens")
       if(token){
      debugger
      this._zone.run(() => { 
      let name = localStorage.getItem("carter_name");
      let mail = localStorage.getItem("carter_email");
      this.events.publish('login:created', name, mail);
      console.log("when logged in",name,mail)
    })
    }


     let pic = localStorage.getItem("pic");
      if(pic != "null"){
        let profile = pic
        this.events.publish('profilepic:created', profile);
      }
      else{
        this.defaultPic = "assets/imgs/cater_banner.png"
      }



 this.is_Admin = localStorage.getItem("is_admin")
       if(this.platform.is('ios')){
        console.log("ios true")
         this.filelocation = this.file.cacheDirectory;
        this.user.setDownloadPath(this.filelocation)
      }

      if(this.platform.is('android')){
        console.log("android true")
        this.filelocation = this.file.externalDataDirectory;
        this.user.setDownloadPath(this.filelocation)
      }
         
            // this._zone.run(() => { 

         this.is_Admin = localStorage.getItem("is_admin")
         // let id:any = localStorage.getItem("tokens")

      //     this.user.getUsername(id).subscribe((data: any) => {
      // console.log(data,"dash")
     
      // localStorage.setItem("carter_name",  data.data.catt_first_name)
      // localStorage.setItem("carter_email",  data.data.catt_email_address)

      //   }, (error) => {
      //      console.log(error)
      //   })
    



        console.log(this.carter_mail)
        console.log(this.carter_name)
            // })
        this.startApp()
        this.statusBar.backgroundColorByHexString('#c2953b');
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page);
  }



        startApp() {
        if (this.user.checkLogin()) {
            this.rootPage = MainPage;
            // this.menu.enable(false);
        } else {
           this.rootPage = FirstRunPage;        
         }
    }

   async logout(){

         this.alert.logout().then(async(res)=>{
        await    this.user.logout();
            let alertPresent:any = false
            localStorage.setItem("alert",alertPresent);
            this.app.getRootNav().setRoot(FirstRunPage)
          },err => {
            console.log(err)
          })   
     }
        
}
