import {
    Injectable
} from '@angular/core';
import {
    AlertController,
    ToastController,
    LoadingController,
    Platform
} from 'ionic-angular';


@Injectable()


export class AlertProvider {
  public alertShown:boolean = false;
  public alertPresent: any;

    constructor(public platform:Platform,private _alert: AlertController, private _toastCtrl: ToastController, private loadingCtrl: LoadingController) {
        this.alertPresent = false;
        localStorage.setItem("alert",this.alertPresent);
    }
    
    TrnsData: any = [];
   /**
    * Variable to store toast message.
    */
    public _toastMsg;
   /**
    * Variable to store loading .
    */
    public loading;
   /**
    * It creates the loading popup.
    */
    public Loader = {
        show: (data) => {
           this.alertPresent = true
          localStorage.setItem("alert",this.alertPresent);
            this.loading = this.loadingCtrl.create({
                content: data ? data : "Loading",
            });
            this.loading.present();
        },
        hide: () => {
           this.alertPresent = false
           localStorage.setItem("alert",this.alertPresent);
            this.loading.dismiss();
        }
    }
/**
 * Global declaration for Alert message.
 */
    public Alert = {
        confirm: (msg ? , title ? , okay ? , cancel ? ) => {
            return new Promise((resolve, reject) => {
                let alert = this._alert.create({
                    title: title || 'Confirm',
                    message: msg ,
                    buttons: [{
                        text: cancel || ['CANCEL'],
                        role: 'cancel',
                        handler: () => {
                            reject(false);
                        }
                    }, {
                        text: okay || ['OKAY'],
                        handler: () => {
                            resolve(true);
                        }
                    }]
                });
                alert.present();
            });
            // this.shared.Alert.confirm().then((res) => {
            //          console.log('confirmed');
            //      }, err => {
            //          console.log('user cancelled');
            //      })
        },
        alert: (msg, title ? ) => {
          this.alertPresent = true
          localStorage.setItem("alert",this.alertPresent);
            let alert = this._alert.create({
                title: title || '',
                subTitle: msg,
                buttons: [{
                  text: 'Dismiss',
                  role: 'cancel',
                  handler: () => {
                    this.alertPresent = false
                    localStorage.setItem("alert",this.alertPresent);
                  }
                }]
            });
            alert.present();
        }
    }
/**
 * Global declaration for Toast message.
 */
    public Toast = {
        show: (text: string, duration ? , position ? , closeButton ? , btnText ? ) => {
            this._toastMsg = this._toastCtrl.create({
                message: text,
                duration: duration || closeButton ? null : 3000,
                position: position || 'bottom',
                showCloseButton: closeButton || false,
                closeButtonText: btnText || 'OK'
            });
            this._toastMsg.present();
        },
        hide() {
            this._toastMsg.dismiss();
        }
    }

    presentConfirmExit():any {
 this.alertPresent = true
 localStorage.setItem("alert",this.alertPresent);
 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Exit',
    message: 'Are You Sure To Exit the App?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           this.alertPresent = false
 localStorage.setItem("alert",this.alertPresent);
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
           this.alertPresent = false
 localStorage.setItem("alert",this.alertPresent);
          resolve(true);
          console.log('Buy clicked');
        }
      }
    ]
  });
  alert.present();
              });
          }

  


    presentConfirm():any {
 this.alertPresent = true
 localStorage.setItem("alert",this.alertPresent);
 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Confirm',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
           this.alertPresent = false
 localStorage.setItem("alert",this.alertPresent);
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Confirm',
        handler: () => {
           this.alertPresent = false
 localStorage.setItem("alert",this.alertPresent);
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }


   presentCancel():any {
 this.alertPresent = true
 localStorage.setItem("alert",this.alertPresent);
 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Cancel',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           this.alertPresent = false
 localStorage.setItem("alert",this.alertPresent);
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
           this.alertPresent = false
 localStorage.setItem("alert",this.alertPresent);
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }


      requestAudit():any {
 this.alertPresent = true
 localStorage.setItem("alert",this.alertPresent);
 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Request Audit',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           this.alertPresent = false
 localStorage.setItem("alert",this.alertPresent);
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
           this.alertPresent = false
 localStorage.setItem("alert",this.alertPresent);
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }


 presentOuttoDelivery():any {
 this.alertPresent = true
 localStorage.setItem("alert",this.alertPresent);
 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Out To Delivery',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           this.alertPresent = false
           localStorage.setItem("alert",this.alertPresent);
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
           this.alertPresent = false
           localStorage.setItem("alert",this.alertPresent);
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }


 presentDelivered():any {
 this.alertPresent = true
 localStorage.setItem("alert",this.alertPresent);
 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Delivered',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           this.alertPresent = false
 localStorage.setItem("alert",this.alertPresent);
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
           this.alertPresent = false
 localStorage.setItem("alert",this.alertPresent);
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }



      logout():any {

 return new Promise((resolve, reject) => {
    this.alertPresent = true
    localStorage.setItem("alert",this.alertPresent);
  let alert = this._alert.create({
    title: 'Logout',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           this.alertPresent = false
           localStorage.setItem("alert",this.alertPresent);
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
           this.alertPresent = false
           localStorage.setItem("alert",this.alertPresent);
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }

presentPromptFeedBack() {
   this.alertPresent = true
 localStorage.setItem("alert",this.alertPresent);
   return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Cancellation',
    inputs: [
      {
        name: 'Reason',
        placeholder: 'Reason for Cancellation'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
           this.alertPresent = false
 localStorage.setItem("alert",this.alertPresent);
           reject(false);
          console.log('Cancel clicked');
        }
      },
       {
        text: 'Submit',
        handler: (data) => {
          if (data) {
             this.alertPresent = false
             localStorage.setItem("alert",this.alertPresent);
            console.log(data,"feedback")
            localStorage.setItem("feedback",data.Reason)
            resolve(true);
          } 
        }
      }
    ]
  });
  alert.present();
   });
}
    
 presentDelete():any {
 this.alertPresent = true
 localStorage.setItem("alert",this.alertPresent);
 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Delete',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
           this.alertPresent = false
           localStorage.setItem("alert",this.alertPresent);
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
           this.alertPresent = false
           localStorage.setItem("alert",this.alertPresent);
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }



 goLiveAlert():any {
 this.alertPresent = true
 localStorage.setItem("alert",this.alertPresent);
 
 return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Go Live',
    message: 'Are You Sure ?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
            this.alertPresent = false
            localStorage.setItem("alert",this.alertPresent);
           reject(false);
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.alertPresent = false
          localStorage.setItem("alert",this.alertPresent);
          resolve(true);
        }
      }
    ]
  });
  alert.present();
              });
          }



  presentAddGroupMenu() {
   this.alertPresent = true
   localStorage.setItem("alert",this.alertPresent);

   return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Add Menu Group',
    inputs: [
      {
        name: 'Group',
        placeholder: 'Enter Group Name'
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
             this.alertPresent = false
             localStorage.setItem("alert",this.alertPresent);
             reject(false);
             console.log('Cancel clicked');
        }
      },
       {
        text: 'Submit',
        handler: (data) => {
          if (data) {
            this.alertPresent = false
            localStorage.setItem("alert",this.alertPresent);
            console.log(data,"group")
            localStorage.setItem("group",data.Group)
            resolve(true);
          } 
        }
      }
    ]
  });
  alert.present();
   });
}

  presentEditGroupMenu(grup) {
   this.alertPresent = true
   localStorage.setItem("alert",this.alertPresent);

   return new Promise((resolve, reject) => {
  let alert = this._alert.create({
    title: 'Edit Menu Group',
    inputs: [
      {
        name: 'Group',
        placeholder: 'Enter Group Name',
        value: grup
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
             this.alertPresent = false
             localStorage.setItem("alert",this.alertPresent);
             reject(false);
             console.log('Cancel clicked');
        }
      },
       {
        text: 'Submit',
        handler: (data) => {
          if (data) {
            this.alertPresent = false
            localStorage.setItem("alert",this.alertPresent);
            console.log(data,"group")
            localStorage.setItem("group",data.Group)
            resolve(true);
          } 
        }
      }
    ]
  });
  alert.present();
   });
}

}
