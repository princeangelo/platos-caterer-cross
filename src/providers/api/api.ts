import {
    HttpClient,
    HttpParams
} from '@angular/common/http';
import {
    Injectable
} from '@angular/core';




           
@Injectable()
export class Api {

    url: string = 'http://platos.in/api/';
    Food_certificate='http://platos.in/images/caterer/foodimage/';
    GST_certificate='http://platos.in/images/caterer/gstimage/';
    profile_pic = 'http://platos.in/images/caterer/profile_pic/';
    Menu_image='http://platos.in/images/menu_images/';
    filepath='http://platos.in/images/caterer/fileimage/'

    constructor(public http: HttpClient) {

    }
     /**
      * Method used to get data from server.
      */
    get(endpoint: string, params ? : any, reqOpts ? : any) {
        if (!reqOpts) {
            reqOpts = {
                params: new HttpParams()
            };
        }
        // Support easy query params for GET requests
        if (params) {
            reqOpts.params = new HttpParams();
            for (let k in params) {
                reqOpts.params = reqOpts.params.set(k, params[k]);
            }
        }
        reqOpts = this.getRetOpts(reqOpts)
        return this.http.get(this.url + endpoint, reqOpts);
    }
     /**
      * Method used to send data to server.
      */
    post(endpoint: string, body: any, reqOpts ? : any) {
        reqOpts = this.getRetOpts(reqOpts)
        return this.http.post(this.url + endpoint, body, reqOpts);
    }
     /**
      * Set the headers.
      */
    getRetOpts(reqOpts) {
        if (!reqOpts) {
            reqOpts = {}
        }

        return reqOpts;
    }
     /**
      * Getting Token from Local storage.
      */
    get_token() {
        return localStorage.getItem("token");
    }

     /**
      * Put Method.
      */
    put(endpoint: string, body: any, reqOpts ? : any) {
        reqOpts = this.getRetOpts(reqOpts)
        return this.http.put(this.url + endpoint, body, reqOpts);
    }

     /**
      * Delete Method.
      */
    delete(endpoint: string, reqOpts ? : any) {
        reqOpts = this.getRetOpts(reqOpts)
        return this.http.delete(this.url + endpoint, reqOpts);
    }
     /**
      * Bulk Update Method.
      */
    patch(endpoint: string, body: any, reqOpts ? : any) {
        reqOpts = this.getRetOpts(reqOpts)
        return this.http.patch(this.url + endpoint, body, reqOpts);
    }


}
