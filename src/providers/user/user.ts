import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../api/api';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/share'
import { Events,Platform } from 'ionic-angular';
import { AlertProvider } from '../alert/alert';

@Injectable()

export class UserProvider {
_user: any;
  constructor(public alert: AlertProvider,public platform:Platform,public events:Events,public http: HttpClient,public api: Api) {

    console.log('Hello UserProvider Provider');
  }


  getEventDropdownList() {
        let seq = this.api.get('getall_events/').share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

    getCuisineDropdownList() {
        let seq = this.api.get('getall_cuisine/').share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }


        getGroupsDropdownList() {
        let seq = this.api.get('getall_groups/').share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }


            getMealsDropdownList() {
        let seq = this.api.get('getall_mealtypes/').share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }


     getPackageDropdownList() {
        let seq = this.api.get('getall_package/').share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

     removeCatererImg(id:any) {
        let seq = this.api.get('Delete_catimage/'+id).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

    deleteMenu(id:any) {
        let seq = this.api.delete('getAll_menu/'+id).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }  

    deleteOtherCertificates(certify_id:any) {
        let seq = this.api.post('Delete_othercertificate/',{"certify_id" : certify_id}).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }  

    deleteCate(id:any) {
        let seq = this.api.delete('Deletespl_cat/'+id).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }  

    deleteImageMenu(id:any) {
        let seq = this.api.delete('Deletemenu_img/'+id).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }  

    deleteGroupMenu(id:any) {
        let seq = this.api.delete('Deletecat_menu/'+id).share();
        seq.subscribe((res: any) => {
            if (res.status == 'success') {} else {}
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }





    getAllMenu(token: any) {
        let seq = this.api.post('GetAll_catterMenu/',token).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

     updateGroupMenuList(list: any) {
        let seq = this.api.post('GroupOrder_update/',{"data":list}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

      rating(data: any) {
        let seq = this.api.post('Caterer_rating/',{"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

       getMenu(token: any) {
        let seq = this.api.post('MenuList/',{"token":token}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }  

    goLive(token: any) {
        let seq = this.api.post('getmenu_golive/',{"token":token}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }  

    vendoegoLive(token: any) {
        let seq = this.api.post('Vendor_golive/',{"token":token}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

     AuditUpload(uri: any) {
        let seq = this.api.post('catfile_image/',uri).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }



    getOrders(data: any) {
        let seq = this.api.post('GetAllMy_Orders/',{"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }  

     getGroupList(token: any) {
        let seq = this.api.post('getcat_grouplist/',{"token":token}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }   

    addGroupName(data: any) {
        let seq = this.api.post('getall_groups/',{"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }  

    addSpecialCategory(data: any) {
        let seq = this.api.post('getmy_specialcategory/',{"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    } 

    requestAudit(token: any) {
        let seq = this.api.post('Requesting_audit/',{"token":token}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    } 

    deleteCertificate(data: any) {
        let seq = this.api.post('Delete_certificate/',{"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }


     getCaterer(id: number) {
        let seq = this.api.post('GetCatter_details/',id).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }


        getUsername(id: any) {
        let seq = this.api.post('GetCatter_details/',{"token":id}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

     changeStatus(id: any,data:any) {
        let seq = this.api.put('GetMyOrder_List/'+id,{"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    } 

    updateProfile(item: any,data:any) {
        let seq = this.api.put('getall_catterlist/'+item,{"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }

     updateGroupMenu(item: any,data:any) {
        let seq = this.api.put('Set_MenuCategory/'+item,{"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }


    



     updateMenu(id: any,data:any) {
        let seq = this.api.put('getAll_menu/'+id,{"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
                
            }
        }, err => {
            console.error('ERROR', err);
        });
        return seq;
    }



     signUp(details: any) {
        let seq = this.api.post('getall_catterlist/', {"data":details}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
               console.log("details sent")
            }
        }, err => {
           console.log(err)
        });
        return seq;
    }

    getConfirmationList(data: any) {
        let seq = this.api.post('GetMyOrder_newList/', {"data":data}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
               console.log("details sent")
            }
        }, err => {
           console.log(err)
        });
        return seq;
    }



     login(cred: any) {
        let seq = this.api.post('UserLogin/', {data:cred}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
               console.log("login done")
            }
        }, err => {
           console.log(err)
        });
        return seq;
    }

    addMenu(list: any) {
        let seq = this.api.post('getAll_menu/', {data:list}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
               console.log("addmenu done")
            }
        }, err => {
           console.log(err)
        });
        return seq;
    }

    checkMenuNameExists(name: any) {
        let seq = this.api.post('Check_Menuname/', {data:name}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
               console.log("addmenu done")
            }
        }, err => {
           console.log(err)
        });
        return seq;
    }    

    getPincodes() {
        let seq = this.api.get('GellAll_Pincodes/').share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
               console.log("addmenu done")
            }
        }, err => {
           console.log(err)
        });
        return seq;
    }

     forgot(mail: any) {
        let seq = this.api.post('Forgot_password/', {data:mail}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
               console.log("forgot done")
            }
        }, err => {
           console.log(err)
        });
        return seq;
    }


    change_password(pass: any) {  
        let seq = this.api.post('Caterer_resetpassword/', {data:pass}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
               console.log("forgot done")
            }
        }, err => {
           console.log(err)
        });
        return seq;
    }


    mis_report(token: any) {
        let seq = this.api.post('GetMyMIS_reports/', {"token":token}).share();
        seq.subscribe((res: any) => {
           
            if (res.status == 'success') {
               console.log("forgot done")
            }
        }, err => {
           console.log(err)
        });
        return seq;
    }


        checkLogin() {
        if ((localStorage.getItem("tokens") !== null && localStorage.getItem("tokens") !== "")) {
            return true;
        }
        return false;
       }


            logout() {
            this._user = null;
            localStorage.clear();
         }



        setDownloadPath(value){

            localStorage.setItem("DownloadPath",value);
        }

        getDownloadPath(){

           return localStorage.getItem("DownloadPath") || "";
        }




}